package com.practice.threading;

public class Producer implements Runnable {
    Company c;
    public Producer(Company c){
        this.c = c;
    }

    @Override
    public void run() {
        int i = 1;
        while(i<=10){
            try {
                c.produceItem(i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            i++;
        }
    }
}
