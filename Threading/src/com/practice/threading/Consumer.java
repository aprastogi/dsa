package com.practice.threading;

public class Consumer implements Runnable{
    Company c;
    public Consumer(Company c){
        this.c = c;
    }

    @Override
    public void run() {
        boolean exit = false;
        while(!exit){
            try {
                int n = c.consumeItem();
                if(n == 10){
                    exit = true;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
