package com.practice.threading;

public class Main {
    public static void main(String[] args) {
        Company c = new Company();
        Thread t1 = new Thread(new Producer(c));
        Thread t2 = new Thread(new Consumer(c));

        t1.start();
        t2.start();
    }
}
