package com.practice.threading;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorMain {
    static ExecutorService executorService = Executors.newFixedThreadPool(4);

    public static void main(String[] args) {
        Company c = new Company();
        executorService.submit(new Producer(c));
        executorService.submit(new Consumer(c));
        executorService.shutdown();
    }
}
