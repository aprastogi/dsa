package com.practice.threading;

public class Company {
    int n;
    boolean producer = true;

    synchronized public void produceItem(int n) throws InterruptedException {
        if(!producer)
            wait();
        this.n = n;
        System.out.println("Produced item : " + n);
        producer = false;
        notify();
    }

    synchronized public int consumeItem() throws InterruptedException {
        if(producer){
            wait();
        }
        System.out.println("Consumed item : "+ this.n);
        producer = true;
        notify();
        return n;
    }
}
