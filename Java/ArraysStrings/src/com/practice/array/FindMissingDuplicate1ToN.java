package com.practice.array;

public class FindMissingDuplicate1ToN {

	public static void swap(int [] arr, int i, int j) {
		int temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}
	
	public static void swapSort(int [] arr) {
		int i = 0;
		while(i < arr.length) {
			if(arr[i] != arr[arr[i] - 1]) 
				swap(arr,i, arr[i] -1);
			else
				i++;
		}
	}
	
	public static void main(String[] args) {
		int arr[] = {2,3,1,5,1};
        swapSort(arr);
        for(int i= 0;i<arr.length;i++) {
        	if(arr[i] != i + 1) {
        		System.out.println("Missing is : " + (i + 1) + " Duplicate is : " + arr[i]);
        	}
        }
	}

}
 