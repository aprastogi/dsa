package com.practice.interfaceequals;

public interface Equality {
    default boolean equals1(Object obj){
        return this.equals(obj);
    }
}
