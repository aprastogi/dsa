package com.practice.javaexample;

public class Main extends Parent implements I,J{

      @Override
      public void show(){
          System.out.println("Hello from Main");
      }

      public static void main(String []  args){
          Main obj = new Main();
          obj.show();
      }
}
