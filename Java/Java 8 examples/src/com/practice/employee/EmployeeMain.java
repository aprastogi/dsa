package com.practice.employee;

import java.util.*;

public class EmployeeMain {

    public static void main(String [] args){
        Employee e1 = new Employee("poorv","rastogi","9538691289");
        Employee e2 = new Employee("apoorv","rastogi","9670424789");
        List<Employee> list = new ArrayList<>();
        list.add(e1);
        list.add(e2);

        Collections.sort(list,new Comparator<Employee>(){
            @Override
            public int compare(Employee obj1, Employee obj2){
                return obj1.getFirstName().compareTo(obj2.getFirstName());
            }
        });

        Collections.sort(list);

        System.out.println(Arrays.asList(list));
        System.out.println(e1.equals(e2));
    }
}
