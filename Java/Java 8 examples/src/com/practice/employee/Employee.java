package com.practice.employee;

public class Employee implements Comparable{

    private String firstName;
    private String lastName;
    private String phoneNumber;

    public Employee(String firstName, String lastNamem, String phoneNumber) {
        this.firstName = firstName;
        this.lastName = lastNamem;
        this.phoneNumber = phoneNumber;
    }

    public String getFirstName(){
        return this.firstName;
    }



    @Override
    public boolean equals(Object obj){
        if(this == obj)
            return true;
        if(obj == null || obj.getClass() != this.getClass())
            return false;
        Employee e = (Employee) obj;
        return this.firstName.equals(e.firstName) && this.lastName.equals(e.lastName);
    }

    @Override
    public String toString(){
        return this.firstName + " " + this.lastName +
                " " + this.phoneNumber;
    }

    @Override
    public int compareTo(Object o) {
        Employee e = (Employee) o;
        if(this.firstName.equals(e.firstName))
            return 1;
        return -1;
    }
}
