package com.practice.recursion;

public class PrintPaths {

    public static void main(String[] args) {
        int [][] matrix = new int[3][3];
        printPaths(matrix, "", 0, 0);
    }

    private static void printPaths(int [][] matrix, String p, int r, int c){
        if(r == matrix.length -1 && c == matrix[0].length-1){
            System.out.println(p);
            return;
        }

        if(r < matrix.length)
            printPaths(matrix, p + "D", r +1, c);
        if(c < matrix[0].length)
            printPaths(matrix, p + "R", r, c+1);
    }
}
