package com.practice.recursion;

public class PrintPathWIthFourDirectionWithSteps {

    public static void main(String[] args) {
        int [][] matrix = new int[3][3];
        boolean [][] visited = new boolean[3][3];
        int [][] path = new int[3][3];
        printPaths(matrix, visited, path,"", 0, 0, 0);
    }

    private static void printPaths(int [][] matrix, boolean [][] visited,int [][] path, String p, int r, int c, int steps){
        if(r == matrix.length -1 && c == matrix[0].length-1){
            System.out.println(p);
            path[r][c] = steps;
            for(int i=0;i<path.length;i++){
                for(int j=0;j<path.length;j++){
                    System.out.print(path[i][j]);
                }
                System.out.println();
            }
            return;
        }
        if(visited[r][c])
            return;
        visited[r][c] = true;
        path[r][c] = steps;
        if(r < matrix.length -1)
            printPaths(matrix, visited, path,p + "D", r +1, c, steps +1);
        if(c < matrix[0].length -1)
            printPaths(matrix,visited,path, p + "R", r, c+1, steps+1);
        if(r > 0)
            printPaths(matrix,visited,path, p + "U", r-1, c, steps+1);
        if(c > 0)
            printPaths(matrix,visited,path, p + "L", r, c-1, steps+1);
        visited[r][c] = false;
        path[r][c] = 0;
    }
}
