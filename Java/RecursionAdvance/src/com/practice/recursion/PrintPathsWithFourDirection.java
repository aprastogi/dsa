package com.practice.recursion;

public class PrintPathsWithFourDirection {
    public static void main(String[] args) {
        int [][] matrix = new int[3][3];
        boolean [][] visited = new boolean[3][3];
        printPaths(matrix, visited,"", 0, 0);
    }

    private static void printPaths(int [][] matrix, boolean [][] visited, String p, int r, int c){
        if(r == matrix.length -1 && c == matrix[0].length-1){
            System.out.println(p);
            return;
        }
        if(visited[r][c])
            return;
        visited[r][c] = true;
        if(r < matrix.length -1)
            printPaths(matrix, visited,p + "D", r +1, c);
        if(c < matrix[0].length -1)
            printPaths(matrix,visited, p + "R", r, c+1);
        if(r > 0)
            printPaths(matrix,visited, p + "U", r-1, c);
        if(c > 0)
            printPaths(matrix,visited, p + "L", r, c-1);
        // backtrack
        visited[r][c] = false;
    }
}
