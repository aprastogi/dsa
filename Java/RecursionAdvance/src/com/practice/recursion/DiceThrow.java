package com.practice.recursion;

public class DiceThrow {

    public static void totalWays(int target, String p){
        if(target == 0){
            System.out.println(p);
            return;
        }
        for(int i=1;i<=6 && i <= target;i++){
            totalWays(target - i, p + i);
        }
    }

    public static void main(String[] args) {
        int target = 4;
        totalWays(target, "");
    }
}
