package com.practice.recursion;

public class ReverseNumber {

    static int rev = 0;

    public static void main(String[] args) {
        reverse(123);
        System.out.println(rev);
    }

    private static void reverse(int n){
        if(n == 0){
            return;
        }
        rev = rev * 10 + n % 10;
        reverse(n/10);
    }
}
