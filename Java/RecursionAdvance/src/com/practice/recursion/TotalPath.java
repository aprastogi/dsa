package com.practice.recursion;

public class TotalPath {

    public static int totalPath(int r, int c){
        if(r == 0 || c == 0)
            return 1;
        if(r < 0 || c < 0)
            return 0;
        return totalPath(r-1, c) + totalPath(r, c-1);
    }

    public static int totalPath1(int r, int c){
        if(r == 2 || c == 2)
            return 1;
        return totalPath1(r+1, c) + totalPath1(r, c+1);
    }


    public static void main(String[] args) {
        int r = 3;
        int c = 3;
        System.out.println(totalPath(r-1,c-1));
        System.out.println(totalPath1(0,0));
    }
}
