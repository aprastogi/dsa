package com.practice.recursion;

public class PrintSubsets {

    private static void printSubset(String p, String up){
        if(up.isEmpty()){
            System.out.println(p);
            return;
        }
        char ch = up.charAt(0);
        printSubset(p + "_" + ch, up.substring(1));
        printSubset(p + ch, up.substring(1));
    }
    public static void main(String[] args) {
        String str = "ABC";
        printSubset("", str);
    }
}
