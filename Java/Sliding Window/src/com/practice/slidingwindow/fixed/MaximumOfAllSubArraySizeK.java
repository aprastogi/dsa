package com.practice.slidingwindow.fixed;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

public class MaximumOfAllSubArraySizeK {

    private static List<Integer> solve(int [] arr, int k){
        List<Integer> list = new ArrayList<>();
        Deque<Integer> deque = new LinkedList<>();
        int i=0,j=0;
        while(j < arr.length){
            while(deque.size() > 0 && deque.getLast() < arr[j])
                deque.removeLast();
            deque.addLast(arr[j]);
            if(j - i + 1 < k)
                j++;
            else if(j - i + 1 == k){
                list.add(deque.getFirst());
                if(deque.getFirst() == arr[i])
                    deque.removeFirst();
                i++;
                j++;
            }
        }
        return list;
    }

    public static void main(String[] args) {
        int [] arr = {1,3,-1,-3,5,3,6,7};
        int k = 3;
        System.out.println(solve(arr,k));
    }
}
