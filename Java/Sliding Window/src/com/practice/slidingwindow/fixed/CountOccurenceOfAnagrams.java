package com.practice.slidingwindow.fixed;

import java.util.HashMap;
import java.util.Map;

public class CountOccurenceOfAnagrams {

    private static int solve(String str, String pattern, int k){
         Map<Character, Integer> map = new HashMap<>();
         int answer = 0;
         for(int i=0;i<pattern.length();i++)
             map.put(pattern.charAt(i), map.getOrDefault(pattern.charAt(i),0) +1);
         int count = map.size();
         int i=0, j =0;
         while(j < str.length()){
             if(map.containsKey(str.charAt(j)))
                 map.put(str.charAt(j), map.get(str.charAt(j)) - 1);
             if(map.get(str.charAt(j)) == 0)
                 count--;
             if(j - i + 1 < k)
                 j++;
             else if(j - i + 1 == k){
                 if(count == 0)
                     answer++;
                 // Slide the window
                 if(map.containsKey(str.charAt(i))){
                     map.put(str.charAt(i), map.get(str.charAt(i)) + 1);
                     if(map.get(str.charAt(i)) == 1)
                        count++;
                 }
                 i++;
                 j++;
             }
         }
         return answer;
    }

    public static void main(String[] args) {
        String str = "aabaabaa";
        String pattern = "aaba";
        System.out.println(solve(str, pattern, pattern.length()));
    }
}
