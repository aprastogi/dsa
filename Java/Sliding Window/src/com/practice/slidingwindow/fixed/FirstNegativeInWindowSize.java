package com.practice.slidingwindow.fixed;

import java.util.ArrayList;
import java.util.List;

public class FirstNegativeInWindowSize {

    private static List<Integer> solve(int [] arr, int k){
        int i =0, j= 0;
        List<Integer> answer = new ArrayList<>();
        List<Integer> negative = new ArrayList<>();
        while(j < arr.length){
            if(arr[j] < 0){
                System.out.println(arr[j]);
                negative.add(arr[j]);
            }
            if(j - i + 1 < k)
                j++;
            else if(j - i + 1 == k){
                if(negative.size() == 0)
                    answer.add(0);
                else
                    answer.add(negative.get(0));
                if(arr[i] < 0 && negative.size() > 0) {
                    negative.remove(0);
                }
                i++;
                j++;
            }
        }
        return answer;
    }

    public static void main(String[] args) {
        int [] arr = {12, -1, -2, 8, -16, 30, 16, 28};
        int k = 3;
        System.out.println(solve(arr, k));
    }
}
