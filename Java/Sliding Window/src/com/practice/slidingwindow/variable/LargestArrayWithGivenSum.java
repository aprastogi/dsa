package com.practice.slidingwindow.variable;

import java.util.Arrays;

public class LargestArrayWithGivenSum {

	public static int[] solve(int [] arr, int target) {
		int i = 0, j = 0;
		int sum = 0;
		int maxWindow = Integer.MIN_VALUE;
		int startI = 0, endI = 0;
		while(j < arr.length){
			sum = sum + arr[j];
			if(sum == target){
				if(maxWindow < j - i + 1){
					maxWindow = j - i + 1;
					startI = i;
					endI = j;
				}
				sum = sum - arr[i];
				i++;
			}
			else if(sum > target){
				while(i < j && sum > target){
					sum = sum - arr[i];
					i++;
				}
			}
			j++;
		}
		return new int []{startI, endI};
	}

	public static void main(String[] args) {
		int [] arr = {4,2,5,2,1,8};
		System.out.println(Arrays.toString(solve(arr,9)));
	}
}
