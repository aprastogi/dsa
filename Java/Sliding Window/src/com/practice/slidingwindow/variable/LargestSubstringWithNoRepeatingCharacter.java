package com.practice.slidingwindow.variable;

import java.util.HashMap;
import java.util.Map;

public class LargestSubstringWithNoRepeatingCharacter {

    public static String solve(String str){
        int i = 0, j = 0;
        Map<Character, Integer> map = new HashMap<>();
        int max = Integer.MIN_VALUE;
        int start = 0, end = 0;
        while(j < str.length()){
            map.put(str.charAt(j), map.getOrDefault(str.charAt(j), 0) + 1);
            if(map.size() == j - i + 1){
                if(max < j - i + 1){
                    max = j - i + 1;
                    start = i;
                    end = j;
                }
            }
            else if(map.size() < j - i + 1){
                while(map.size() < j - i + 1){
                    map.put(str.charAt(i), map.get(str.charAt(i)) - 1);
                    if(map.get(str.charAt(i)) == 0)
                        map.remove(str.charAt(i));
                    i++;
                }
            }
            j++;
        }
        return str.substring(start, end + 1);
    }

    public static void main(String[] args) {
        String str = "pwwkew";
        System.out.println(solve(str));
    }
}
