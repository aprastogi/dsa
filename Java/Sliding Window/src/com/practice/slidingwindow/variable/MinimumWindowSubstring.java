package com.practice.slidingwindow.variable;

import java.util.HashMap;
import java.util.Map;

public class MinimumWindowSubstring {

    public static String minWindow(String str, String pat){
        Map<Character,Integer> map = new HashMap<>();
        for(int i=0;i<pat.length();i++)
            map.put(pat.charAt(i), map.getOrDefault(pat.charAt(i), 0) + 1);
        int start = -1, end = -1;
        int minWindow = Integer.MAX_VALUE;
        int count = map.size();
        int i=0,j=0;
        while(j < str.length()){
            if(map.containsKey(str.charAt(j))){
                map.put(str.charAt(j), map.get(str.charAt(j)) - 1);
                if(map.get(str.charAt(j)) == 0)
                    count--;
            }
            if(count > 0){
                j++;
            }
            else if(count == 0){
                while(count == 0){
                    if(minWindow > j - i + 1){
                        minWindow = j - i + 1;
                        start = i;
                        end = j;
                    }
                    if(map.containsKey(str.charAt(i))){
                        map.put(str.charAt(i), map.get(str.charAt(i)) + 1);
                        if(map.get(str.charAt(i)) == 1)
                            count++;
                    }

                    i++;
                }
                j++;
            }
        }
        return start == -1 ? "" : str.substring(start, end + 1);
    }

    public static void main(String[] args) {
        String str = "ADOBECODEBANC";
        String pattern = "ABC";
        System.out.println(minWindow(str, pattern));
    }
}
