package com.practice.slidingwindow.variable;

import java.util.HashMap;
import java.util.Map;

public class LargestSubstringWithKUniqueCharacter {

    private static String largestSubString(String str, int k) {
        Map<Character,Integer> map = new HashMap<>();
        int i = 0, j = 0;
        int max = Integer.MIN_VALUE;
        int start = 0, end=0;
        while(j < str.length()){
            map.put(str.charAt(j), map.getOrDefault(str.charAt(i), 0 ) + 1);
            if(map.size() == k){
                if(max < j - i + 1){
                    max = j - i + 1;
                    end = j;
                    start = i;
                }
            }
            else if(map.size() > k) {
                while(map.size() > k){
                    map.put(str.charAt(i), map.get(str.charAt(i)) - 1);
                    if(map.get(str.charAt(i)) == 0)
                        map.remove(str.charAt(i));
                    i++;
                }
            }
            j++;
        }
        return str.substring(start, end + 1);
    }

    public static void main(String[] args) {
        String str  = "aabacbebebe";
        System.out.println(largestSubString(str, 3));
    }
}
