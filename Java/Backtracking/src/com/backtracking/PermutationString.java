package com.backtracking;

import java.util.ArrayList;
import java.util.List;

public class PermutationString {

    public static void main(String[] args) {
        String str = "ABC";
        printPermutation(str, "");
    }

    private static void printPermutation(String str, String permutation) {
        if(str.length() == 0){
            System.out.println(permutation);
            return;
        }
        for(int i=0;i<str.length();i++){
            char currentChar = str.charAt(i);
            String newString = str.substring(0, i) + str.substring(i+1);
            printPermutation(newString, permutation + currentChar);
        }
    }
}
