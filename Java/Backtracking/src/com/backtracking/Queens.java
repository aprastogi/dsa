package com.backtracking;

public class Queens {
    
int count = 0;
    
    boolean isSafe(int [][] board,int row,int col,int n){
        for(int i = row;i< n;i++){
            if(board[i][col] == 1)
                return false;
        }
        for(int i=row;i>=0;i--){
            if(board[i][col] == 1)
                return false;
        }
        for(int i=col;i< n;i++){
            if(board[row][i] == 1)
                return false;
        }
        for(int i=col;i>=0;i--){
            if(board[row][i] == 1)
                return false;
        }
        for(int i=row,j=col;i>=0 && j>=0;i--,j--){
            if(board[i][j] == 1)
                return false;
        }
        for(int i=row,j=col;i<n && j>=0;i++,j--){
            if(board[i][j] == 1)
                return false;
        }
        return true;
    }
    
    void solve(int [][] board,int col,int n){
        if(col==n){
            count++;
            return;
        }
        for(int row = 0;row< n;row++){
            if(isSafe(board,row,col,n)){
                board[row][col] = 1;
                solve(board,col+1,n);
                board[row][col] = 0;
            }
        }        
    }
    
    public int totalNQueens(int n) {
        int board [][] = new int[n][n];
        solve(board,0,n);
        return count;
    }
    
    public int getSum(int n){
        int sum = 0;
        while(n>0){
            sum = (int) (sum + Math.pow(n%10, 2));
            n/=10;
        }
        return sum;
    }
    
    public boolean isHappy(int n) {
        int result = 0;
        while(n > 1){
            result = getSum(n);
            n = result;
        }
        if(n == 1)
            return true;
        return false;
    }
    
	public static void main(String[] args) {
		Queens que = new Queens();
		System.out.println(que.totalNQueens(4));
        System.out.println(que.isHappy(2));
	}

}
