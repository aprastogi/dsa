package com.backtracking;

import java.util.ArrayList;

public class NQueens {
    
    public boolean isSafe(ArrayList<String> board, int row, int col, int n){
        for(int j=0;j<n;j++){
            if(board.get(row).charAt(j) == 'Q')
              return false;
        }
        
        for(int i=0;i<n;i++){
            if(board.get(i).charAt(col) == 'Q')
              return false;
        }
        
        int i=row, j=col;
        while(i>=0 && j>=0){
            if(board.get(i).charAt(j) == 'Q')
               return false;
            i--; j--;
        }
        
        i=row; j=col;
        while(i<n && j>=0){
            if(board.get(i).charAt(j) == 'Q')
               return false;
            i++; j--;
        }
        return true;
    } 

    public boolean solveUtil(ArrayList<ArrayList<String>> result, ArrayList<String> board, int col, int a){
        if(col == a){
            result.add(board);
            return true;
        }
        
        for(int row = 0; row < a; row++){
            if(isSafe(board,row,col,a)){
                 String str = board.get(row);
                 str = str.substring(0, col) + 'Q' + str.substring(col + 1); 
                 board.set(row,str);
                 solveUtil(result, board, col+1, a);
                 String str1 = board.get(row);
                 str1 = str1.substring(0, col) + '.' + str1.substring(col + 1); 
                 board.set(row,str1);
            }
        }
        return false;
    }
    
    public ArrayList<ArrayList<String>> solve(int a){
       ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();
       ArrayList<String> board = new ArrayList<String>();
       for(int i=0;i<a;i++){
            String s = "";
            for(int j=0;j<a;j++)
               s += '.';
            board.add(s);
        }
        solveUtil(result, board, 0, a);
        return result;
    }
    public ArrayList<ArrayList<String>> solveNQueens(int a) {
        ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();
        if(a==2 || a==3)
            return result;
        result = solve(a);
        return result;
    }

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		NQueens obj = new NQueens();
		ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();
		result = obj.solveNQueens(4);
		System.out.println(result);
	}
}
