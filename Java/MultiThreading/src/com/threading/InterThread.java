package com.threading;

class Q{
	int num;
	boolean valueSet = false;
	
	public synchronized void put(int num) throws Exception {
		while(valueSet) {
		    wait();
		}
		System.out.println("Put : " + num);
		this.num = num;
		valueSet = true;
		notify();
	}
	
	public synchronized void get() throws InterruptedException {
		while(!valueSet) {
			wait();
		}
		System.out.println("Get : " + num);
		valueSet = false;
		notify();
	}
}

class Producer implements Runnable{
	Q q;

	public Producer(Q q) {
		this.q = q;
		Thread t = new Thread(this, "Producer");
		t.start();
	}	
	
	public void run(){
		int i =0;
		while(true) {
			try {
				q.put(i++);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try { Thread.sleep(1000); } catch(Exception ex) {};
		}
	}
}

class Consumer implements Runnable{
	Q q;

	public Consumer(Q q) {
		this.q = q;
		Thread t = new Thread(this, "Consumer");
		t.start();
	}
	
	public void run() {
		while(true) {
			try {
				q.get();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try { Thread.sleep(1000); } catch(Exception ex) {};
		}
	}
}

public class InterThread {

	public static void main(String[] args) {
		Q q = new Q();
		new Producer(q);
		new Consumer(q);
	}
}
