package com.threading;

class Hii implements Runnable{
	public void run() {
		for(int i = 1; i<=5;i++) {
		   System.out.println("Hi");
		   try {
			  Thread.sleep(1000);
		   } 
		   catch (InterruptedException e) {
			e.printStackTrace();
		   }
		}
	}
}

class  Helloo implements Runnable {
	public void run() {
	  for(int i = 1; i<=5;i++) {
		System.out.println("Hello");
		try {
			  Thread.sleep(1000);
		   } 
		   catch (InterruptedException e) {
			e.printStackTrace();
		   }
		}
	}
}

public class ThreadRunnable {

	public static void main(String[] args) throws InterruptedException {
		Hii obj1 = new Hii();
		Hello obj2 = new Hello();
		Thread t1 = new Thread(obj1);
		Thread t2 = new Thread(obj2);
		t1.start();
		Thread.sleep(10);
		t2.start();
		t1.join();
		t2.join();
		System.out.println("Bye");

	}

}
