package com.lrucache;

import java.util.HashMap;
import java.util.Map;

public class LRUCache {

    Map<Integer,Node> map;
    Node head = new Node(0,0);
    Node tail = new Node(0,0);
    int size;

    public LRUCache(int size){
        map = new HashMap<>();
        this.size = size;
        head.next = tail;
        tail.prev = head;
    }

    public int get(int key){
        if(!map.containsKey(key))
            return -1;
        Node node = map.get(key);
        remove(node);
        insert(node);
        return node.value;
    }

    public void put(int key, int value){
        if(map.containsKey(key)) {
            remove(map.get(key));
        }
        if(map.size() == size) {
            remove(tail.prev);
        }
        insert(new Node(key, value));
    }

    public void remove(Node node){
        map.remove(node.key);
        node.prev.next = node.next;
        node.next.prev = node.prev;
    }

    public void insert(Node node){
        map.put(node.key, node);
        node.next = head.next;
        node.next.prev = node;
        head.next = node;
        node.prev = head;
    }

    public static void main(String[] args) {
        LRUCache obj = new LRUCache(2);
        obj.put(1,1);
        obj.put(2,2);
        System.out.println(obj.get(1));
        obj.put(3,3);
        System.out.println(obj.map.entrySet());
    }
}
