package com.lrucache;

public class Node {
    Node prev, next;
    int key, value;

    public Node(int key, int value){
        this.key = key;
        this.value = value;
    }

    @Override
    public String toString() {
        return "Node{" +
                ", key=" + key +
                ", value=" + value +
                '}';
    }
}
