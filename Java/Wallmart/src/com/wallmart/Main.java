package com.wallmart;

/*
Create a school attendance system (no need to use databases, everything in memory) . You can assume there is only 1 school. There are 12 classes in school, a teacher can have only 1 class or no classes assigned to them. Each class can have multiple students upto max 20 per class.

Write a Java program to -

Create a singleton class School,

School
Teacher
Class
Students

Each teacher and student will have few common properties -

 - name
 - age
 - gender
 - class they belong to

Teachers will have additional info like salary, experience years.
Students will have additional info like 1 parent name

A Class should know about the teacher, students, and their attendance per day.

Apart from this each Teacher can take attendance of their class students for a particular date.

School should have a function print which should prnt the entire school structure in following format -

 - class1 name
- teacher name
- student 1 name
- student 2 name
..
..
..
- last student name

- class2 name
- teacher name
..
..
..


Testing

-  Function to create a school, all 12 Classes, 15 teachers, assign 12 teachers to 12 Classes randomly, and create 20 students per Class.

2 - print the entire structure of the school

3 - write a function which adds random attendance for a particular date for all the students.

And looking at a Class, I should be able to tell the number of absentee students for a particular date.

 */
public class Main {
    public static void main(String[] args) {

    }
}
