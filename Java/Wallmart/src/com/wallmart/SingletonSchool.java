package com.wallmart;

public class SingletonSchool {
    private static School schoolInstance;

    private SingletonSchool(){}

    public static School getSchoolInstance(){
        if(schoolInstance == null) {
            synchronized (SingletonSchool.class) {
                if (schoolInstance == null)
                    schoolInstance = new School();
            }
        }
        return schoolInstance;
    }
}
