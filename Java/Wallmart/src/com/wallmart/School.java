package com.wallmart;

import java.util.List;
import java.util.Map;

public class School {
    List<Teacher> teacherList;
    List<Class> classList;

    void print(){
        for(Class claas1 : classList){
            System.out.println(claas1.getClassName());
            System.out.println("--");
            System.out.println(claas1.getTeacher().getName());
            System.out.println("----");
            List<Student> studentList = claas1.getStudentList();
            for(Student student: studentList)
                System.out.println(student);
        }
    }
}
