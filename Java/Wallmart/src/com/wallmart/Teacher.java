package com.wallmart;

import java.util.List;
import java.util.Map;

public class Teacher extends Person {
    private Double salary;
    private Integer yearOfExperience;
    private Class aClass;
    private Map<String, Map<Student, Boolean>> attendanceMap;

    public Double getSalary() {
        return salary;
    }

    public Integer getYearOfExperience() {
        return yearOfExperience;
    }

    public Class getaClass() {
        return aClass;
    }

    public void takeAttendance(String date, List<Student> studentList){
        attendanceMap.put(date, studentList);
    }

    public Map<String, List<Student>> getAttendanceMap(){
        return attendanceMap;
    }
}
