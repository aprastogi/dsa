package com.wallmart;

import java.util.List;

public class Class {
    String className;
    List<Student> studentList;
    Teacher teacher;

    public String getClassName(){
        return className;
    }

    public Teacher getTeacher(){
        return teacher;
    }

    public List<Student> getStudentList(){
        return studentList;
    }
}
