package com.practice.stack;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class NextsmallerElementToLeft {

	public static List<Integer> NSL(int [] arr){
		List<Integer> result = new ArrayList<Integer>();
		Stack<Integer> st = new Stack<>();
		st.push(arr[0]);
		result.add(-1);
		for(int i=1;i<arr.length;i++) {
			while(!st.isEmpty() && st.peek() >= arr[i])
				st.pop();
			if(st.isEmpty())
				result.add(-1);
			else
				result.add(st.peek());
			st.push(arr[i]);
		}
		return result;
	}
	
	public static void main(String[] args) {
		int arr[] = {1,3,2,4};
        List<Integer> result = new ArrayList<>();
		result = NSL(arr);
        for(int i=0;i<result.size();i++)
        	System.out.print(result.get(i)+ " ");
	}

}
