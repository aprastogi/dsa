package com.practice.stack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

public class Histogram {

	public static List<Integer> nextSmallRight(int [] arr){
		List<Integer> result = new ArrayList<Integer>();
		Stack<Integer> st = new Stack<Integer>();
		st.push(arr.length-1);
		result.add(arr.length);
		for(int i=arr.length-2;i>=0;i--) {
			while(!st.isEmpty() && arr[i] <= arr[st.peek()])
				st.pop();
			if(st.isEmpty())
				result.add(arr.length);
			else
				result.add(st.peek());
			st.add(i);
			   
		}
		Collections.reverse(result);
		return result;
	}
	
    public static List<Integer> nextSmallLeft(int [] arr){
    	List<Integer> result = new ArrayList<Integer>();
    	Stack<Integer> st = new Stack<>();
    	st.push(0);
    	result.add(-1);
    	for(int i=1;i<arr.length;i++) {
			while(!st.isEmpty() && arr[st.peek()] >= arr[i])
				st.pop();
			if(st.isEmpty())
				result.add(-1);
			else
				result.add(st.peek());
			st.push(i); 
    	}
		return result;
	}

	public static void main(String[] args) {
		int[] arr = {6,2,5,4,5,1,6};
		List<Integer> left  = nextSmallLeft(arr);
		List<Integer> right = nextSmallRight(arr);
		List<Integer> width = new ArrayList<Integer>();
		List<Integer> area = new ArrayList<Integer>();
		for(int i=0;i<left.size();i++)
			width.add(right.get(i) - left.get(i) - 1);
		for(int i=0;i<width.size();i++)
			area.add(arr[i] * width.get(i));
		System.out.println(Collections.max(area));


	}
}