package com.practice.stack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

public class NextGreaterElementToRight {

	public static List<Integer> NGR(int [] arr) {
		List<Integer> result = new ArrayList<>();
		Stack<Integer> st = new Stack<>();
		st.push(arr[arr.length-1]);
		result.add(-1);
		for(int i=arr.length-2;i>=0;i--) {
			while(!st.isEmpty() && arr[i] >= st.peek())
				st.pop();
			if(st.isEmpty())
				result.add(-1);
			else
				result.add(st.peek());
			st.add(arr[i]);
			   
		}
		return result;
	}
	
	public static void main(String[] args) {
		int arr[] = {1,3,2,4};
        List<Integer> result = new ArrayList<>();
		result = NGR(arr);
		Collections.reverse(result);
        for(int i=0;i<result.size();i++)
        	System.out.print(result.get(i)+ " ");
	}
}
