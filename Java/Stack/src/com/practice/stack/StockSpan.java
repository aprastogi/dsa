package com.practice.stack;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

class Pair{
	int first;
	int second;
	
	public Pair(int first,int second) {
		this.first = first;
		this.second = second;
	}
}

public class StockSpan {

	public static List<Integer>  NGL(int[] arr) {
		List<Integer> result = new ArrayList<Integer>();
		Stack<Pair> st = new Stack<Pair>();
		st.push(new Pair(arr[0],0));
		for(int i=0;i<arr.length;i++) {
			while(!st.isEmpty() && st.peek().first <= arr[i])
				st.pop();
			if(st.isEmpty())
				result.add(-1);
			else
				result.add(st.peek().second);
			st.push(new Pair(arr[i], i));
		}
		for(int i=0;i<result.size();i++) {
			result.set(i, i - result.get(i));
		}
		return result;
	}
	
	public static void main(String[] args) {
		int[] arr = {100,80,60,70,60,75,85};
		List<Integer> result;
		result = NGL(arr);
		for (Integer integer : result) System.out.print(integer + " ");
	}

}

/*
 
 100 80 60 70 60 75 85
 
 */
