package com.practice.stack;


public class TrapingRainWater {

	public static int trw(int [] arr) {
		int area = 0;
		int[] left = new int[arr.length];
		int[] right = new int[arr.length];
		left[0] = 0;
		right[right.length-1] = 0;
		for(int i=1;i<left.length;i++)
			left[i] = Math.max(arr[i-1], left[i-1]);		
		for(int i=right.length-2;i>=0;i--)
			right[i] = Math.max(arr[i+1], right[i+1]);
		for(int i=0;i<arr.length;i++)
			area += Math.max((Math.min(left[i], right[i]) - arr[i]), 0);
		return area;
	}
	
	public static void main(String[] args) {
		int[] arr = {3,0,0,2,0,4};
		System.out.println(trw(arr));
	}
}
