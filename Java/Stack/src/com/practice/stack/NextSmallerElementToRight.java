package com.practice.stack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

public class NextSmallerElementToRight {

	public static List<Integer> NSR(int [] arr) {
		List<Integer> result = new ArrayList<>();
		Stack<Integer> st = new Stack<>();
		for(int i=arr.length-1;i>=0;i--) {
			if(st.isEmpty())
				result.add(-1);
			else if(arr[i] > st.peek())
				result.add(st.peek());
			else {
				while(!st.isEmpty() && arr[i] <= st.peek())
					st.pop();
				if(st.isEmpty())
					result.add(-1);
				else
					result.add(st.peek());
			}
			st.add(arr[i]);
			   
		}
		return result;
	}
	
	public static void main(String[] args) {
		int arr[] = {1,3,2,4};
        List<Integer> result = new ArrayList<>();
		result = NSR(arr);
		Collections.reverse(result);
        for(int i=0;i<result.size();i++)
        	System.out.print(result.get(i)+ " ");
	}

}
