package com;

import java.util.Arrays;

public class intuit {
    public static void main(String[] args) {
        int [] arr = {2,3,1,4,5};
        System.out.println(Arrays.toString(getSubarray(arr, 9)));
    }

    /*
    2   3  1  4   5
    start = 1
    end = 3
    sum = 10 -2 = 9
     */
    private static int[] getSubarray(int[] arr, int target) {
        int start =0, end = 0;
        int sum = 0;
        while (end < arr.length){
            if(sum == target){
                return new int[]{start, end};
            }
            sum += arr[end];
            if(sum > target){
                sum -= arr[start];
                start++;
            }else {
                end++;
            }
        }
        return new int[]{};
    }
}
