package com.practice.heap;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

/*Given an integer array nums and an integer k, return the k most frequent elements. You may return the answer in any order.
https://leetcode.com/problems/top-k-frequent-elements/
*/

public class KFrequentNumbers {

	public static List<Integer> kFreq(int [] arr, int k){
		List<Integer> result = new ArrayList<>();
		HashMap<Integer,Integer> map = new HashMap<>();
		// Create a min heap
		PriorityQueue<Pair> que = new PriorityQueue<>(k, Comparator.comparingInt(o -> o.first));
		for (int j : arr) map.put(j, map.getOrDefault(j, 0) + 1);
		for(Map.Entry<Integer,Integer> entry : map.entrySet()) {
			que.add(new Pair(entry.getValue(),entry.getKey()));
			// If heap size is greater than k remove the top element from heap as it will never be in our answer
			if(que.size() > k)
				que.poll();
		}
		while(que.size() > 0)
			result.add(que.poll().second);
		return result;
	}
	
	public static void main(String[] args) {
		int[] arr = {1,1,1,3,2,2,4};
		int k = 2;
		List<Integer> result = kFreq(arr,k);
		System.out.println(result);
	}
}
