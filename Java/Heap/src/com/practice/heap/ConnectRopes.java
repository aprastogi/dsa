package com.practice.heap;

import java.util.PriorityQueue;

//Given are N ropes of different lengths, the task is to connect these ropes into one rope with minimum cost,
// such that the cost to connect two ropes is equal to the sum of their lengths.
public class ConnectRopes {

	public static int minCost(int [] arr) {
		int cost = 0;
		// Create a min heap
		PriorityQueue<Integer>  que = new PriorityQueue<>();
		for(int ele : arr)
			que.add(ele);
		while(que.size() >= 2) {
			int first = que.poll();
			int second = que.poll();
		    cost += first + second;
		    que.add(first + second);
		}
		return cost;
	}
	
	public static void main(String[] args) {
		int[] arr = {1,2,3,4,5};
		System.out.println(minCost(arr));
	}
}
