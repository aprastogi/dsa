package com.practice.heap;

//Sum of k1 smallest and k2 the smallest composite numbers in the array
public class SumOfElements {

	public static int sumOfEle(int [] arr, int k1, int k2) {
		int sum = 0;
		int first = KthSmallestElement.kthSmallest(arr, k1);
		int second = KthSmallestElement.kthSmallest(arr, k2);
		for (int num : arr) {
			if (num > first && num < second)
				sum += num;
		}
		return sum;
	}
	
	public static void main(String[] args) {
		int [] arr = {1,3,12,5,15,11};
		int k1 = 3, k2 =6;
		System.out.println(sumOfEle(arr,k1,k2));
	}

}
