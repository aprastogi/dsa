package com.practice.heap;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

/*
Given an array of points where points[i] = [xi, yi] represents a point on the X-Y plane and an integer k, return the k closest points to the origin (0, 0).
The distance between two points on the X-Y plane is the Euclidean distance (i.e., √(x1 - x2)2 + (y1 - y2)2).
You may return the answer in any order. The answer is guaranteed to be unique (except for the order that it is in).
https://leetcode.com/problems/k-closest-points-to-origin/
 */
class Pair1{
	int first;
	Pair pair;
	
	public Pair1(int first, Pair pair) {
		this.first = first;
		this.pair = pair;
	}
}

public class KClosetPointToOrigin {
	public static List<List<Pair>> kClosetNo(int [][] arr, int k){
		List<List<Pair>> result = new ArrayList<>();
		//Create a max heap
		PriorityQueue<Pair1> que = new PriorityQueue<>(k, (o1, o2) -> o2.first - o1.first);
		for (int[] ints : arr) {
			// adding distance a key and co-ordinates as value to max heap
			que.add(new Pair1((ints[0] * ints[0]) + (ints[1] * ints[1]), new Pair(ints[0], ints[1])));
			if (que.size() > k)
				que.poll();
		}
		while(que.size() > 0) {
			Pair p = que.poll().pair;
			List<Pair> list = new ArrayList<>();
			list.add(p);
			result.add(list);
		}
		return result;
	}
	
	public static void main(String[] args) {
		int[][] arr = {{1,3},{-2,2},{5,8},{0,1}};
		int k = 2;
		List<List<Pair>> result = kClosetNo(arr,k);
		System.out.println(result);
	}
}
