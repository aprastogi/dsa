package com.practice.heap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

// Print the elements of an array in the decreasing frequency
public class FrequencySort {

	public static List<Integer> freqSort(int [] arr){
		List<Integer> result = new ArrayList<>();
		HashMap<Integer,Integer> map = new HashMap<>();
		// Create a max heap
		PriorityQueue<Pair> que = new PriorityQueue<>(100, (o1, o2) -> o2.first - o1.first);
		for (int num : arr){
			map.put(num, map.getOrDefault(num, 0) + 1);
		}
		// Add all elements in max heap based on the frequency
		for(Map.Entry<Integer,Integer> entry : map.entrySet()) {
			que.add(new Pair(entry.getValue(),entry.getKey()));
		}

		// Pop elements from the heap and put it in the result list
		while(que.size() > 0) {
			int freq = que.peek().first;
			int ele = que.peek().second;
			for(int i=1;i<=freq;i++)
			    result.add(ele);
			que.poll();
		}
		return result;
	}
	
	public static void main(String[] args) {
		int[] arr = {1,1,1,3,2,2,4};
		List<Integer> result =  freqSort(arr);
		System.out.println(result);
	}
}
