package com.practice.heap;

import java.util.Collections;
import java.util.PriorityQueue;

/*
Find K’th Smallest Element in Unsorted Array
 */

public class KthSmallestElement {

	public static int kthSmallest(int [] arr, int k) {
		// Create a max heap
		PriorityQueue<Integer> que = new PriorityQueue<>(Collections.reverseOrder());
		for (int num : arr) {
			que.add(num);
			// If heap size is greater than k remove the top element from heap as it will never be in our answer
			if (que.size() > k)
				que.poll();
		}
		if(que.size() == 0)
			return -1;
		return que.peek();
	}
	
	public static void main(String[] args) {
		int[] arr = {7,10,4,3,20,15};
		int k = 3;
		System.out.println(kthSmallest(arr,k));
	}

}
