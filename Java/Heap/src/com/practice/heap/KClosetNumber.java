package com.practice.heap;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

//Find k closest elements to a given value
public class KClosetNumber {

	public static List<Integer> kClosetNo(int [] arr, int k , int x){
		List<Integer> result = new ArrayList<>();
		// Create a max heap
		PriorityQueue<Pair> que = new PriorityQueue<>(k, (o1, o2) -> o2.first - o1.first);
		for (int num : arr) {
			que.add(new Pair(Math.abs(num - x), num));
			// If heap size is greater than k remove the top element from heap as it will never be in our answer
			if (que.size() > k)
				que.poll();
		}
		while(que.size() > 0)
			result.add(que.poll().second);
		return result;
	}
	
	public static void main(String[] args) {
		int[] arr = {5,6,7,8,9};
		int k = 3, x = 7;
		List<Integer> result = kClosetNo(arr,k,x);
		System.out.println(result);
	}
}
