package com.practice.heap;

public class Pair {
    int first,second;

    public Pair(int first, int second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public String toString() {
        return "Pair = {" +
                first +
                "," + second +
                '}';
    }
}
