package com.practice.heap;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

//k largest elements in an array
public class KLargestElements {

	public static List<Integer> kLargest(int [] arr, int k){
		List<Integer> result = new ArrayList<>();
		// Create a min heap
		PriorityQueue<Integer> que = new PriorityQueue<>();
		for (int num : arr) {
			que.add(num);
			// If heap size is greater than k remove the top element from heap as it will never be in our answer
			if (que.size() > k)
				que.poll();
		}
		while(que.size() > 0)
			result.add(que.poll());
		return result;
	}
	
	public static void main(String[] args) {
		int[] arr = {7,10,4,3,20,15};
		int k = 3;
		List<Integer> result = kLargest(arr,k);
		System.out.println(result);
	}

}
