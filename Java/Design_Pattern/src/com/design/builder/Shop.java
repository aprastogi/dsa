package com.design.builder;

public class Shop {

	public static void main(String[] args) {
		Phone p = new PhoneBuilder().setOs("Android").setCamera(48).setProcessor("Nouget").getPhone();
		System.out.println(p);
	}

}
