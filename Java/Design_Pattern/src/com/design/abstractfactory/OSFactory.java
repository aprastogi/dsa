package com.design.abstractfactory;

public class OSFactory {
      public OS getinstance(AbstractFactory af) {
    	  return af.getinstance();
      }
}
