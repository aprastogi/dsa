package com.design.abstractfactory;


public class FactoryMain {

	public static void main(String[] args) {
		OSFactory osf = new OSFactory();
		OS instance = osf.getinstance(new WindowsFactory());
		instance.spec();
	}

}
