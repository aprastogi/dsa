package com.design.abstractfactory;

public class IOS implements OS {

	@Override
	public void spec() {
		System.out.println("Welcome to IOS");
	}

}
