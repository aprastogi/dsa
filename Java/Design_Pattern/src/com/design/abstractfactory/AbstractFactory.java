package com.design.abstractfactory;

public interface AbstractFactory {
     public OS getinstance();
}
