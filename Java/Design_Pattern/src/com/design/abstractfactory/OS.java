package com.design.abstractfactory;

public interface OS {
     public void spec();
}
