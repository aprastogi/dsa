package com.design.singleton;

public class BillPughSingleton {
       
	private BillPughSingleton() {};
	String msg = "Implementation of BillPugh Singleton Pattern";
	private static class BillSingleton {
		private static final BillPughSingleton instance  = new BillPughSingleton();
	}
	public static BillPughSingleton getInstance() {
		return BillSingleton.instance;
	}
}
