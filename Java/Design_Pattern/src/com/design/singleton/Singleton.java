package com.design.singleton;

public class Singleton {
    
    private static Singleton instance;
    private Singleton() {};
    String msg = "Implementation of Singleton Pattern";
    public static Singleton getinstance() {
    	if(instance == null) {
    		synchronized (Singleton.class) {
				if(instance == null) {
					instance = new Singleton();
				}
			}    		
    	}
    	return instance;
    }
}
