package com.design.singleton;

public class BillPughSingletonMain {

	public static void main(String[] args) {		
		BillPughSingleton instance = BillPughSingleton.getInstance();
		System.out.println(instance.msg);
	}

}
