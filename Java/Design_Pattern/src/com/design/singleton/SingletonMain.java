package com.design.singleton;

public class SingletonMain {
	public static void main(String[] args) {		
		Singleton instance = Singleton.getinstance();
		System.out.println(instance.msg);
	}
}
