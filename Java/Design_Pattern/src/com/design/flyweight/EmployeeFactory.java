package com.design.flyweight;

import java.util.HashMap;

public class EmployeeFactory {
	
	 private static HashMap<EmployeeType,Employee> map = new HashMap<EmployeeType, Employee>();
	 
     public static Employee getEmployee(EmployeeType type) {
    	 Employee employee = null;
    	 if(map.get(type) != null) {
    		 employee = map.get(type);
    	 }
    	 else {
    		 switch(type) {
    		   case DEVELOPER: 
    			   System.out.println("Developer Created");
    			   employee = new Developer();
    			   break;
    		   case TESTER:
    			   System.out.println("Tester Created");
    			   employee = new Tester();
    			   break;
    		   default:
    			   System.out.println("No Such Employee");
    		 }
    		 map.put(type,employee);
    	 }    	 
    	 return employee;
     }
}
