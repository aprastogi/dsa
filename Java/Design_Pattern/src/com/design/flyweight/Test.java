package com.design.flyweight;

public class Test {
    
	static int id = 1;
	
	public int uniqueId() {
		return id++;
	}
	public static void main(String[] args) {
		Test t = new Test();
		Test t1 = new Test();
		for(int i=0;i<10;i++)
			System.out.println(t.uniqueId());
		System.out.println(t1.uniqueId());
	}

}
