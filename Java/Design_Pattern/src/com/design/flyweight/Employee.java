package com.design.flyweight;

public interface Employee {
      public void assignSkill(String name);
      public void task();
}
