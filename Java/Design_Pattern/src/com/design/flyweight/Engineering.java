package com.design.flyweight;

import java.util.Random;

public class Engineering {
    
	private static EmployeeType employeeType[] = {EmployeeType.DEVELOPER, EmployeeType.TESTER};
	private static String skills[] = {"Java","C++","C#","SpringBoot"};
	
	public static EmployeeType getRandEmployee() {
		Random r = new Random();
		int randInt = r.nextInt(employeeType.length);
		return employeeType[randInt];
	}
	
	public static String getRandSkill() {
		Random r = new Random();
		int randInt = r.nextInt(skills.length);
		return skills[randInt];
	}
	
	public static void main(String[] args) {
		for(int i = 1;i<=10;i++) {
			Employee e = EmployeeFactory.getEmployee(getRandEmployee());
			e.assignSkill(getRandSkill());
			e.task();
		}
	}
}
