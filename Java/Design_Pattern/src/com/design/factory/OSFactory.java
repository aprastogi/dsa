package com.design.factory;

public class OSFactory {
      public OS getinstance(String str) {
    	  if(str.contentEquals("A"))
    		  return new Android();
    	  else if(str.contentEquals("W"))
    		  return new Windows();
    	  else
    		  return null;
    
      }
}
