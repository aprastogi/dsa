package com.design.factory;

public class FactoryMain {
	public static void main(String[] args) {
         OSFactory osf = new OSFactory();
         OS obj = osf.getinstance("W");
         obj.spec();
	}

}
