package com.design.chainofresponse;

public class ATMDispenseChain {
	
	private DispenseChain c1;
	private DispenseChain c2;
	private DispenseChain c3;

	public ATMDispenseChain() {
		// initialize the chain
		this.c1 = new Dollar50Dispenser();
		this.c2 = new Dollar20Dispenser();
		this.c3 = new Dollar10Dispenser();

		// set the chain of responsibility
		c1.setNextChain(c2);
		c2.setNextChain(c3);
	}

	public static void main(String[] args) {
		ATMDispenseChain atmDispenser = new ATMDispenseChain();
		int amount = 530;
		if (amount % 10 != 0) {
			System.out.println("Amount should be in multiple of 10s.");				
		}
		else {
		// process the request
		atmDispenser.c1.dispense(new Currency(amount));
		}
	}
}
