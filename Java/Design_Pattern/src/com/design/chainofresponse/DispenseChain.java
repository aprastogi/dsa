package com.design.chainofresponse;

public interface DispenseChain {
	
    void setNextChain(DispenseChain nextChain);	
	void dispense(Currency cur);
}
