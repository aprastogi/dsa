package com.design.observer;

public interface Observer {

	void update();

	void subscriberChannel(Channel ch);

}