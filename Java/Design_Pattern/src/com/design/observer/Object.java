package com.design.observer;

public interface Object {

	void subscribe(Subscriber sub);

	void unsubscribe(Subscriber sub);

	void notifySUbscriber();

	void upload(String title);

}