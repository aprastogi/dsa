package com.design.observer;

public class Subscriber implements Observer {
      private String name;
      private Channel channel = new Channel();
      
      @Override
	  public void update() {
    	  System.out.println("Hey "+ name + " Video uploaded with title " + channel.title);  
      }     
      
      public Subscriber(String name) {
		super();
		this.name = name;
	  }

	  @Override
	  public void subscriberChannel(Channel ch) {
    	  channel = ch;
      }
      
}
