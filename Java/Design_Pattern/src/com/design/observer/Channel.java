package com.design.observer;

import java.util.ArrayList;
import java.util.List;

public class Channel implements Object {
       private List<Subscriber> subs = new ArrayList<>();
       public String title;
       
       @Override
	   public void subscribe(Subscriber sub) {
    	   subs.add(sub);
       }
       
       @Override
	   public void unsubscribe(Subscriber sub) {
    	   subs.remove(sub);
       }
       
       @Override
	   public void notifySUbscriber() {
    	     for(Subscriber sub: subs) {
    	    	  sub.update();
    	     }
       }
       
       @Override
	   public void upload(String title) {
    	   this.title = title;
    	   notifySUbscriber();
       }
}
