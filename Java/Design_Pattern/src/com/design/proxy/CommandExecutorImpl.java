package com.design.proxy;

public class CommandExecutorImpl implements CommandExecutor {

	@Override
	public void runCommand(String cmd) {
		System.out.println("Command Executed Successfully");		
	}

}
