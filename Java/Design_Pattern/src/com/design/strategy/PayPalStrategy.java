package com.design.strategy;

public class PayPalStrategy implements PaymentStrategy {
	
	 private String emailId;
	 private String password;
		
     public PayPalStrategy(String emailId, String password) {
		super();
		this.emailId = emailId;
		this.password = password;
	 }     
     
     public String getEmailId() {
		return emailId;
	 }

	 public void setEmailId(String emailId) {
		this.emailId = emailId;
	 }

	 public String getPassword() {
		return password;
	 }

	 public void setPassword(String password) {
		this.password = password;
	 }


	public void pay(int amount) { 	 
    	 System.out.println("Amount " + amount + " Paid by PayPal");
     }
}
