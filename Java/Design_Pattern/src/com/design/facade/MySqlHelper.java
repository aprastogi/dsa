package com.design.facade;

import java.sql.Connection;

public class MySqlHelper {
       public static Connection getMySqlConnection() {
    	   return null;
       }
       
       public void generateMySqlPDFReport(String tableName, Connection con) {
    	     System.out.println("Generated Mysql PDF Report");
       }
       
       public void generateMySqlHTMLReport(String tableName, Connection con) {
    	     System.out.println("Generate Mysql HTML Report");
       }
} 
