package com.design.facade;

public enum DBType {
     MYSQL, ORACLE;
}
