package com.design.facade;


public class FacadeMain {

	public static void main(String[] args) {
		
		String tableName = "Employee";
		
		//generating MySql HTML report and Oracle PDF report without using Facade
		/*Connection con = MySqlHelper.getMySqlConnection();
		MySqlHelper msh = new MySqlHelper();
		msh.generateMySqlHTMLReport(tableName, con);
			
		Connection con1 = MySqlHelper.getMySqlConnection();
		OracleHelper oh = new OracleHelper();
		oh.generateOracleHTMLReport(tableName, con1);*/
		
		//generating MySql HTML report and Oracle PDF report using Facade
		HelperFacade.generateReport(DBType.MYSQL, ReportType.HTML, tableName);

	}

}
