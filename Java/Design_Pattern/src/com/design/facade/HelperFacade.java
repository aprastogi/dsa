package com.design.facade;

import java.sql.Connection;

public class HelperFacade {
      public static void generateReport(DBType dbType, ReportType reportType, String tableName) {
    	  Connection con = null;
    	  switch(dbType) {
    	    case MYSQL:
    	    	 con = MySqlHelper.getMySqlConnection();
    	    	 MySqlHelper msh = new MySqlHelper();
    	    	 switch(reportType) {    	    	  
    	    	    case HTML:
    	    	    	msh.generateMySqlHTMLReport(tableName, con);
    	    	    break;
    	    	    case PDF:
    	    	    	msh.generateMySqlPDFReport(tableName, con);
    	    	    break;
    	    	 }
    	    break;
    	    case ORACLE:
    	    	con = OracleHelper.getOracleConnection();
   	    	    OracleHelper oh = new OracleHelper();
   	    	    switch(reportType) {    	    	  
   	    	    case HTML:
   	    	    	oh.generateOracleHTMLReport(tableName, con);
   	    	    break;
   	    	    case PDF:
   	    	    	oh.generateOraclePDFReport(tableName, con);
   	    	    break;
   	    	 }
    	  } 
      }
      
      
}
