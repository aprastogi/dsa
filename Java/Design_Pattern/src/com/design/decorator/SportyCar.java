package com.design.decorator;

public class SportyCar extends CarDecorator {
	public SportyCar(Car c) {
		super(c);
	}
	
	public void assemble() {
		super.assemble();
		System.out.println("Adding Sporty Car Features");
	}
}
