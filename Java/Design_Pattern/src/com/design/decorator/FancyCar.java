package com.design.decorator;

public class FancyCar extends CarDecorator {
	public FancyCar(Car c) {
		super(c);
	}
	
	public void assemble() {
		super.assemble();
		System.out.println("Adding Fancy Car Features");
	}

}
