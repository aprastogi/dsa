package com.design.decorator;

public class CarDecorator implements Car {
	protected Car c;
	
	public CarDecorator(Car c) {
		this.c = c;
	}
	
    public void assemble() {
    	this.c.assemble();
    }

}
