package com.design.decorator;

public class DecoratorMain {
    public static void main(String args[]) {
    	FancyCar fancy = new FancyCar(new BasicCar());
    	fancy.assemble();	    	
    }
} 
