package com.design.templateMethod;

public class WoodenHouse extends HouseTemplate {

	@Override
	public void buildWalls() {
		System.out.println("Building Wooden Walls");
	}

	@Override
	public void buildPillers() {
		System.out.println("Building Pillars with Wood coating");
	}

}
