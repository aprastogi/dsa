package com.design.templateMethod;

public class GlassHouse extends HouseTemplate {

	@Override
	public void buildWalls() {
		System.out.println("Building Glass Walls");
	}

	@Override
	public void buildPillers() {
		System.out.println("Building Pillars with glass coating");
	}

}
