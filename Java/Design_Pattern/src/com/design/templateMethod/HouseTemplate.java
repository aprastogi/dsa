package com.design.templateMethod;

public abstract class HouseTemplate {
	
      public final void buildHouse() {
    	  buildFoundation();
    	  buildPillers();
    	  buildWalls();    	  
    	  buildWindows();
    	  System.out.println("House is built");
      }
      
      public void buildWindows() {
    	  System.out.println("Building Glass Windows");
      }
      
      public void buildFoundation() {
    	  System.out.println("Building foundation with cement,iron rods and sand");
      }
      
      public abstract void buildWalls();
      public abstract void buildPillers();
      
}
