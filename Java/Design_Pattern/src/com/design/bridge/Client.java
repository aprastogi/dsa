package com.design.bridge;

public class Client {

	public static void main(String[] args) {
		TV sonyOldRemote = new Sony(new OldRemote());
		sonyOldRemote.on();
		sonyOldRemote.off();
		System.out.println();
		
		TV sonyNewRemote = new Sony(new NewRemote());
		sonyNewRemote.on();
		sonyNewRemote.off();
		System.out.println();
		
		TV lgOldRemote = new LG(new OldRemote());
		lgOldRemote.on();
		lgOldRemote.off();
		System.out.println();
		
		TV lgNewRemote = new LG(new NewRemote());
		lgNewRemote.on();
		lgNewRemote.off();
		System.out.println();

	}
}