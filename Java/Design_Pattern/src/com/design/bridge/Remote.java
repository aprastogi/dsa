package com.design.bridge;

public interface Remote {
      public void on();
      public void off();
}
