package com.design.bridge;

public class LG extends TV {

	Remote remoteType;
	
	public LG(Remote r) {
		super(r);
		this.remoteType = r;
	}

	@Override
	void on() {
		System.out.println("LG TV ON : ");
		remoteType.on();
	}

	@Override
	void off() {
		System.out.println("LG TV OFF : ");
        remoteType.off();
	}

}
