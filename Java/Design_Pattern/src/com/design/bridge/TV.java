package com.design.bridge;

public abstract class TV {
      Remote remote;
      
      public TV(Remote r) {
    	  this.remote = r;
      }
      
      abstract void on();
      abstract void off();
}
