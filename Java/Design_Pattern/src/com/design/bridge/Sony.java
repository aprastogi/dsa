package com.design.bridge;

public class Sony extends TV {

	Remote remoteType;
	public Sony(Remote r) {
		super(r);
		this.remoteType = r;
	}

	@Override
	void on() {
        System.out.println("Sony TV ON : ");
        remoteType.on();
	}

	@Override
	void off() {
		System.out.println("Sony TV OFF: ");
        remoteType.off();
	}

}
