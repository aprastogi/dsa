package com.design.composite;

public class CompositeTest {

	public static void main(String[] args) {
		
		Component hd = new Leaf(4000, "HardDrive");
		Component mouse = new Leaf(500, "Mouse");
		Component monitor = new Leaf(10000, "Monitor");
		Component ram = new Leaf(2000, "RAM");
		Component cpu = new Leaf(6000, "CPU");
		
		Composite ph = new Composite("Peripherals");
		Composite cabinet = new Composite("Cabinet");
		Composite mb = new Composite("Motherboard");
		Composite computer = new Composite("Computer");
		
		mb.addComponenet(cpu);
		mb.addComponenet(ram);
		
		ph.addComponenet(mouse);
		ph.addComponenet(monitor);
		
		cabinet.addComponenet(hd);
		cabinet.addComponenet(mb);
		
		computer.addComponenet(ph);
		computer.addComponenet(cabinet);
		
		computer.showPrice();
	}

}
