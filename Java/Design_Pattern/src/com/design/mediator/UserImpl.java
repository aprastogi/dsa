package com.design.mediator;

public class UserImpl extends User {

	public UserImpl(ChatMediator chatMediator, String name) {
		super(chatMediator, name);
	}

	@Override
	public void send(String msg) {
        System.out.println("Sending Message : " + msg);
        mediator.sendMsg(msg, this);
	}

	@Override
	public void receive(String msg) {
         System.out.println(this.name + " Received Message " + msg);
	}

}
