package com.design.mediator;

public interface ChatMediator {
      public void sendMsg(String Msg, User User);
      public void addUser(User u1);
}
