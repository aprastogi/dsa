package com.design.mediator;

import java.util.ArrayList;
import java.util.List;

public class ChatMediatorImpl implements ChatMediator {

	private List<User> users;

	public ChatMediatorImpl() {
		super();
		this.users = new ArrayList<>();
	}

	@Override
	public void sendMsg(String msg, User user) {
		for(User u : this.users) {
			if(u != user) {
				u.receive(msg);
			}
		}
	}

	@Override
	public void addUser(User u1) {
		this.users.add(u1);
	}

}
