package com.design.adapter;

public interface Pen {
     public void write(String str);
}
