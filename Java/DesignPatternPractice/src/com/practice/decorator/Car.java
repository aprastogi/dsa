package com.practice.decorator;

public interface Car {
     void assemble();
}
