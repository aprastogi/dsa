package com.practice.decorator;

public class DecorateCar implements Car {
    
	Car c;
	
	public DecorateCar(Car c) {
	   this.c = c;
	}
	
	@Override
	public void assemble() {
		this.c.assemble();		
	}

}
