package com.practice.decorator;

public class FancyCar extends DecorateCar {

	public FancyCar(Car c) {
		super(c);
	}
	
	public void assemble() {
		super.assemble();
		System.out.println("Adding Fancy Car Features");
	}

}
