package com.practice.decorator;

public class SportyCar extends DecorateCar {

	public SportyCar(Car c) {
		super(c);
	}
    
	public void assemble() {
		super.assemble();
		System.out.println("Adding sporty features");
	}
}
