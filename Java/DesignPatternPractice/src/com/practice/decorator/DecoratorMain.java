package com.practice.decorator;

public class DecoratorMain {

	public static void main(String[] args) {
		FancyCar fancy = new FancyCar(new SportyCar(new BasicCar()));
		fancy.assemble();
	}

}
