package com.practice.prototype;

public class Prototype {

	public static void main(String[] args) throws CloneNotSupportedException {
		BookShop shop = new BookShop();
		shop.setShopName("Novelty");		
		shop.loadData();
		

		BookShop shop2 = (BookShop) shop.clone();
		shop2.setShopName("FamillyMall");
		shop.getBooks().remove(2);
		System.out.println(shop);
		System.out.println(shop2);
	}

}
