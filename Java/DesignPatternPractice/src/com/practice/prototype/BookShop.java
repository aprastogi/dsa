package com.practice.prototype;

import java.util.ArrayList;
import java.util.List;

public class BookShop implements Cloneable {
	
    private String shopName;
    List<Book> list = new ArrayList<Book>();
     
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public List<Book> getBooks() {
		return list;
	}
	public void setBooks(List<Book> list) {
		this.list = list;
	}
	
	@Override
	public String toString() {
		return "BookShop [shopName=" + shopName + ", list=" + list + "]";
	}
    
	public void loadData() {
		for(int i=1;i<=5;i++) {
			Book book = new Book();
			book.setId(i);
			book.setName("Book-" + i);
			getBooks().add(book);
		}
	}
	
	@Override
	protected BookShop clone() throws CloneNotSupportedException {
		BookShop shop = new BookShop();
		for(Book b : getBooks()) {
			shop.getBooks().add(b);
		}
		return shop;
	}
     
}
