package com.practice.singleton;

public class Singleton {
	
     private static Singleton instance;
     private Singleton() {};
     String msg = "Singleton Implementation";
     
     public static Singleton getInstance() {
    	 if(instance == null) {
    		 synchronized(Singleton.class) {
    			 if(instance == null) {
    				 instance = new Singleton();
    			 }
    		 }
    	 }
    	 return instance;
     }
}
