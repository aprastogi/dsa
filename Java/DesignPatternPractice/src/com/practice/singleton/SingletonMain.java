package com.practice.singleton;

public class SingletonMain {

	public static void main(String[] args) {
		Singleton instance = Singleton.getInstance();
		System.out.println(instance.msg);
	}
}
