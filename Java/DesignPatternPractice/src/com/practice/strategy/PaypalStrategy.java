package com.practice.strategy;

public class PaypalStrategy implements PaymentStrategy {
	
    private String username;
    private String password;   
    
	public PaypalStrategy(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public void pay(int amount) {
		System.out.println(amount + " paid by paypal");		
	}         
}
