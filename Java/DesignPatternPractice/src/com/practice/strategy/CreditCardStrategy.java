package com.practice.strategy;

public class CreditCardStrategy implements PaymentStrategy {
	
	private String cvv;
	private String cardNumber;
	private String expiryDate;	
	
	
	public CreditCardStrategy(String cvv, String cardNumber, String expiryDate) {
		super();
		this.cvv = cvv;
		this.cardNumber = cardNumber;
		this.expiryDate = expiryDate;
	}

	public String getCvv() {
		return cvv;
	}

	public void setCvv(String cvv) {
		this.cvv = cvv;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public void pay(int amount) {
		System.out.println(amount + " paid by CreditCard");	
	}

}
