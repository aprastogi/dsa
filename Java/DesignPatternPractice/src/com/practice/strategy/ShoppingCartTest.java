package com.practice.strategy;

public class ShoppingCartTest {

	public static void main(String[] args) {
		ShoppingCart sc = new ShoppingCart();
        sc.addItem(new Item(123, "Keyboard", 1000));
        sc.addItem(new Item(124, "Mouse", 500));
        
        sc.pay(new CreditCardStrategy("123","343847", "12/05"));
        sc.pay(new PaypalStrategy("apoorv", "rastogi"));
	}

}
