package com.practice.strategy;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {
     List<Item> items = new ArrayList<Item>();
     int amount = 0;
     
     public void addItem(Item i) {
    	 this.items.add(i);
     }
     
     public void removeItem(Item i) {
    	 this.items.remove(i);
     }
     
     public void calulateToal() {
    	 for(Item i : this.items)
    		 amount += i.getPrice();
     }
     
     public void pay(PaymentStrategy ps) {
    	 calulateToal();
    	 ps.pay(amount);
     }
}
