package com.practice.strategy;

public interface PaymentStrategy {
     void pay(int amount);
}
