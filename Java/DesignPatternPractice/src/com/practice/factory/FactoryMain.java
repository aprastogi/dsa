package com.practice.factory;

public class FactoryMain {

	public static void main(String[] args) throws Exception {
		Factory f = new Factory();
		OS osf = f.getInstance("I");
		osf.spec();
	}
}
