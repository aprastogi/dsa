package com.practice.factory;

public class Factory {
     public OS getInstance(String str) throws Exception {
    	 if(str.equals("A"))
    		 return new Android();
    	 else if(str.equals("I"))
    		 return new IOS();
    	 else
    		 throw new Exception("Invalid argument !!!");
     }
}
