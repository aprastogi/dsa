package com.practice.observer;

public class Youtube {

	public static void main(String[] args) {
		Channel learning = new Channel();
		
		Subscriber s1 = new Subscriber("Apoorv");
		Subscriber s2 = new Subscriber("Kishan");
		Subscriber s3 = new Subscriber("Shubham");
		Subscriber s4 = new Subscriber("Saurabh");
		
		learning.subscribe(s1);
		learning.subscribe(s2);
		learning.subscribe(s3);
		learning.subscribe(s4);
		
		learning.unsubscribe(s2);
		
		s1.subscriberChannel(learning);
		s2.subscriberChannel(learning);
		s3.subscriberChannel(learning);
		s4.subscriberChannel(learning);
		
		learning.upload("Spring boot");

	}

}
