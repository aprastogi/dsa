package com.practice.observer;

public class Subscriber {
	
     private String name;
     Channel ch = new Channel();
     
     public Subscriber(String name) {
		super();
		this.name = name;
	}

	 public void update() {
    	 System.out.println("Hi " + this.name + " video uploaded with title " + ch.title);
     }
     
     public void subscriberChannel(Channel ch) {
    	 this.ch = ch;
     }
}
