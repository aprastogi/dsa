package com.practice.observer;

import java.util.ArrayList;
import java.util.List;

public class Channel {
     String title;
     
     List<Subscriber> subs = new ArrayList<Subscriber>();
     
     public void subscribe(Subscriber sub) {
    	 this.subs.add(sub);
     }
     
     public void unsubscribe(Subscriber sub) {
    	 this.subs.remove(sub);
     }
     
     public void notifySubs() {
    	 for(Subscriber sub : subs) {
    		 sub.update();
    	 }
     }
     
     public void upload(String title) {
    	 this.title = title;
    	 notifySubs();
     }
}
