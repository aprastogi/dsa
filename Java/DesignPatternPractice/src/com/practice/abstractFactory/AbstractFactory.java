package com.practice.abstractFactory;

public interface AbstractFactory {
      public OS getinstance();
}
