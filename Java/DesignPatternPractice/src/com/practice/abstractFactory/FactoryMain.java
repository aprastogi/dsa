package com.practice.abstractFactory;

public class FactoryMain {

	public static void main(String[] args) {
	    AbstractFactory af = new IOSFactory();
		OS os = af.getinstance();
		os.spec();
	}
}
