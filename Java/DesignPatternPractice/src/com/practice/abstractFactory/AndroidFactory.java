package com.practice.abstractFactory;

public class AndroidFactory implements AbstractFactory {

	@Override
	public OS getinstance() {
		return new Android();
	}

}
