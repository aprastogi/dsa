package com.practice.builder;

public class Phone {
    private String os;
    private String battery;
    private String ram;
    private String camera;
    
    public Phone(String os, String battery, String ram, String camera) {
    	 this.os = os;
    	 this.battery = battery;
    	 this.camera = camera;
    	 this.ram = ram;
    }

	@Override
	public String toString() {
		return "Phone [os=" + os + ", battery=" + battery + ", ram=" + ram + ", camera=" + camera + "]";
	}
    
    
}
