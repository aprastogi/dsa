package com.practice.builder;

public class Shop {

	public static void main(String[] args) {
		Phone pb = new PhoneBuilder().setBattery("6000").setOs("android").getPhone();
		System.out.println(pb);

	}

}
