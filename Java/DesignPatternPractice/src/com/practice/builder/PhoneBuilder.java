package com.practice.builder;

public class PhoneBuilder {
	private String os;
    private String battery;
    private String ram;
    private String camera;
    
	public PhoneBuilder setOs(String os) {
		this.os = os;
		return this;
	}
	public PhoneBuilder setBattery(String battery) {
		this.battery = battery;
		return this;
	}
	public PhoneBuilder setRam(String ram) {
		this.ram = ram;
		return this;
	}
	public PhoneBuilder setCamera(String camera) {
		this.camera = camera;
		return this;
	}
    
	public Phone getPhone() {
		return new Phone(os, battery, ram, camera);
	}
    
}
