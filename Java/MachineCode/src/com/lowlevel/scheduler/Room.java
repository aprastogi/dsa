package com.lowlevel.scheduler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Room {

    private String name;
    HashMap<Integer,List<Meeting>> calander;
    
    public Room(String name) {
    	this.name = name;
    	this.calander = new HashMap<Integer, List<Meeting>>();
    }
    
    public String getName() {
    	return name;
    }
    
    public boolean book(int day, int start, int end) {
    	if(!calander.containsKey(day))
    		calander.put(day, new ArrayList<Meeting>());
    	for(Meeting m : calander.get(day)) {
    		if(m.getStart() < end && start < m.getEnd())
    			return false;
    	}
    	Meeting meeting = new Meeting(start,end, this);
    	List<Meeting> curr = new ArrayList<Meeting>();
    	if(calander.containsKey(day)) {
    		curr = calander.get(day);
    		curr.add(meeting);
    		calander.put(day, curr);
    	}
    	else {
    		curr.add(meeting);
    		calander.put(day, curr);
    	}
    	return true;
    }
}
