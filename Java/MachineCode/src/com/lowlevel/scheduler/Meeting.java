package com.lowlevel.scheduler;

public class Meeting {
	
     private int start, end;
     Room room;
     
     public Meeting(int start, int end, Room room) {
    	 this.start = start;
    	 this.end = end;
    	 this.room = room;
     }
     
	 public int getStart() {
	  	return start;
	 }
	 
	 public int getEnd() {
		return end;
	 } 
}
