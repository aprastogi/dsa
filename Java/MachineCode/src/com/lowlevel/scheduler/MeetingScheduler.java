package com.lowlevel.scheduler;

import java.util.ArrayList;
import java.util.List;

public class MeetingScheduler {

	public static void main(String[] args) {
		Room room1 = new Room("room1");
		Room room2 = new Room("room2");
		Room room3 = new Room("room3");
		
		List<Room> rooms = new ArrayList<Room>();
		rooms.add(room1);
		rooms.add(room2);
		rooms.add(room3);
		
        Scheduler sc = new Scheduler(rooms);
        System.out.println(sc.book(15,2, 5)); //room1
        System.out.println(sc.book(15,5, 8)); //room1
        System.out.println(sc.book(15,4, 8)); //room2
        System.out.println(sc.book(15,3, 6)); //room3
        System.out.println(sc.book(15,7, 8)); //room3
        System.out.println(sc.book(16,6, 9)); //not available
        
	}

}
