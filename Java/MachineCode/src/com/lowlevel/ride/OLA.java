package com.lowlevel.ride;

public class OLA {

	public static void main(String[] args) {
		Rider rider = new Rider("Lucifer");
		Driver driver = new Driver("John");
		
		rider.createRide(1, 50, 60, 1);
		System.out.println("Pay " + rider.closeRide(1) +" amount to " + driver.getName());
		rider.updateRide(1, 50, 60, 2);
		
		System.out.println("***************************************");
		
		rider.createRide(1, 50, 60, 1);
		rider.withdrawRide(1);
		rider.updateRide(1, 50, 60, 2);
		System.out.println("Pay " + rider.closeRide(1) +" amount to " + driver.getName());
		
		System.out.println("***************************************");
		
		rider.createRide(1, 50, 60, 1);
		rider.updateRide(1, 50, 60, 2);
		System.out.println("Pay " + rider.closeRide(1) +" amount to " + driver.getName());
	}

}
