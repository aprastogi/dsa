package com.lowlevel.ride;

public class Ride {
	
	static final int AMT_PER_KM = 20;
	
    private int id;
    private int origin;
    private int destination;
    private int seats;
    private RideStatus rideStatus;
    
    public Ride() {
    	this.id = this.origin = this.destination  = this.seats = 0;
    	this.rideStatus = RideStatus.IDLE;
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getOrigin() {
		return origin;
	}

	public void setOrigin(int origin) {
		this.origin = origin;
	}

	public int getDestination() {
		return destination;
	}

	public void setDestination(int destination) {
		this.destination = destination;
	}

	public int getSeats() {
		return seats;
	}

	public void setSeats(int seats) {
		this.seats = seats;
	}

	public RideStatus getRideStatus() {
		return rideStatus;
	}

	public void setRideStatus(RideStatus rideStatus) {
		this.rideStatus = rideStatus;
	}
    
    public double calculateFare(boolean isPriorityRider) {
    	int dist = destination - origin;
    	if(seats < 2)
    		return dist * AMT_PER_KM * (isPriorityRider == true ? 0.75 : 1);
    	return dist * seats * AMT_PER_KM * (isPriorityRider == true ? 0.50 : 0.75);
    }
    
    
}
