package com.lowlevel.ride;

public enum RideStatus {
    IDLE,CREATED,WITHDRAWN,CLOSED
}
