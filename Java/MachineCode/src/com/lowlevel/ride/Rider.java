package com.lowlevel.ride;

import java.util.ArrayList;
import java.util.List;

public class Rider {
	
     private List<Ride> completedRide = new ArrayList<Ride>();
     private Ride currentRide;
     private String riderName;
     
     public Rider(String riderName) {
    	 this.riderName = riderName;
    	 this.currentRide = new Ride();
     }
     
     public void createRide(int id, int origin, int destination, int seats) {
    	 if(origin >= destination) {
    		 System.out.println("Wrong values provided for origin and destination !!!");
    		 return;
    	 }
    	 System.out.println("Hi " + riderName + " ride is created/updated successfully");
    	 currentRide.setId(id);
    	 currentRide.setOrigin(origin);
    	 currentRide.setDestination(destination);
    	 currentRide.setSeats(seats);
    	 currentRide.setRideStatus(RideStatus.CREATED);
     }

     public void updateRide(int id, int origin, int destination, int seats) {
    	 if(currentRide.getRideStatus() == RideStatus.WITHDRAWN) {
    		 System.out.println("Ride is withdrawn !!!");
    		 return;
    	 }
    	 if(currentRide.getRideStatus() == RideStatus.CLOSED) {
    		 System.out.println("Ride is already Closed !!!");
    		 return;
    	 }
    	 createRide(id, origin, destination, seats);
     }

     public void withdrawRide(int id) {
    	 if(currentRide.getId() != id) {
    		 System.out.println("Id is wrong !!!");
    		 return;
    	 }
    	 if(currentRide.getRideStatus() != RideStatus.CREATED) {
    		 System.out.println("Ride is not in progress !!!");
    		 return;
    	 }
    	 currentRide.setRideStatus(RideStatus.WITHDRAWN);
     }
     
     public double closeRide(int id) {
    	 if(currentRide.getRideStatus() != RideStatus.CREATED) {
    		 System.out.println("Ride is not in progress !!!");
    		 return 0;
    	 }
    	 currentRide.setRideStatus(RideStatus.CLOSED);
    	 completedRide.add(currentRide);
    	 return currentRide.calculateFare(completedRide.size() >= 10);
     }
}
