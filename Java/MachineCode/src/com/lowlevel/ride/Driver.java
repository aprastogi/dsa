package com.lowlevel.ride;

public class Driver {
     
	private String driverName;
	
	public Driver(String driverName) {
		this.driverName = driverName;
	}
	
	public String getName() {
		return driverName;
	}
}
