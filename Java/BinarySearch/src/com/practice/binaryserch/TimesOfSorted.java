package com.practice.binaryserch;

// Find the Rotation Count in Rotated Sorted array
public class TimesOfSorted {

	public static int timesOfSorted(int [] arr) {
		int start = 0, end = arr.length-1;
		while(start <= end) {
			int mid = start + (end - start)/2;
			// Check if the array is already sorted
			if(arr[start] < arr[end])
				return start;
			// Doing the modulo operation here as we don't want to out of bound
			if(arr[mid] < arr[(mid+1)%arr.length] && arr[mid] < arr[(mid + arr.length - 1)%arr.length])
			    return mid;
			if(arr[start] < arr[mid])
				start = mid + 1;
			else
				end = mid - 1;		
			}
		return -1;
	}
	
	public static void main(String[] args) {
		int [] arr = {11,12,15,2,5,6,8};
		System.out.println(timesOfSorted(arr));

	}

}
