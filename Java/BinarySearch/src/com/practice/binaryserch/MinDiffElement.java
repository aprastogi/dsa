package com.practice.binaryserch;

//Find the min difference element for a given element in input
public class MinDiffElement {

	public static int binarySearchAsc(int [] arr, int start, int end, int element) {
		while(start <= end) {
			int mid = start + (end - start)/2;
			if(arr[mid] == element)
				return arr[mid];
			if(arr[mid] < element)
				start = mid + 1;
			else
				end = mid -1;
		}
		return Math.abs(arr[start] - element) < Math.abs(arr[end] - element) ? arr[start] : arr[end];
	}
	
	public static void main(String[] args) {
		int [] arr = {1,3,10,15};
		int key = 12;
		int ans = binarySearchAsc(arr, 0, arr.length-1, key);
		System.out.println(ans);
	}
}
