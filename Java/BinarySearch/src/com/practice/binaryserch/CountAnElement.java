package com.practice.binaryserch;

// Return the count of target element in sorted array
public class CountAnElement {

	public static int count(int [] arr,int ele) {
		// Get the first occurrence of the target
		int first = FirstLastOccurrence.firstOccurrence(arr, ele);
		// Get the last occurrence of the target
		int last  = FirstLastOccurrence.lastOccurrence(arr, ele);
		if(first == - 1 && last == -1)
			return 0;
		// final count of the target element
		return last - first + 1;
	}
	
	public static void main(String[] args) {
		int[] arr = {2,4,10,10,10,15,15};
		System.out.println(count(arr,10));
	}
}
