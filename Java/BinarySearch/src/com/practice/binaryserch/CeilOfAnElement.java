package com.practice.binaryserch;

public class CeilOfAnElement {

	public static int findCeil(int [] arr, int element) {
		int res = 0;
		int start = 0, end = arr.length -1 ;
		while(start <= end) {
			int mid = start + (end - start)/2;
			if(arr[mid] == element)
				return mid;
			// If current element is less than the target element then store this as answer and search for a more accurate ceil if available
			if(arr[mid] > element) {
				res = arr[mid];
				end = mid - 1;
			}
			else
				start = mid + 1;
		}
		return res;
	}
	
	public static void main(String[] args) {
		int[] arr = {1,2,3,4,8,10,10,12,19};
		System.out.println(findCeil(arr,5));
	}

}
