package com.practice.binaryserch;

import java.util.ArrayList;
import java.util.List;

/*
Given an n x n matrix and an integer x, find the position of x in the matrix if it is present.
The designed algorithm should have linear time complexity
 */
public class SearchRowColWise {

	public static List<Integer> searchRowCol(int [][] arr, int element) {
		int i = 0 , j = arr[0].length-1;
		List<Integer> result = new ArrayList<Integer>();
		while(i < arr.length && j >= 0) {
			if(arr[i][j] == element) {
				result.add(i);
				result.add(j);
				return result;
			}
			else if(arr[i][j] < element)
		        i++;
			else
				j--;
		}
		return result;
	}
	
	public static void main(String[] args) {
		int[][] arr = {{10,20,30,40},
				       {15,25,35,45},
				       {27,29,37,48},
				       {32,33,39,50}};
		List<Integer> result = searchRowCol(arr,37);
		if(result.size() == 0)
		    System.out.println("Not Found !!!");
		else
		    System.out.println(result.get(0) + "," +  result.get(1));
	}

}
