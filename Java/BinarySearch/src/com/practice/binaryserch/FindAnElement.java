package com.practice.binaryserch;

// Find an element in a Rotated Sorted Array
public class FindAnElement {

	public static int find(int [] arr, int start, int end,int element) {
		if(start > end)
			return -1;
		int mid = start + (end - start)/2;
		if(arr[mid] == element)
			return mid;
		// Check if the left part of the array is sorted
		if(arr[start] < arr[mid]) {
			// element is available in left or right part of the array?
			if(arr[start] <= element && arr[mid] > element)
				return find(arr,start,mid-1,element);
			return find(arr,mid+1,start,element);
		}
		// Check if the right part of the array is sorted
		else {
			if(arr[mid] < element && arr[end] >= element)
				return find(arr,mid+1,end,element);
			return find(arr,start,mid-1,element);
		}
	}
	
	public static void main(String[] args) {
		int [] arr = {11,12,15,2,5,6,8};
		System.out.println(find(arr,0,arr.length-1,2));
	}
}
