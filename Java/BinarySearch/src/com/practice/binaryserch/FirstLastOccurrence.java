package com.practice.binaryserch;

//Find first and last positions of an element in a sorted array
public class FirstLastOccurrence {

	public static int firstOccurrence(int [] arr, int ele) {
		int start = 0, end = arr.length -1;
		int res = -1;
		while(start <= end) {
			int mid = start + (end - start)/2;
			// if we found the element at mid then store this in result as this could be our final answer and continue search in the first part as we will only find the
			// answer in first part in this case
			if(arr[mid] >= ele) {
				res = mid;
				end = mid - 1;
			}
			else {
				// search in the first part of array
				start = mid + 1;
			}
		}
		return res;
	}
	
	public static int lastOccurrence(int [] arr, int ele) {
		int start = 0, end = arr.length -1;
		int res = - 1;
		while(start <= end) {
			int mid = start + (end - start)/2;
			// if we found the element at mid then store this in result as this could be our final answer and continue search in the second part as we will only find the
			// answer in second part in this case
			if(arr[mid] <= ele) {
				res = mid;
				start = mid + 1;
			}
			else {
				// search in the first part of array
				end = mid - 1;
			}
		}
		return res;
	}
	
	public static void main(String[] args) {
		int[] arr = {2,4,10,10,10,15,20};
		System.out.println(firstOccurrence(arr,10));
		System.out.println(lastOccurrence(arr,10));
	}

}
