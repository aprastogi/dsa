package com.practice.recursion;

import java.util.Stack;

public class sortAStack {

	public static void insert(Stack<Integer> st, int ele) {
		if(st.size() == 0 || st.peek() <= ele) {
			st.push(ele);
			return;
		}
		int temp = st.pop();
		insert(st, ele);
		st.push(temp);
	}
	
	public static void sort(Stack<Integer> st) {
		if(st.size() == 1)
			return;
		int temp = st.pop();
		sort(st);
		insert(st,temp);
	}
	
	public static void main(String[] args) {
		Stack<Integer> st = new Stack<Integer>();
		st.push(2);
		st.push(1);
		st.push(9);
		st.push(5);
        sort(st);
        while(st.size() > 0){
            System.out.print(st.pop() + " ");
        }
	}

}
