package com.practice.recursion;

import java.util.Stack;

public class DeleteMiddleFromStack {

	public static void delete(Stack<Integer> st, int k) {
		if(k == 1) {
			st.pop();
			return;
		}
		int temp = st.pop();
		delete(st,k-1);
		st.push(temp);
	}
	
	public static void main(String[] args) {
		Stack<Integer> st = new Stack<Integer> ();
        st.push(1);
        st.push(2);
        st.push(3);
        st.push(4);
        st.push(5);
        int k = 1 + (st.size()/2);
        delete(st,k);
        while(st.size() > 0)
        	System.out.print(st.pop() + " ");
	}

}
