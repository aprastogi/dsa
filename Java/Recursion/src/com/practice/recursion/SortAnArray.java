package com.practice.recursion;

import java.util.ArrayList;
import java.util.List;

public class SortAnArray {

	public static void insert(List<Integer> list, int ele) {
		if(list.size() == 0 || ele > list.get(list.size() -1)) {
			list.add(ele);
			return;
		}
		int num = list.remove(list.size() -1);
		insert(list,ele);
		list.add(num);
	}
	
	public static void sort(List<Integer> list) {
		if(list.size() == 1) 
			return;
		int num = list.remove(list.size()-1);
		sort(list);
		insert(list,num);
	}
	
	public static void main(String[] args) {
		List<Integer> list = new ArrayList<Integer>();
		list.add(2);
		list.add(1);
		list.add(9);
		list.add(5);
		sort(list);
		for(int i= 0;i<list.size();i++)
		   System.out.print(list.get(i) + " ");
		
	}

}
