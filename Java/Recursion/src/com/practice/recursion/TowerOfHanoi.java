package com.practice.recursion;

public class TowerOfHanoi {

	static void solve(int s, int d, int h, int n) {
		if(n == 1) {
			System.out.println("Moving plate " + n + " From " + s + " To " + d);
			return;
		}
		solve(s,h,d,n-1);
		System.out.println("Moving plate " + n + " From " + s + " To " + d);
		solve(h,d,s,n-1);
	}
	
	public static void main(String[] args) {
		int n = 3, s = 1, h = 2, d =3;
		solve(s,d,h,n);
	}

}
