package com.practice.recursion;

import java.util.Stack;

public class ReverseStack {

	public static void insert(Stack<Integer> st, int ele) {
		if(st.size() == 0) {
			st.push(ele);
			return;
		}
		int temp = st.pop();
		insert(st,ele);
		st.push(temp);
	}
	
	public static void reverse(Stack<Integer> st) {
		if(st.size() == 1)
			return;
		int temp = st.pop();
		reverse(st);
		insert(st,temp);
	}
	
	public static void main(String[] args) {
		Stack<Integer> st = new Stack<Integer> ();
        st.push(1);
        st.push(2);
        st.push(3);
        st.push(4);
        st.push(5);
        reverse(st);
        while(st.size() > 0)
        	System.out.print(st.pop() + " ");
	}

}
