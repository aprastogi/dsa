package com.practice.recursion;

public class PermutationWithSpace {

	static void solve(String input, String output) {
		if(input.length() == 0) {
			System.out.println(output);
			return;
		}
		String out1 = output;
		String out2 = output;
		out1 += input.charAt(0);
		out2 += "_" + input.charAt(0);
		StringBuilder sb = new StringBuilder(input);
		input = sb.deleteCharAt(0).toString();
		solve(input,out1);
		solve(input,out2);
	}
	
	public static void main(String[] args) {
		String str = "ABC";
		StringBuilder sb = new StringBuilder(str);
		String output = str.charAt(0)+"";
		sb.deleteCharAt(0);
		solve(sb.toString(),output);
	}

}
