package com.practice.recursion;

public class PermutationWithCase {

	static void solve(String input, String output) {
		if(input.length() == 0) {
			System.out.println(output);
			return;
		}
		String out1 = output; 
		String out2 = output;
		out1 += input.charAt(0);
		out2 += Character.toUpperCase(input.charAt(0));
		StringBuilder sb = new StringBuilder(input);
		sb.deleteCharAt(0);
		solve(sb.toString(),out1);
		solve(sb.toString(),out2);
	}
	
	public static void main(String[] args) {
		String str = "ab";
		solve(str,"");
	}

}
