package com.practice.recursion;

public class PrintSubsets {

	static void printSubSet(String input, String output) {
		if(input.length() == 0) {
			System.out.println(output);
			return;
		}
		String out1 = output;
		String out2 = output;
		out2 += input.charAt(0);
		StringBuilder sb = new StringBuilder(input);
		sb.deleteCharAt(0);
		printSubSet(sb.toString(), out1);
		printSubSet(sb.toString(), out2);
	}
	
	public static void main(String[] args) {
		String str = "abc";
        printSubSet(str,"");
	}

}
