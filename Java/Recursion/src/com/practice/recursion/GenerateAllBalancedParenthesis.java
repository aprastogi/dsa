package com.practice.recursion;

import java.util.ArrayList;
import java.util.List;

public class GenerateAllBalancedParenthesis {

	static List<String> result = new ArrayList<>();
	
	public static void solve(int open, int close, String output) {
		if(open == 0 && close == 0) {
			result.add(output);
			return;
		}
		if(open != 0) {
			String out1 = output;
			out1 += "(";
			solve(open-1,close, out1);
		}
		if(close > open) {
			String out2 = output;
			out2 += ")";
			solve(open,close-1,out2);
		}
	}
	
	public static void main(String[] args) {
		int n= 3;
		int open = n, close = n;
		String output = "";
		solve(open,close,output);
		for(int i=0;i<result.size();i++)
			System.out.println(result.get(i));
	}

}
