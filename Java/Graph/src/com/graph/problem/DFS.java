package com.graph.problem;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Stack;

public class DFS {
	int V;   
    ArrayList<Integer>[] adj;
    // Constructor 
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public DFS(int V) 
    { 
        this.V = V; 
        adj = new ArrayList[V];
        for (int i=0; i< V; ++i) 
            adj[i] = new ArrayList(); 
    } 
    
    void addEdge(int v, int w) 
    { 
        adj[v].add(w);
    }
    
    void dfs(int s)
    {
        boolean []visited=new boolean[V];
        Arrays.fill(visited, false);
        Stack<Integer> st = new Stack<Integer>();
        visited[s]=true;
        st.push(s);
        while(!st.empty())
        {
            s=st.pop();
            System.out.print(s + " ");
            Iterator<Integer> it = adj[s].listIterator(); 
            while (it.hasNext()) 
            { 
                int element = it.next(); 
                if (!visited[element]) 
                {
                    visited[element] = true;
                    st.push(element);
                }                   
            }            
        }
    }

    void dfsUtil(int s, boolean [] visited){
        visited[s] = true;
        System.out.print(s + " ");
        for(int element : adj[s]){
            if(!visited[element])
              dfsUtil(element, visited);
        }
    }
	public static void main(String[] args) {
		DFS g = new DFS(4);		  
        g.addEdge(0, 1); 
        g.addEdge(0, 2); 
        g.addEdge(1, 2); 
        g.addEdge(2, 0); 
        g.addEdge(2, 3); 
        g.addEdge(3, 3);   
        System.out.println("Following is Depth First Traversal "+ 
                           "(starting from vertex 2)"); 
        boolean [] visited = new boolean[4];
        g.dfs(2);
        System.out.println();
        g.dfsUtil(2, visited);
	}

}
