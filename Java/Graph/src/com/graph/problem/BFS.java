package com.graph.problem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class BFS {
    int V;
    ArrayList<Integer>[] adj;
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public BFS(int v) {
    	this.V = v;
    	adj = new ArrayList[v];
    	for(int i=0;i<v;i++)
    		adj[i] = new ArrayList();
    }
    
    public void addEdge(int v, int w) {
    	adj[v].add(w);
    }
    
    public void bfs(int s) {
    	boolean [] visited = new boolean[V];
    	Arrays.fill(visited, false);
    	Queue<Integer> que = new LinkedList<>();
    	visited[s] = true;
    	que.add(s);
    	while(!que.isEmpty()) {
			int size = que.size();
    		s = que.poll();
    		System.out.println(s + " ");
			for(int i=0;i<size;i++){
				for (int element : adj[s]) {
					if (!visited[element]) {
						visited[element] = true;
						que.offer(element);
					}
				}
			}

    	}
    }
    
	public static void main(String[] args) {
		BFS g = new BFS(4);		  
        g.addEdge(0, 1); 
        g.addEdge(0, 2); 
        g.addEdge(1, 2); 
        g.addEdge(2, 0); 
        g.addEdge(2, 3); 
        g.addEdge(3, 3);   
        System.out.println("Following is Depth First Traversal "+ 
                           "(starting from vertex 2)"); 
  
        g.bfs(2); 

	}

}
