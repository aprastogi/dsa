package com.graph.problem;

import java.util.ArrayList;
import java.util.Stack;

class Topological {
    public boolean canFinish(int numCourses, int[][] prerequisites) {
        ArrayList<Integer> [] adj = new ArrayList[numCourses];
        for(int i=0;i<numCourses;i++)
            adj[i] = new ArrayList<>();
        for(int [] pre : prerequisites)
            adj[pre[1]].add(pre[0]);
        boolean [] visited = new boolean[numCourses];
        boolean [] dfsVisited = new boolean[numCourses];
        for(int v = 0; v< numCourses; v++){
            if(!visited[v]){
                if(isCycle(v, adj, visited, dfsVisited))
                    return false;
            }

        }
        return true;
    }

    private boolean isCycle(int s, ArrayList<Integer> [] adj, boolean [] visited, boolean [] dfsVisited){
        visited[s] = true;
        dfsVisited[s] = true;
        for(int element : adj[s]){
            if(!visited[s]){
                if(isCycle(element, adj, visited, dfsVisited))
                    return true;
            } else if(dfsVisited[s]) {
                return true;
            }
        }
        dfsVisited[s] = false;
        return false;
    }

    public static void main(String[] args) {
        Topological obj = new Topological();
        System.out.println(obj.canFinish(2, new int[][]{{0,1}}));
    }
}
