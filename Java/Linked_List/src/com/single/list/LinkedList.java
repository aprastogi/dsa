package com.single.list;

class Node{
	int data;
	Node next;
	
	public Node(int data) {
		this.data = data;
		next = null;
	}
}

public class LinkedList {
	Node head;	  
	
    public static void print(Node head) {
         while(head != null) {
        	 System.out.println(head.data);
        	 head = head.next;
         }
    }
    
    public static int length(Node head) {
    	int ctr = 0;
    	while(head != null) {
    		ctr++;
    		head = head.next;
    	}
    	return ctr;
    }
    
	public static void main(String[] args) {
		LinkedList list = new LinkedList();
		list.head = new Node(1);
		list.head.next = new Node(2);
		list.head.next.next = new Node(3);
		list.head.next.next.next = new Node (4);
		print(list.head);
		System.out.println("Length of the Linked list is : " + length(list.head));
	}
}
