package com.practice;

class parent {
	void show() {
		System.out.println("hello");
	}
}

public class AnnonymousInnerClass {

	public static void main(String[] args) {
		parent obj = new parent() {
			void show() {
				System.out.println("world");
			}
		};
		obj.show();

	}

}
