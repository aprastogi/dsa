package com.practice;

public class BracketMatch {
  
  static int bracketMatch(String text) {
      int openBracket = 0, closeBracket = 0;
      for(int i=0;i<text.length();i++){
          if(text.charAt(i) == '('){
              openBracket++;
          }
          else if(openBracket == 0)
               closeBracket++;
          else
               openBracket--;
      }
      return openBracket + closeBracket;
  }

  public static void main(String[] args) {
      String text = ")(()()";
      System.out.println(bracketMatch(text));
  }

}