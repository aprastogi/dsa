package com.practice;

public class AntiDiagonal {

	public static void main(String[] args) {
		int arr[][] = {{1,2,3},
				       {4,5,6},
				       {7,8,9}};
       for(int i=0;i<arr.length;i++) {
    	   for(int j=i,k =0;j>=0;j--,k++) {
    		   System.out.print(arr[k][j]);
    	   }
    	   System.out.println();
       }
       for(int i = 1 ; i < arr.length; i++) {
    	   for(int j= i,k= arr.length-1; j < arr.length;j++,k--) {
    		   System.out.print(arr[j][k]);
    	   }
    	   System.out.println();
       }
	}

}
