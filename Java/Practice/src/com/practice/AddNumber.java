package com.practice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class AddNumber {
    
	public static ArrayList<Integer> plusOne(ArrayList<Integer> A) {
        ArrayList<Integer> result = new ArrayList<Integer>();
        if(A.size() == 1)
            return A;
        result.add((A.get((A.size()-1)) + 1) % 10);
        int carry = (A.get((A.size()-1)) + 1) / 10;
        int i = A.size() -2;
        while(i>=0){
            result.add((A.get(i) + carry) % 10);
            carry = (A.get(i) + carry) / 10;
            i--;
        }
        if(carry > 0)
           result.add(carry);
        Collections.reverse(result);
        return result;
    }
	
	public static int[][] reconstructQueue(int[][] people) {
        List<int[]> list = new ArrayList<>();
        Arrays.sort(people, (a,b) -> {
            return (a[0] == b[0] ? a[1] - b[1] : b[0] - a[0]);
        });
        for(int [] x : people){
            list.add(x[1],x);
        }
        return list.toArray(new int[people.length][2]);
    }
	
	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<Integer>();
		ArrayList<Integer> result = new ArrayList<Integer>();
		list.add(9);
		list.add(9);
		list.add(9);
		result = plusOne(list);
		System.out.println(result);
		int arr [][] = {{7,0},{4,4},{7,1}, {5,0}, {6,1}, {5,2}};
		reconstructQueue(arr);
	}

}
