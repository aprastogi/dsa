package com.practice;

public class FirstMissingPositive2 {
   
	public static void swap(int nums[],int i, int j){
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
	}
	    
    public static int firstMissingPositive(int[] nums) {
        int i = 0;
        while(i < nums.length){
          if (nums[i] != i && nums[i] < nums.length && nums[i] != nums[nums[i]])
             swap(nums, i, nums[i]);
          else
             i++;
        }
        for (i = 0; i < nums.length; i++)
          if (nums[i] != i )
            return i ;
        return nums.length;
	}
    
	public static void main(String[] args) {
		int nums[] = {3,4,2,0,1};
		System.out.println(firstMissingPositive(nums));
	}
}
