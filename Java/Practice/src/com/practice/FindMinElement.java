package com.practice;

import java.util.ArrayList;

public class FindMinElement {

	public static int findMin(ArrayList<Integer> list, int low, int high) {	  
		  if(low >= high)
			  return list.get(0);	
		  int mid = (low + high)/2;
		  if(mid < high && list.get(mid) > list.get(mid+1))
			  return list.get(mid + 1);
		  if(mid > low && list.get(mid) < list.get(mid-1))
			  return list.get(mid);		  
		  if(list.get(low) <= list.get(mid))
			  return findMin(list, low , mid -1);
		  return findMin(list, mid + 1, high);
	}
	
	public static int distanceBetweenBusStops(int[] distance, int start, int destination) {
        int clockwise = 0;
        int total = 0;
        for(int i=0;i< distance.length;i++){
            if(start < destination && (i >= start && i < destination))
                clockwise += distance[i];
            if(start > destination && (i >= start || i < destination))
                clockwise += distance[i];
            total += distance[i];
        }
        return Math.min(clockwise, total - clockwise);
    }
	
	 
	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(3);
		list.add(4);
		list.add(5);
		list.add(6);
		list.add(7);
		list.add(8);
		System.out.println(findMin(list,0,list.size()-1));
		int [] distance = {1,2,3,4,5};
		System.out.println(distanceBetweenBusStops(distance,4,1));
	}

}
