package com.practice;

import java.util.Arrays;
import java.util.Comparator;

public class ComparatorEx {

	public static void main(String[] args) {
		int [][] arr = {{2,3},{5,6},{1,9},{4,13},{6,0}};
		Arrays.sort(arr, new Comparator<int []>() {
			@Override
			public int compare(int [] o1, int [] o2) {
				return o1[0] - o2[0];
			}			
		});
		for(int i=0;i<arr.length;i++) {
			for(int j=0;j<arr[0].length;j++) {
				System.out.print(arr[i][j]+ " ");
			}
			System.out.println();
		}
	}

}
