package com.practice;

class DemoParent{
	protected void show() {
		System.out.println("Parent");
	}
}

class DemoChild extends DemoParent{
	public void show() {
		super.show();
	}
}

public class ProtectedDemo {

	public static void main(String[] args) {
		DemoChild obj = new DemoChild();
		obj.show();
	}

}
