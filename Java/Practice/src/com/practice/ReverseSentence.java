package com.practice;

public class ReverseSentence {
    
	static void reverse(char arr[],int start,int end) {
		while(start < end) {
			char temp = arr[start];
			arr[start] = arr[end];
			arr[end] = temp;
			start++;
			end --;
		}
	}
	
	static void reverseUtil(char arr[]) {
		reverse(arr,0,arr.length -1);
	    int start = 0;
	    for(int i=0;i< arr.length;i++){
	         if(i == arr.length - 1)
	           reverse(arr,start, i);          
	         else if(arr[i] == ' ')
	         { 	           
	            reverse(arr,start,i-1);             
	            while(arr[i] == ' ')
	              i++;
	            start = i;    
	         }
	     }
    }
	
	public static void main(String[] args) {
		char arr[] = {'a','p','o','o','r','v',' ','r','a','s','t','o','g','i'};
		reverseUtil(arr);
		for(int i=0;i<arr.length;i++)
			System.out.print(arr[i] + " ");
	}
}
