package com.practice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Solution {
    private static int totalOptions = 0;


    private static void countWays(List<Integer> jeansPrices, List<Integer> shoesPrices, List<Integer> skirtPrices, List<Integer> topsPrices,
                           Integer budget, String productToPurchase, Integer totalAmount) {
        switch(productToPurchase) {
            case "jeans": {
                for(Integer price : jeansPrices) {
                    if((totalAmount + price) > budget) {
                        continue;
                    }

                    countWays(jeansPrices, shoesPrices, skirtPrices, topsPrices, budget, "shoes", totalAmount+price);
                }
                break;
            }
            case "shoes": {
                for(Integer price : shoesPrices) {
                    if(totalAmount + price > budget) {
                        continue;
                    }

                     countWays(jeansPrices, shoesPrices, skirtPrices, topsPrices, budget, "skirt", totalAmount+price);
                }
                break;
            }
            case "skirt": {
                for(Integer price : skirtPrices) {
                    if(totalAmount + price > budget) {
                        continue;
                    }
                     countWays(jeansPrices, shoesPrices, skirtPrices, topsPrices, budget, "tops", totalAmount+price);
                }
                break;
            }
            case "tops": {
                int currentNoOfWays = 0;
                for(int price : topsPrices) {
                    if(totalAmount + price > budget) {
                        continue;
                    } else {
                        currentNoOfWays++;
                    }
                }
                totalOptions += currentNoOfWays;
            }

        }
    }

    public static void main(String args[]) {
        List<Integer> jeansPrices = new ArrayList<Integer>(Arrays.asList(2,3));
        List<Integer> shoesPrices = new ArrayList<Integer>(Arrays.asList(4));
        List<Integer> skirtPrices = new ArrayList<Integer>(Arrays.asList(2,3));
        List<Integer> topsPrices = new ArrayList<Integer>(Arrays.asList(1,2));
        int budget = 10;
        countWays(jeansPrices, shoesPrices, skirtPrices, topsPrices, budget, "jeans", 0);
        System.out.println("total options are "+totalOptions);

    }
}
