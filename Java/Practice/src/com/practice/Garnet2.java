package com.practice;

import java.util.Arrays;

public class Garnet2 {
	
	public static int maxExperience(int[] dementer) {
		int max_experience_so_far = 0;
		int curr_experience = 0;
		//splitting the array into two from right
		for(int i=dementer.length-1 ;i>=0;i--) {
			int health = i + 1;
			//sum of the right elements
			curr_experience += dementer[i];
			// calculating max_experience_so_far
			max_experience_so_far = Math.max(max_experience_so_far, curr_experience * health);
		}
		return max_experience_so_far;		
	}
	
	public static void main(String[] args) {
		int dementer[] = {3,2,5};
		//Sort in ascending order
		Arrays.parallelSort(dementer);
		System.out.println(maxExperience(dementer)); 
	}
}
