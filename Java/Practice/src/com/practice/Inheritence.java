package com.practice;

class A{
	public void show() {
		System.out.println("Class A");
	}
	
	protected void show(int a) {
		System.out.println(a);
	}
}

class B extends A{
	public void show() {
		System.out.println("Class B");
	}
}

public class Inheritence {

	public static void main(String[] args) {
		A obj = new B();
		obj.show();
		A obj1 = new B();
		obj1.show(4);

	}

}
