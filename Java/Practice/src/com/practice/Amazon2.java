package com.practice;

import java.util.Arrays;

public class Amazon2 {
    
	public static int solveUtil(String s, int start, int end) {
		int firstPipe = 0, secondPipe = 0;
		int tmpStart = start;
		int ans = 0;
		while(start < end && s.charAt(start) != '|')
			start++;
		if(start == end)
			return 0;
		firstPipe = start;
		while(end > tmpStart && s.charAt(end) != '|')
			end--;
		if(end == tmpStart)
			return 0;
		secondPipe = end;
		for(int j = firstPipe ; j<= secondPipe ;j++) {
			if(s.charAt(j) == '*')
				ans++;
		}
		return ans;			
	}
	
	public static int [] solve(String s, int [] start, int [] end) {	
	     int [] result = new int[start.length];
	     int ndx = 0;
	     for(int i=0;i<start.length;i++)
	    	 result[ndx++] = solveUtil(s,start[i] -1 , end[i] -1);
	     return result;
	}
	
	public static void main(String[] args) {
		System.out.println(Arrays.toString(solve("|**|*|", new int[]{1,1,2,4,2}, new int[]{5,6,4,6,6})));
	}

}
