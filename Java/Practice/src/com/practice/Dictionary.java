package com.practice;

import java.util.ArrayList;
import java.util.HashSet;

public class Dictionary {
    
	public static int validWord(String word, String letters) {
		HashSet<Character> set = new HashSet<>();
		for(int i=0; i<letters.length();i++) {
			set.add(letters.charAt(i));
		}
		for(int i=0;i< word.length();i++) {
			if(!set.contains(word.charAt(i)))
				return 0;
		}
		return word.length();
	}
	
	public static String longestWord(ArrayList<String> list, String letters) {
		String result = "";
		for(int i = 0; i<list.size(); i++) {
			if(validWord(list.get(i), letters) != 0 && list.get(i).length() > result.length()) {
			     result = list.get(i);
			}
		}
		return result;
	}
	
	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<>();
		list.add("t");
		list.add("to");
		list.add("toe");
		list.add("toes");
		list.add("tttooooeeesss");
		list.add("book");
		String letters = "toes";
        System.out.println(longestWord(list,letters));
	}

}
