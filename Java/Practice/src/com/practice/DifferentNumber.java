package com.practice;

import java.util.ArrayList;
import java.util.Collections;

public class DifferentNumber {
    
	static int missingNumber(ArrayList<Integer> list) {
		int i= 0;
		while(i < list.size()) {
			if (list.get(i) > 0 && list.get(i) < list.size() && list.get(i) != list.get(list.get(i)))
				Collections.swap(list, i, list.get(i));
	        else
	             i++;
	    }
        for (i = 0; i < list.size(); i++)
          if (list.get(i) != i)
            return i;

	    return list.size();
	}
	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<Integer>();
		list.add(7);		
		list.add(5);
		list.add(4);
		list.add(0);
		list.add(3);
		list.add(6);
		list.add(2);
        System.out.println(missingNumber(list));
	}

}
