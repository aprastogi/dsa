package com.practice;

import java.util.Arrays;

public class Garnet {
   
	public static int maxExperience(int[] dementer) {
		int max_experience_so_far = 0;
		for(int i = 1 ;i< dementer.length;i++) {
		    int curr_experience = 0;
		    int health = i + 1;
		    // Calculate the experience after battle
		    for(int j = i;j< dementer.length;j++) {
		    	curr_experience += dementer[j] * health;
		    }
		    //Store the max experience
		    max_experience_so_far = Math.max(max_experience_so_far, curr_experience);
		}
		return max_experience_so_far;
	}
	
	public static void main(String[] args) {
		int dementer[] = {3,2,5};
		//Sort in ascending order
		Arrays.parallelSort(dementer);
		System.out.println(maxExperience(dementer));        
	}
}

//Time Complexity - O(N2) where N is the number of dementer
// Space Complexity - O(1)

