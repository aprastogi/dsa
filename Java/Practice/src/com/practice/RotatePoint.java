package com.practice;

public class RotatePoint {

	public static int findPivot(int [] arr,int low,int high) {
		if (high < low) 
            return -1; 
        if (high == low) 
            return low; 
        int mid = (low + high)/2;
		if(mid < high && arr[mid] > arr[mid+1])
			return mid;
		if(mid > low && arr[mid] < arr[mid-1])
			return mid-1;
		if(arr[mid] <= arr[low])
			return findPivot(arr, low, mid-1);
		return findPivot(arr,mid+1,high);	
	}
	
	public static void main(String[] args) {
		int [] nums = {4,0,0,1,2,3,4};
        System.out.println(findPivot(nums,0,nums.length));
	}

}
