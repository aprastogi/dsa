package com.flipkart.test;

import lombok.Data;

import java.sql.Timestamp;

@Data
public abstract class Post {
    private String content;
    private Type type;
    private Timestamp createdTime;
}
