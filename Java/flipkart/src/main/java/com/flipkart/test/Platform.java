package com.flipkart.test;

import lombok.Data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class Platform {

    Map<String, User> userList;
    List<Post> postList;
    User currentActiveUser;
    int advertisementThreshold = 3;

    public Platform(){
        userList = new HashMap<>();
    }

    public void signUP(UserType userType, String userId){
        User user = null;
        if(userType == UserType.USER){
            user = new Customer(userId);
        }
        else if(userType == UserType.ADMIN){
            user = new Admin(userId);
        }
        userList.put(userId, user);
        System.out.println("User is registered successfully");
    }

    public User login(String userId){
        if(!userList.containsKey(userId)){
            System.out.println("user doesn't exist");
        }
        currentActiveUser = userList.get(userId);
        System.out.println("user logged in");
        return currentActiveUser;
    }

    public void logout(String userId){
        if(!userList.containsKey(userId)){
            System.out.println("user doesn't exist");
        }
        if(currentActiveUser.equals(userList.get(userId))){
            currentActiveUser = null;
        }
        else{
            System.out.println("userId is not correct");
        }
    }

    public void addPost(Post post){
        postList.add(post);
    }

    public void deletePost(Post post){
        postList.remove(post);
    }

}
