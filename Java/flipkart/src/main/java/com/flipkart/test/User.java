package com.flipkart.test;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public abstract class User {
    private String userid;

    public User(String userid) {
        this.userid = userid;
    }

    public void upVote(News news){
        news.setUpVoteCount(news.getUpVoteCount() + 1);
    }

    public void downVote(News news){
        news.setDownVoteCount(news.getDownVoteCount() + 1);
    }

    public List<News> viewFeed(Mode mode){
        if(mode.equals(Mode.RECENT)){
            // sort news list based on timestamp
        }
        else if(mode.equals(Mode.TRENDING)){
            // sort based on upvote - downvote
        }
        return new ArrayList<>();
    }
}
