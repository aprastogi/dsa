package com.flipkart.test;

import lombok.Data;

@Data
public class News {

    public News(Category category, boolean isPromoted, int upVoteCount, int downVoteCount) {
        this.category = category;
        this.isPromoted = isPromoted;
        this.upVoteCount = upVoteCount;
        this.downVoteCount = downVoteCount;
    }

    private Category category;
    private boolean isPromoted;
    private int upVoteCount;
    private int downVoteCount;

}
