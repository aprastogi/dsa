package com.flipkart.test;

import lombok.experimental.SuperBuilder;

public class Admin extends User{

    public Admin(String userid) {
        super(userid);
    }

    public boolean uploadPost(Platform platform, Post post){
        platform.addPost(post);
        return true;
    }
}
