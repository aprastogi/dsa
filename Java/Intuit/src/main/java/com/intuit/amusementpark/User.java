package com.intuit.amusementpark;

import com.intuit.amusementpark.exception.AlreadyRidingException;
import com.intuit.amusementpark.exception.NotRidingException;
import lombok.Builder;
import lombok.Data;

import java.util.Map;
import java.util.Objects;

@Data
@Builder
public class User {
    private String id;
    private String name;
    private boolean isRiding;
    private Map<User, Ride> userRideMap;

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", isRiding=" + isRiding +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return isRiding == user.isRiding && Objects.equals(id, user.id) && Objects.equals(name, user.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, isRiding);
    }

    public void chooseRide(Ride ride, Ticket ticket) throws AlreadyRidingException {
        if(isRiding) {
            throw new AlreadyRidingException(this.name + " is already riding");
        }
        userRideMap.put(this, ride);
        ride.addUserToQueue(this, ticket);
    }

    public void completeRide() throws NotRidingException {
        if(!isRiding)
            throw new NotRidingException(this.name + "");
        this.isRiding = false;
        userRideMap.remove(this);
    }
}
