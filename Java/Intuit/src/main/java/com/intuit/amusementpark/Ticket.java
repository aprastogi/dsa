package com.intuit.amusementpark;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Ticket {
    private String ticketId;
    private TicketType ticketType;
}
