package com.intuit.amusementpark;

import com.intuit.amusementpark.exception.AlreadyRidingException;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        AmusementPark amusementPark = new AmusementPark();
        amusementPark.addUser(User.builder().id("1").name("user1").userRideMap(new HashMap<>()).build());
        amusementPark.addUser(User.builder().id("2").name("user2").userRideMap(new HashMap<>()).build());
        amusementPark.addUser(User.builder().id("3").name("user3").userRideMap(new HashMap<>()).build());
        amusementPark.addUser(User.builder().id("4").name("user4").userRideMap(new HashMap<>()).build());
        amusementPark.addUser(User.builder().id("5").name("user5").userRideMap(new HashMap<>()).build());
        amusementPark.addUser(User.builder().id("6").name("user6").userRideMap(new HashMap<>()).build());
        amusementPark.addUser(User.builder().id("7").name("user7").userRideMap(new HashMap<>()).build());
        amusementPark.addUser(User.builder().id("8").name("user8").userRideMap(new HashMap<>()).build());
        amusementPark.addUser(User.builder().id("9").name("user9").userRideMap(new HashMap<>()).build());
        amusementPark.addUser(User.builder().id("10").name("user10").userRideMap(new HashMap<>()).build());

        amusementPark.buyTicket(amusementPark.getUserList().get(0), Ticket.builder().ticketId("t1").ticketType(TicketType.VIP).build());

        Map<User,Ticket> userTicketMap = amusementPark.getUserTicketMap();

        for(Map.Entry<User,Ticket> entry : userTicketMap.entrySet()){
            User user = entry.getKey();
            try {
                user.chooseRide(amusementPark.getRideList().get(0), amusementPark.getUserTicketMap().get(user));
            } catch (AlreadyRidingException e) {
                e.printStackTrace();
            }
        }
        amusementPark.getRideList().get(0).startRide();
    }
}
