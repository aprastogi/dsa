package com.intuit.amusementpark.exception;

public class NotRidingException extends Exception {
    public NotRidingException(String s) {
        super(s);
    }
}
