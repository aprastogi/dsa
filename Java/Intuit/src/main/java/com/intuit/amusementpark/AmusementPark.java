package com.intuit.amusementpark;

import lombok.Getter;

import java.util.*;

@Getter
public class AmusementPark {
    List<Ride> rideList;
    List<User> userList;
    Map<User, Ticket> userTicketMap;

    public AmusementPark(){
        this.rideList = new ArrayList<>(Arrays.asList(Ride.builder().name("Ride1").capacity(10).ticketTypeQueueMap(new HashMap<>()).build()
                , Ride.builder().name("Ride2").capacity(20).ticketTypeQueueMap(new HashMap<>()).build()
                ,Ride.builder().name("Ride3").capacity(16).ticketTypeQueueMap(new HashMap<>()).build()
                ,Ride.builder().name("Ride4").capacity(30).ticketTypeQueueMap(new HashMap<>()).build()
                ,Ride.builder().name("Ride5").capacity(8).ticketTypeQueueMap(new HashMap<>()).build() ));
        this.userList = new ArrayList<>();
        this.userTicketMap = new HashMap<>();
    }

    void addUser(User user){
        userList.add(user);Hi
    }

    void addRide(Ride ride){
        rideList.add(ride);
    }

    void buyTicket(User user, Ticket ticket){
        userTicketMap.put(user, ticket);
    }
}
