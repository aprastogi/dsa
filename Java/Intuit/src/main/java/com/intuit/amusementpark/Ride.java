package com.intuit.amusementpark;

import com.intuit.amusementpark.exception.NotRidingException;
import lombok.Builder;
import lombok.Data;

import java.util.*;

@Builder
@Data
public class Ride {
    private String name;
    private int capacity;
    private Map<TicketType, Queue<User>> ticketTypeQueueMap;

    public void addUserToQueue(User user, Ticket ticket) {
        TicketType userTicketType = ticket.getTicketType();
        Queue<User> currentQueue = ticketTypeQueueMap.get(userTicketType);
        currentQueue.add(user);
        ticketTypeQueueMap.put(userTicketType, currentQueue);
    }

    public void exitRide(List<User> userList) {
        for (User user : userList) {
            try {
                user.completeRide();
            } catch (NotRidingException e) {
                e.printStackTrace();
            }
        }
    }

    public void startRide() {
        int count = 0;
        int emptyQueueCounter = 0;
        while(count < this.capacity && emptyQueueCounter < ticketTypeQueueMap.size()) {
            List<User> currentUsersInRide = new ArrayList<>();
            for (Map.Entry<TicketType, Queue<User>> entry : ticketTypeQueueMap.entrySet()) {
                TicketType ticketType = entry.getKey();
                Queue<User> queue = entry.getValue();
                switch (ticketType) {
                    case VIP:
                        int vipCount = 0;
                        while (!queue.isEmpty() && vipCount != capacity / 2) {
                            User user = queue.poll();
                            currentUsersInRide.add(user);
                            System.out.println(user.getName() + "is in ride with " + ticketType + "Ticket");
                            user.setRiding(true);
                            count++;
                            vipCount++;
                        }
                        break;
                    case PREMIUM:
                        int pCount = 0;
                        while (!queue.isEmpty() && pCount != capacity / 3) {
                            User user = queue.poll();
                            currentUsersInRide.add(user);
                            System.out.println(user.getName() + "is in ride with " + ticketType + "Ticket");
                            user.setRiding(true);
                            count++;
                            pCount++;
                        }
                        break;
                    case BASIC:
                        while (!queue.isEmpty() && capacity - count != 0) {
                            User user = queue.poll();
                            currentUsersInRide.add(user);
                            System.out.println(user.getName() + "is in ride with " + ticketType + "Ticket");
                            user.setRiding(true);
                            count++;
                        }
                        break;
                }
                if(queue.isEmpty())
                    emptyQueueCounter++;
            }
            exitRide(currentUsersInRide);
        }
    }
}
