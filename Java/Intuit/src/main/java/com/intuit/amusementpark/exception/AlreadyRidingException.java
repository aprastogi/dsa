package com.intuit.amusementpark.exception;

public class AlreadyRidingException extends Exception {
    public AlreadyRidingException(String s) {
        super(s);
    }
}
