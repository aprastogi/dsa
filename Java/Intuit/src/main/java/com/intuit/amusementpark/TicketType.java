package com.intuit.amusementpark;

public enum TicketType {
    BASIC(100),PREMIUM(500),VIP(700);

    int cost;

    TicketType(int cost) {
        this.cost = cost;
    }

    public int getCost(){
        return cost;
    }
}
