package com.binary.tree;

public class BinarySearchTree {
    
	Node root;
	
	public Node insertNode(Node current, int val) {		
		if(current == null)
			return new Node(val);
		if(current.data < val)
			current.right = insertNode(current.right, val);
		else if(current.data > val)
			current.left = insertNode(current.left, val);
		return current;
	}	
	
	public void print(Node root) {
		if(root == null)
			return;
		print(root.left);
		System.out.print(root.data + " ");
		print(root.right);
	}
	
	public Node delete(Node current, int val) {
		if(current == null)
			return null;
		if(current.data < val)
			current.right = delete(current.right, val);
		else if(current.data > val)
			current.left = delete(current.left, val);
		else {
			if(current.left == null && current.right == null)
				return null;
			return current.left != null ? current.left : current.right;
		}
		return current;
			
	}
	
	public boolean isBSTUtil(Node root, Integer min, Integer max) {
		if(root == null)
			return true;
		if(min != null && root.data <= min)
			return false;
		if(max != null && root.data >= max)
			return false;
		return  isBSTUtil(root.left , min, root.data) && isBSTUtil(root.right, root.data, max);
	}
	
	public boolean isBST(Node root) {
		return isBSTUtil(root, null, null);
	}
	
	public static void main(String[] args) {
		BinarySearchTree bst = new BinarySearchTree();
		bst.root = new Node(8);
		bst.root = bst.insertNode(bst.root, 10);
		bst.root = bst.insertNode(bst.root , 4);
		bst.root= bst.insertNode(bst.root , 7);
		bst.root = bst.insertNode(bst.root , 2);
		bst.root = bst.insertNode(bst.root , 12);
		bst.root = bst.insertNode(bst.root , 11);
		bst.root = bst.insertNode(bst.root , 14);
        bst.print(bst.root);
        System.out.println();
        System.out.println(bst.isBST(bst.root));
	}

}
