package com.binary.tree;

public class Queue {
   
	private int [] arr;
	private int front;
	private int rear;
	private int capacity;
	private int size;
	
	public Queue(int capacity){
		this.arr = new int[capacity];
		this.front = 0;
		this.rear = 0;
		this.size = 0;
		this.capacity = capacity;
	}
	
	public void push(int element) {
		try {
			if(this.rear == this.capacity)
				throw new Exception();
		}
		catch(Exception e) {
			System.out.println("Overflow");
			return;
		}
		arr[rear++] = element;
		size++;
		System.out.println("Element pushed to the queue");
	}
	
	public int pop() {
		try {
			if(this.size == 0)
				throw new Exception();
		}
		catch(Exception e) {
			return -1;
		}
		int ele = arr[front++];
		size--;
		return ele;
	}
	
	public int size(int [] arr) {
		return size;
	}
	
	public boolean isEmpty() {
		return this.size == 0;
	}
	
	public static void main(String[] args) {
		Queue que = new Queue(4);
		que.push(10);
		que.push(12);
		que.push(13);
		que.push(13);
		System.out.println(que.pop());
		System.out.println(que.pop());
		que.push(34);
	}

}
                   
                 


