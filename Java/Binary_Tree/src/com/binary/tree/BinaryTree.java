package com.binary.tree;

class Node{
	int data;
	Node left, right;
	
	public Node(int data) {
		this.data = data;
		left = right = null;
	}
}

public class BinaryTree {
    Node root;    
    public static void print(Node root) {
        if(root == null)
        	return;
        System.out.print(root.data +  " ");
        print(root.left);
        print(root.right);
    }
    
    public static int height(Node root) {
    	if(root == null)
    		return 0;
    	return Math.max(height(root.left) ,height(root.right)) + 1;
    }
    
    
    public static void levelOrderTraverse(Node root,int level) {
    	if(root == null)
    		return;
    	if(level == 1)
    		System.out.print(root.data + " ");
    	levelOrderTraverse(root.left, level-1);
    	levelOrderTraverse(root.right, level-1);
    }
    
    public static void levelOrderTraversal(Node root) {
    	int height = height(root);
    	for(int i= 1;i<=height;i++)
    		levelOrderTraverse(root,i);
    }
    
    static Node newRoot = null;
    static Node answer = null;
    public static void traverse(Node root){
        if(root == null)
            return;
        traverse(root.left);
        if(newRoot == null) {
           newRoot = root;
           answer = newRoot;
        }
        else{
           newRoot.left = null;
           newRoot.right = root;
           newRoot = newRoot.right;
           newRoot.right = null;
           newRoot.left = null;
        }        
        traverse(root.right);
    }
    
    public static Node increasingBST(Node root) {
        traverse(root);
        return answer;
    }
  
   
    
	public static void main(String[] args) {
		BinaryTree tree = new BinaryTree();
		BinarySearchTree bst = new BinarySearchTree();
		tree.root = new Node(5);
		tree.root.left = new Node(3);
		tree.root.right = new Node(6);
		tree.root.left.left = new Node(2);
		tree.root.left.right = new Node(4);
		tree.root.left.left.left = new Node(1);
		tree.root.right.right = new Node (8);
		tree.root.right.right.left = new Node(7);
		tree.root.right.right.right = new Node(2);
		/*System.out.print("Binary Tree is : ");
		print(tree.root);
		System.out.println("\nHeight of the binary tree is : " + height(tree.root));
		System.out.print("Level Order Traversal of Binary Tree is : ");
		levelOrderTraversal(tree.root);
		tree.root = increasingBST(tree.root);*/
		print(tree.root);
		System.out.println();
		System.out.println(bst.isBST(tree.root));
	}
}
