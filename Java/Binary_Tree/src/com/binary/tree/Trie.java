
package com.binary.tree;

import java.util.HashMap;

class TrieNode{
    HashMap<Character,TrieNode> children;
    boolean endOfWord;
   
    public TrieNode() {
        children = new HashMap<>();
        endOfWord = false;
    }
}

public class Trie {
    TrieNode root;   
    
    public Trie() {
       root = new TrieNode();
    }
   
    public void insert(String word) {
        TrieNode current = root;
        for(int i=0;i<word.length();i++) {
            TrieNode node = current.children.get(word.charAt(i));
            if(node == null) {
                node = new TrieNode();
                current.children.put(word.charAt(i), node);
            }
            current = node;
        }
        current.endOfWord = true;
    }
   
    public boolean search(String word) {
        TrieNode current = root;
        for(int i=0;i<word.length();i++) {
            TrieNode node = current.children.get(word.charAt(i));
            if(node == null)
                return false;
            current = node;
        }
        return current.endOfWord;
    }
   
    public void insert(TrieNode current,String word,int index) {
        if(index == word.length()) {
            current.endOfWord = true;
            return;
        }
        TrieNode node = current.children.get(word.charAt(index));
        if(node == null) {
            node = new TrieNode();
            current.children.put(word.charAt(index), node);
        }
        insert(node,word,index+1);
    }
   
    public boolean search(TrieNode current,String word,int index) {
        if(index == word.length()) {
            return current.endOfWord;
        }
        TrieNode node = current.children.get(word.charAt(index));
        if(node == null) {
            return false;
        }
        return search(node,word,index+1);
    }
   
    public boolean delete(TrieNode current,String word,int index) {
        if(index == word.length()) {
            if(!current.endOfWord)
                return false;
            current.endOfWord = false;
            return current.children.size() == 0;
        }
        TrieNode node = current.children.get(word.charAt(index));
        if(node == null)
            return false;
        boolean shouldDeleteCurrecntNode = delete(node, word, index +1);
        if(shouldDeleteCurrecntNode) {
            current.children.remove(word.charAt(index));
            return current.children.size() == 0;
        }
        return false;
    }
   
    public static void main(String[] args) {
       Trie trie = new Trie();
       trie.insert("apoorv");
       trie.insert(trie.root,"rastogi",0);
       trie.delete(trie.root,"rastogi",0);
       System.out.println(trie.search("apoorv"));
       System.out.println(trie.search(trie.root,"rastogi",0));
    }

}