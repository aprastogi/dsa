package com.binary.tree;

import java.util.ArrayList;
import java.util.List;

public class CircularQueueUsingGenerics<T>{
  
	private List<T> arr;
	private int front;
	private int rear;
	private int capacity;
	private int size;
	
	public CircularQueueUsingGenerics(int capacity, T initial){
		this.arr = new ArrayList<T>(capacity);
		for (int i = 0; i < capacity; i++) 
			arr.add(initial);
		this.front = 0;
		this.rear = capacity -1;
		this.size = 0;
		this.capacity = capacity;
	}
	
	public void push(T element) {
		try {
			if(size == capacity)
				throw new Exception();
		}
		catch(Exception e) {
			System.out.println("Overflow");
			return;
		}
		rear = (rear + 1) % capacity;
		arr.set(rear, element);
		size++;
		System.out.println("Element " + element + " pushed to the queue");
	}
	
	public void pop() {
		try {
			if(this.size == 0)
				throw new Exception();
		}
		catch(Exception e) {
			System.out.println("Underflow");
		}
		T ele = arr.get(front);
		front = (front + 1) % capacity;
		size--;
		System.out.println("Element " + ele + " popped from the queue");
	}
	
	public int size(int [] arr) {
		return size;
	}
	
	public boolean isEmpty() {
		return this.size == 0;
	}
	
	public static void main(String[] args) {
		CircularQueueUsingGenerics<Integer> que = new CircularQueueUsingGenerics<Integer>(4,0);
		que.push(10);
		que.push(12);
		que.push(13);
		que.push(14);
		que.pop();
		que.pop();
		que.push(4);
		que.push(13);
		que.push(23);
		que.push(67);
	}
}
