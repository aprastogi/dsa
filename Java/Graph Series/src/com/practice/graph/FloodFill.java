package com.practice.graph;

import java.util.Arrays;

//https://practice.geeksforgeeks.org/problems/flood-fill-algorithm1856/1
public class FloodFill {

    static int [][] direction = {{0,1},{1,0},{-1,0},{0,-1}};
    public static int[][] floodFill(int[][] image, int sr, int sc, int newColor)
    {
        int iniColor = image[sr][sc];
        boolean [][] visited = new boolean[image.length][image[0].length];
        dfs(image, image, visited, sr, sc, newColor, iniColor);
        return image;
    }

    public static void dfs(int[][] image, int[][] result, boolean[][] visited, int row, int col, int newColor, int iniColor){
        visited[row][col] = true;
        result[row][col] = newColor;
        for(int [] dir : direction){
            int nextR = dir[0] + row;
            int nextC = dir[1] + col;
            if(nextR >=0 && nextR < image.length && nextC >=0 && nextC < image[0].length && image[nextR][nextC] == iniColor && !visited[nextR][nextC]){
                dfs(image, result, visited, nextR, nextC, newColor, iniColor);
            }
        }
    }

    public static void main(String[] args) {
        System.out.println(Arrays.deepToString(floodFill(new int[][]{}, 0, 0, 2)));
    }
}
