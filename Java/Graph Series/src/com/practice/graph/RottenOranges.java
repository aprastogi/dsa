package com.practice.graph;

import java.util.LinkedList;
import java.util.Queue;

public class RottenOranges {

    static int [][] direction = {{0,1},{1,0},{0,-1},{-1,0}};

    //Function to find minimum time required to rot all oranges.
    public static int orangesRotting(int[][] grid){
        Queue<Pair> que = new LinkedList<>();
        boolean [][] visited = new boolean[grid.length][grid[0].length];
        int minTime = -1;
        for(int i=0;i<grid.length;i++){
            for(int j=0;j<grid[0].length;j++){
                if(grid[i][j] == 2){
                    visited[i][j] = true;
                    que.offer(new Pair(new int[]{i,j},0));
                }
            }
        }
        while(!que.isEmpty()){
            Pair pair = que.poll();
            int row = pair.first[0];
            int col = pair.first[1];
            int time = pair.second;
            minTime = Math.max(minTime, time);
            for(int [] dir : direction){
                int nextR = dir[0] + row;
                int nextC = dir[1] + col;
                if(nextR >=0 && nextC >=0 && nextR < grid.length && nextC < grid[0].length && grid[nextR][nextC] == 1 && !visited[nextR][nextC]){
                    visited[nextR][nextC] = true;
                    que.offer(new Pair(new int[]{nextR, nextC}, time + 1));
                }
            }
        }
        for(int i=0;i<grid.length;i++){
            for(int j=0;j<grid[0].length;j++){
                if(grid[i][j] == 1 && !visited[i][j])
                    return -1;
            }
        }
        return minTime;
    }

    public static void main(String[] args) {
        System.out.println(orangesRotting(new int[][]{}));
    }
}
