package com.practice.graph;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

//https://practice.geeksforgeeks.org/problems/number-of-distinct-islands/1
public class NumberOfDistinctIslands {
    static int [][] direction = {{0,1},{1,0},{-1,0},{0,-1}};

    static int countDistinctIslands(int[][] grid) {
        int m = grid.length;
        int n = grid[0].length;
        Set<List<String>> set = new HashSet<>();
        boolean [][] visited = new boolean[m][n];
        for(int i=0;i<m;i++){
            for(int j=0;j<n;j++){
                if(!visited[i][j] && grid[i][j] == 1){
                    List<String> list = new ArrayList<>();
                    dfs(grid, visited, i, j, i, j, list);
                    set.add(list);
                }
            }
        }
        return set.size();
    }

    static void dfs(int [][] grid, boolean [][] visited , int row, int col , int row0, int col0, List<String> list){
        int m = grid.length;
        int n = grid[0].length;
        visited[row][col] = true;
        list.add((row - row0) +""+ (col - col0));
        for(int [] dir : direction){
            int nextR = row + dir[0];
            int nextC = col + dir[1];
            if(nextR >=0 && nextC >=0 && nextR < m && nextC < n && !visited[nextR][nextC] && grid[nextR][nextC] == 1){
                dfs(grid, visited, nextR, nextC, row0, col0, list);
            }
        }
    }

    public static void main(String[] args) {
        System.out.println(countDistinctIslands(new int[][]{}));
    }
}
