package com.practice.graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;


//https://practice.geeksforgeeks.org/problems/shortest-path-in-undirected-graph/1
public class ShortestPathInDAG {

    public static void dfs(int node, ArrayList<ArrayList<Pair1>> adj, boolean [] visited, Stack<Integer> st){
        visited[node] = true;
        for(Pair1 it : adj.get(node)){
            if(!visited[it.first]){
                dfs(it.first, adj, visited, st);
            }
        }
        st.push(node);
    }

    public static int[] shortestPath(int N,int M, int[][] edges) {
        ArrayList<ArrayList<Pair1>> adj = new ArrayList<>();
        for(int i=0;i<N;i++){
            adj.add(new ArrayList<>());
        }
        // Create a graph
        for(int i=0;i<M;i++){
            int u = edges[i][0];
            int v = edges[i][1];
            int w = edges[i][2];
            adj.get(u).add(new Pair1(v, w));
        }

        // topological sort
        Stack<Integer> st = new Stack<>();
        boolean [] visited = new boolean[N];
        for(int i=0;i<N;i++){
            if(!visited[i]){
                dfs(i, adj, visited, st);
            }
        }

        int [] dist = new int[N];
        Arrays.fill(dist, (int)1e9);
        dist[0] = 0;
        while(!st.isEmpty()){
            int s = st.pop();
            for(Pair1 p : adj.get(s)){
                int v = p.first;
                int w = p.second;
                dist[v] = Math.min(dist[v], dist[s] + w);
            }
        }
        for(int i=0;i<dist.length;i++){
            if(dist[i] == (int)1e9){
                dist[i] = -1;
            }
        }
        return dist;
    }

    public static void main(String[] args) {
        int n = 4, m = 4;
        System.out.println(Arrays.toString(shortestPath(n, m , new int[][]{})));
    }
}
