package com.practice.graph;

import java.util.ArrayList;
import java.util.List;

//https://practice.geeksforgeeks.org/problems/eventual-safe-states/1
public class EventualSafeStates {

    public static List<Integer> eventualSafeNodes(int V, List<List<Integer>> adj) {
        List<Integer> answer = new ArrayList<>();
        boolean [] visited = new boolean[V];
        boolean [] dfsVisited = new boolean[V];
        int [] check = new int[V];
        for(int i=0;i<V;i++){
            if(!visited[i]){
                dfs(adj, visited, dfsVisited, i, check);
            }
        }
        for(int i=0;i<V;i++){
            if(check[i] == 1)
                answer.add(i);
        }
        return answer;
    }

    public static boolean dfs(List<List<Integer>> adj, boolean [] visited, boolean [] dfsVisited, int node, int [] check){
        visited[node] = true;
        dfsVisited[node] = true;
        for(Integer it : adj.get(node)){
            if(!visited[it]){
                if(dfs(adj, visited, dfsVisited, it, check)){
                    return true;
                }
            }
            else if(dfsVisited[it]){
                return true;
            }
        }
        dfsVisited[node] = false;
        check[node] = 1;
        return false;
    }

    public static void main(String[] args) {
        int V = 8;
        System.out.println(eventualSafeNodes(V, new ArrayList<>()));
    }
}
