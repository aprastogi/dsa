package com.practice.graph;

import java.util.ArrayList;
import java.util.Arrays;

//https://practice.geeksforgeeks.org/problems/distance-from-the-source-bellman-ford-algorithm/1
public class BellmanFord {

    static int[] bellman_ford(int V, ArrayList<ArrayList<Integer>> edges, int S) {
        int [] dist = new int[V];
        Arrays.fill(dist, (int)1e8);
        dist[S] = 0;
        // Need to do the N - 1 relaxation to get the answer
        for(int i=0;i<V;i++){
            for(ArrayList<Integer> it : edges){
                int u = it.get(0);
                int v = it.get(1);
                int w = it.get(2);
                // relaxation of the edge
                if(dist[u] != (int)1e8 && dist[u] + w < dist[v]){
                    dist[v] = dist[u] + w;
                }
            }
        }

        // Nth relaxation to check negative cycle
        for(ArrayList<Integer> it : edges){
            int u = it.get(0);
            int v = it.get(1);
            int w = it.get(2);
            if(dist[u] != Integer.MAX_VALUE && dist[u] + w < dist[v]){
                int [] temp = new int[1];
                temp[0] = -1;
                return temp;
            }
        }
        return dist;
    }

    public static void main(String[] args) {
        int V = 8;
        int S = 0;
        System.out.println(Arrays.toString(bellman_ford(V, new ArrayList<>(), S)));
    }
}
