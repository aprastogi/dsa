package com.practice.graph;

import java.util.ArrayList;
import java.util.Arrays;

//https://practice.geeksforgeeks.org/problems/bipartite-graph/1
public class isBipartite {

    public static boolean isBipartiteGraph(int V, ArrayList<ArrayList<Integer>>adj){
        int [] color = new int[V];
        Arrays.fill(color, -1);
        for(int i=0;i<V;i++){
            if(color[i] == -1){
                if(dfs(adj, 0, i, color))
                    return false;
            }
        }
        return true;
    }

    public static boolean dfs(ArrayList<ArrayList<Integer>> adj, int col, int node, int [] color){
        color[node] = col;
        for(Integer it : adj.get(node)){
            if(color[it] == -1){
                if(dfs(adj, 1 - col, it, color)){
                    return true;
                }
            }
            // If color of neighbour node is same as the current node then it is not a bipartite graph
            else if(color[it] == color[node]){
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        int V = 8;
        System.out.println(isBipartiteGraph(V, new ArrayList<>()));
    }
}
