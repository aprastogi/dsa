package com.practice.graph;

import java.util.Arrays;

//https://practice.geeksforgeeks.org/problems/replace-os-with-xs0052/1
public class SurroundedRegions {
    static int [][] direction = {{0,1},{1,0},{0,-1},{-1,0}};

    static char[][] fill(int n, int m, char[][] a){
        boolean [][] visited = new boolean[n][m];
        // dfs for first row
        for(int i=0;i<m;i++){
            if(!visited[0][i] && a[0][i] == 'O'){
                dfs(a, visited, 0, i);
            }
        }
        // dfs for first col
        for(int i=0;i<n;i++){
            if(!visited[i][0] && a[i][0] == 'O'){
                dfs(a, visited, i, 0);
            }
        }
        // dfs for last row
        for(int i=0;i<m;i++){
            if(!visited[n-1][i] && a[n-1][i] == 'O'){
                dfs(a, visited, n-1, i);
            }
        }
        // dfs for last col
        for(int i=0;i<n;i++){
            if(!visited[i][m-1] && a[i][m-1] == 'O'){
                dfs(a, visited, i, m-1);
            }
        }

        for(int i=0;i<n;i++){
            for(int j=0;j<m;j++){
                if(a[i][j] == 'O' && !visited[i][j]){
                    a[i][j] = 'X';
                }
            }
        }
        return a;
    }

    static void dfs(char [][] a , boolean [][] visited, int row, int col){
        visited[row][col] = true;
        for(int [] dir : direction){
            int nextR = dir[0] + row;
            int nextC = dir[1] + col;
            if(nextR >=0 && nextC >=0 && nextR < a.length && nextC < a[0].length && !visited[nextR][nextC] && a[nextR][nextC] == 'O'){
                visited[nextR][nextC] = true;
                dfs(a, visited, nextR, nextC);
            }
        }
    }

    public static void main(String[] args) {
        int m = 5, n = 4;
        System.out.println(Arrays.deepToString(fill(m, n, new char[][]{})));
    }
}
