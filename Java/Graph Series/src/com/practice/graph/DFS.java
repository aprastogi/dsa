package com.practice.graph;

import java.util.ArrayList;

//https://practice.geeksforgeeks.org/problems/depth-first-traversal-for-a-graph/1
public class DFS {

    // Function to return a list containing the DFS traversal of the graph.
    public static ArrayList<Integer> dfsOfGraph(int V, ArrayList<ArrayList<Integer>> adj) {
        ArrayList<Integer> list = new ArrayList<>();
        boolean [] visited = new boolean[V];
        dfs(adj, 0, visited, list);
        return list;
    }

    // DFS traversal of the graph
    public static void dfs(ArrayList<ArrayList<Integer>> adj, int node, boolean [] visited, ArrayList<Integer> list){
        visited[node] = true;
        list.add(node);
        for(Integer it : adj.get(node)){
            if(!visited[it]){
                dfs(adj, it, visited, list);
            }
        }
    }

    public static void main(String[] args) {
        System.out.println(dfsOfGraph(8, new ArrayList<>()));
    }
}
