package com.practice.graph;

//https://practice.geeksforgeeks.org/problems/number-of-enclaves/1
public class NumberOfEnclaves {
    static int [][] direction = {{0,1},{1,0},{0,-1},{-1,0}};
    static int numberOfEnclaves(int[][] a) {
        int n = a.length;
        int m = a[0].length;
        int count = 0;
        boolean [][] visited = new boolean[n][m];
        // dfs for first row
        for(int i=0;i<m;i++){
            if(!visited[0][i] && a[0][i] == 1){
                dfs(a, visited, 0, i);
            }
        }
        // dfs for first col
        for(int i=0;i<n;i++){
            if(!visited[i][0] && a[i][0] == 1){
                dfs(a, visited, i, 0);
            }
        }
        // dfs for last row
        for(int i=0;i<m;i++){
            if(!visited[n-1][i] && a[n-1][i] == 1){
                dfs(a, visited, n-1, i);
            }
        }
        // dfs for last col
        for(int i=0;i<n;i++){
            if(!visited[i][m-1] && a[i][m-1] == 1){
                dfs(a, visited, i, m-1);
            }
        }

        for(int i=0;i<n;i++){
            for(int j=0;j<m;j++){
                if(a[i][j] == 1 && !visited[i][j]){
                    count++;
                }
            }
        }
        return count;
    }

    static void dfs(int [][] a , boolean [][] visited, int row, int col){
        visited[row][col] = true;
        for(int [] dir : direction){
            int nextR = dir[0] + row;
            int nextC = dir[1] + col;
            if(nextR >=0 && nextC >=0 && nextR < a.length && nextC < a[0].length && !visited[nextR][nextC] && a[nextR][nextC] == 1){
                visited[nextR][nextC] = true;
                dfs(a, visited, nextR, nextC);
            }
        }
    }

    public static void main(String[] args) {
        System.out.println(numberOfEnclaves(new int[][]{}));
    }
}
