package com.practice.graph;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

//https://practice.geeksforgeeks.org/problems/distance-of-nearest-cell-having-1-1587115620/1
public class DistanceToNearest1 {
    static int [][] direction = {{0,1},{1,0},{0,-1},{-1,0}};
    //Function to find distance of nearest 1 in the grid for each cell.
    public static int[][] nearest(int[][] grid){
        int m = grid.length;
        int n = grid[0].length;
        boolean [][] visited = new boolean[m][n];
        Queue<Pair> que = new LinkedList<>();
        int [][] result = new int[m][n];
        for(int i=0;i<m;i++){
            for(int j=0;j<n;j++){
                if(grid[i][j] == 1){
                    visited[i][j] = true;
                    que.offer(new Pair(new int[]{i,j}, 0));
                }
            }
        }
        while(!que.isEmpty()){
            Pair pair = que.poll();
            int row = pair.first[0];
            int col = pair.first[1];
            int dist = pair.second;
            result[row][col] = dist;
            for(int [] dir : direction){
                int nextR = dir[0] + row;
                int nextC = dir[1] + col;
                if(nextR >=0 && nextC >=0 && nextR < m && nextC < n && !visited[nextR][nextC]){
                    visited[nextR][nextC] = true;
                    que.offer(new Pair(new int[]{nextR, nextC}, dist + 1));
                }
            }
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(Arrays.deepToString(nearest(new int[][]{})));
    }
}
