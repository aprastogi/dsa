package com.practice.graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;

//https://practice.geeksforgeeks.org/problems/topological-sort/1
public class TopologicalSort {

    static void dfs(int node, ArrayList<ArrayList<Integer>> adj, boolean [] visited, Stack<Integer> st){
        visited[node] = true;
        for(Integer it : adj.get(node)){
            if(!visited[it]){
                dfs(it, adj, visited, st);
            }
        }
        st.push(node);
    }

    //Function to return list containing vertices in Topological order.
    static int[] topoSort(int V, ArrayList<ArrayList<Integer>> adj)
    {
        Stack<Integer> st = new Stack<>();
        boolean [] visited = new boolean[V];
        for(int i=0;i<V;i++){
            if(!visited[i]){
                dfs(i, adj, visited, st);
            }
        }
        int [] result = new int[st.size()];
        int ndx = 0;
        while(!st.isEmpty()){
            result[ndx++] = st.pop();
        }
        return result;
    }

    public static void main(String[] args) {
        int V = 4;
        System.out.println(Arrays.toString(topoSort(V, new ArrayList<>())));
    }
}
