package com.practice.graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;

//https://practice.geeksforgeeks.org/problems/distance-from-the-source-bellman-ford-algorithm/1
// Dijkstra will fail in case there is negative edge in the graph
public class Dijkstra {

    //Function to find the shortest distance of all the vertices
    //from the source vertex S.
    static int[] dijkstra(int V, ArrayList<ArrayList<ArrayList<Integer>>> adj, int S)
    {
        PriorityQueue<Pair1> pq = new PriorityQueue<>(Comparator.comparingInt(p -> p.first));
        int [] dist = new int[V];
        Arrays.fill(dist, Integer.MAX_VALUE);
        dist[S] = 0;
        pq.add(new Pair1(0, S));

        while(!pq.isEmpty()){
            Pair1 p = pq.poll();
            int dis = p.first;
            int node = p.second;

            for(int i=0;i<adj.get(node).size();i++){
                int weight = adj.get(node).get(i).get(1);
                int adjNode = adj.get(node).get(i).get(0);

                if(dis + weight < dist[adjNode]){
                    dist[adjNode] = dis + weight;
                    pq.add(new Pair1(dist[adjNode], adjNode));
                }
            }
        }
        return dist;
    }

    public static void main(String[] args) {
        int V = 8;
        int S = 0;
        System.out.println(Arrays.toString(dijkstra(V, new ArrayList<>(), S)));
    }
}
