package com.practice.graph;

//https://practice.geeksforgeeks.org/problems/find-the-number-of-islands/1
public class NumberOfIsland {

    static int [][] direction = {{0,1},{1,0},{0,-1},{-1,0},{1,1},{1,-1},{-1,1},{-1,-1}};

    // Function to find the number of islands.
    public static int numIslands(char[][] grid) {
        boolean [][] visited = new boolean[grid.length][grid[0].length];
        int count = 0;
        for(int i=0;i<grid.length;i++){
            for(int j=0;j<grid[0].length;j++){
                if(!visited[i][j] && grid[i][j] == '1'){
                    dfs(grid, visited, i, j);
                    count++;
                }
            }
        }
        return count;
    }

    public static void dfs(char [][] grid, boolean [][] visited, int row, int col){
        visited[row][col] = true;
        for(int [] dir : direction){
            int nextR = dir[0] + row;
            int nextC = dir[1] + col;
            if(nextR >=0 && nextR < grid.length && nextC >=0 && nextC < grid[0].length && !visited[nextR][nextC] && grid[nextR][nextC] == '1'){
                dfs(grid, visited, nextR, nextC);
            }
        }
    }
    public static void main(String[] args) {
        char [][] grid = {};
        System.out.println(numIslands(grid));
    }
}
