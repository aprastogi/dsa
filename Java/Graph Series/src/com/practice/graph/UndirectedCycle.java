package com.practice.graph;

import java.util.ArrayList;

//https://practice.geeksforgeeks.org/problems/detect-cycle-in-an-undirected-graph/1
public class UndirectedCycle {
    // Function to detect cycle in an undirected graph.
    public static boolean isCycle(int V, ArrayList<ArrayList<Integer>> adj) {
        boolean [] visited = new boolean[V];
        for(int i=0;i<V;i++){
            if(!visited[i]){
                if(detectCycle(i, -1, adj, visited))
                    return true;
            }
        }
        return false;
    }

    public static boolean detectCycle(int node, int parent, ArrayList<ArrayList<Integer>> adj, boolean [] visited){
        visited[node] = true;
        for(Integer it : adj.get(node)){
            if(!visited[it]){
                if(detectCycle(it, node, adj, visited))
                    return true;
            }
            else if(parent != it){
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        int v = 8;
        System.out.println(isCycle(v, new ArrayList<>()));
    }
}
