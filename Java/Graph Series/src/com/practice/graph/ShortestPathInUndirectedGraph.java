package com.practice.graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

//https://practice.geeksforgeeks.org/problems/shortest-path-in-undirected-graph-having-unit-distance/1
public class ShortestPathInUndirectedGraph {

    public static int[] shortestPath(int[][] edges,int n ,int src) {
        ArrayList<ArrayList<Integer>> adj = new ArrayList<>();
        for(int i=0;i<n;i++){
            adj.add(new ArrayList<>());
        }
        for(int [] edge : edges){
            int u = edge[0];
            int v = edge[1];
            adj.get(u).add(v);
            adj.get(v).add(u);
        }
        // Applying simple bfs to find the shortest path as it is given that graph is having unit weight
        Queue<Integer> que = new LinkedList<>();
        que.offer(src);
        int [] dist = new int[n];
        Arrays.fill(dist, Integer.MAX_VALUE);
        dist[src] = 0;
        while(!que.isEmpty()){
            int node = que.poll();
            for(Integer it : adj.get(node)){
                if(dist[node] + 1 < dist[it]){
                    dist[it] = 1 + dist[node];
                    que.offer(it);
                }

            }
        }

        for(int i=0;i<dist.length;i++){
            if(dist[i] == Integer.MAX_VALUE){
                dist[i] = -1;
            }
        }
        return dist;
    }

    public static void main(String[] args) {
        int n = 4;
        int src = 0;
        System.out.println(Arrays.toString(shortestPath(new int[][]{}, n, src)));
    }
}
