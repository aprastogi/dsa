package com.practice.graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

//https://practice.geeksforgeeks.org/problems/course-schedule/1
public class CourseSchedule {

    static void dfs(int node, ArrayList<ArrayList<Integer>> adj, boolean [] visited, Stack<Integer> st){
        visited[node] = true;
        for(Integer it : adj.get(node)){
            if(!visited[it]){
                dfs(it, adj, visited, st);
            }
        }
        st.push(node);
    }

    static boolean detectCycle(ArrayList<ArrayList<Integer>> adj, int n){
        boolean [] visited = new boolean[n];
        boolean [] dfsVisited = new boolean[n];
        for(int v = 0; v< n; v++){
            if(!visited[v]){
                if(isCycle(v, adj, visited, dfsVisited))
                    return true;
            }

        }
        return false;
    }


    static boolean isCycle(int s, ArrayList<ArrayList<Integer>> adj, boolean [] visited, boolean [] dfsVisited){
        visited[s] = true;
        dfsVisited[s] = true;
        for(int element : adj.get(s)){
            if(!visited[element]){
                if(isCycle(element, adj, visited, dfsVisited))
                    return true;
            } else if(dfsVisited[element]) {
                return true;
            }
        }
        dfsVisited[s] = false;
        return false;
    }

    static int[] findOrder(int n, ArrayList<ArrayList<Integer>> prerequisites)
    {
        ArrayList<ArrayList<Integer>> adj = new ArrayList<>();
        Stack<Integer> st = new Stack<>();
        boolean [] visited = new boolean[n];
        for(int i=0;i<n;i++){
            adj.add(new ArrayList<>());
        }
        for(List<Integer> list : prerequisites){
            adj.get(list.get(1)).add(list.get(0));
        }
        if(detectCycle(adj, n))
            return new int[]{};
        for(int i=0;i<n;i++){
            if(!visited[i]){
                dfs(i, adj, visited, st);
            }
        }
        int [] result = new int[st.size()];
        int ndx = 0;
        while(!st.isEmpty()){
            result[ndx++] = st.pop();
        }
        return result;
    }

    public static void main(String[] args) {
        int n = 4;
        System.out.println(Arrays.toString(findOrder(n, new ArrayList<>())));
    }
}
