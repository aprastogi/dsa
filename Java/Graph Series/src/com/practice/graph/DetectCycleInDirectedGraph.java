package com.practice.graph;

import java.util.ArrayList;

//https://practice.geeksforgeeks.org/problems/detect-cycle-in-a-directed-graph/1
public class DetectCycleInDirectedGraph {

    // Function to detect cycle in a directed graph.
    public static boolean isCyclic(int V, ArrayList<ArrayList<Integer>> adj) {
        boolean [] visited = new boolean[V];
        boolean [] dfsVisited = new boolean[V];
        for(int i=0;i<V;i++){
            if(!visited[i]){
                if(dfs(adj,visited, dfsVisited, i))
                    return true;
            }
        }
        return false;
    }

    public static boolean dfs(ArrayList<ArrayList<Integer>> adj, boolean [] visited, boolean [] dfsVisited, int node){
        visited[node] = true;
        dfsVisited[node] = true;
        for(Integer it : adj.get(node)){
            if(!visited[it]){
                if(dfs(adj, visited, dfsVisited, it)){
                    return true;
                }
            }
            else if(dfsVisited[it]){
                return true;
            }
        }
        // Backtrack
        dfsVisited[node] = false;
        return false;
    }

    public static void main(String[] args) {
        int V = 8;
        System.out.println(isCyclic(V, new ArrayList<>()));
    }
}
