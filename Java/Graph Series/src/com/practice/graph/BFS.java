package com.practice.graph;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

//https://practice.geeksforgeeks.org/problems/bfs-traversal-of-graph/1
public class BFS {

    // Function to return Breadth First Traversal of given graph.
    public static ArrayList<Integer> bfsOfGraph(int V, ArrayList<ArrayList<Integer>> adj) {
        int source = 0;
        Queue<Integer> que = new LinkedList<>();
        boolean [] visited = new boolean[V];
        ArrayList<Integer> list = new ArrayList<>();
        que.offer(source);
        visited[source] = true;
        while(!que.isEmpty()){
            int node = que.poll();
            list.add(node);
            for(Integer it : adj.get(node)){
                if(!visited[it]){
                    visited[it] = true;
                    que.offer(it);
                }
            }
        }
        return list;
    }

    public static void main(String[] args) {
        System.out.println(bfsOfGraph(8, new ArrayList<>()));
    }
}
