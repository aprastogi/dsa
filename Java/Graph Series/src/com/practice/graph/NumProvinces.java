package com.practice.graph;

import java.util.ArrayList;

//https://practice.geeksforgeeks.org/problems/number-of-provinces/1
public class NumProvinces {

    static int numProvinces(ArrayList<ArrayList<Integer>> adj, int V) {
        ArrayList<ArrayList<Integer>> adjList = new ArrayList<>();
        boolean [] visited = new boolean[V];
        int count = 0;
        for(int i=0;i<V;i++){
            adjList.add(new ArrayList<>());
        }
        // To change adjacency matrix to list
        for(int i=0;i<adj.size();i++){
            for(int j=0;j<adj.get(0).size();j++){
                if(adj.get(i).get(j) == 1 && i != j){
                    adjList.get(i).add(j);
                    adjList.get(j).add(i);
                }
            }
        }
        for(int i=0;i<adjList.size();i++){
            if(!visited[i]){
                dfs(adjList, visited, i);
                count++;
            }
        }
        return count;
    }

    static void dfs(ArrayList<ArrayList<Integer>> adj, boolean [] visited, int node){
        visited[node] = true;
        for(Integer it : adj.get(node)){
            if(!visited[it]){
                dfs(adj, visited, it);
            }
        }
    }

    public static void main(String[] args) {
        int V = 8;
        System.out.println(numProvinces(new ArrayList<>(), V));
    }
}
