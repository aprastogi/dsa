package com.practice.streams;

public class MyException extends Exception {

    public MyException(String msg){
        super(msg);
    }

    public static void main(String [] args){
        int a = 5, b = 0;
        try{
            if(b == 0)
                throw new MyException("Can not divide by zero");
        }
        catch(MyException e){
            System.out.println(e.getMessage());
        }
    }
}
