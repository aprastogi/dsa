package com.practice.streams;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class StreamMain {

    /*
    stream(), filter(), map(), count(), sorted(), min(), max(), foreach()
    */

    public static void main(String [] args){
        List<Integer> list = Arrays.asList(new Integer []{1,2,3,4,5,6,7});
        System.out.println(list);
        System.out.println("Min value " + list.stream().min((o1,o2) -> o1 - o2).get());
        System.out.println(list.stream().filter(i->i % 2 == 0).collect(Collectors.toList()));

        List<String> strList = Arrays.asList(new String [] {"hello","world","hello","hello"});
        HashMap<String,Integer> map = new HashMap();
        
        strList.stream().forEach(str -> map.put(str, map.getOrDefault(str,0)+1));
        map.entrySet().stream().forEach(System.out::println);


    }
}
