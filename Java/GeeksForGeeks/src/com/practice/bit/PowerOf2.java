package com.practice.bit;

public class PowerOf2 {

	public static boolean powerOf2(int x) {
		return x!=0 && ((x&(x-1)) == 0); 
	}
	
	public static void main(String[] args) {
		int n = 4;
		System.out.println(powerOf2(n));
	}

}
