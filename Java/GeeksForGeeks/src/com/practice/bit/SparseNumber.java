package com.practice.bit;

public class SparseNumber {

	public static boolean sparseNumber(int n) {
		int count = 0;
		while(n != 0) {
			n = n & (n << 1);
			count++;
		}
		if(count < 2)
			return true;
		return false;
	}
	
	public static void main(String[] args) {
		int n = 10;
        System.out.println(sparseNumber(n));
	}

}
