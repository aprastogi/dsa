package com.practice.bit;

public class countTotalBits {

	static int countSetBits( int n) 
    { 
        int bitCount = 0;       
        for (int i = 1; i <= n; i++) 
            bitCount += BitDifference.countSetBits(i); 
      
        return bitCount; 
    } 
	
	public static void main(String[] args) {
		int n = 17;
        System.out.println(countSetBits(n));
	}

}
