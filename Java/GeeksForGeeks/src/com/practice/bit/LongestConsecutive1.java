package com.practice.bit;

public class LongestConsecutive1 {

	public static int findLongest(int n) {
		int count = 0;
		while(n != 0) {
			n = n & (n << 1);
			count++;
		}
		return count;
	}
	
	public static void main(String[] args) {
		int n = 222;
        System.out.println(findLongest(n));
	}

}
