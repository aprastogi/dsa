package com.practice.bit;

public class ToggleBits {
	
	public static int toggleBits(int n, int l, int r) {
		int i= 0;
		int power = 0, val = 0;
		while(i < l) {
			if((n & 1) == 1)
				val += Math.pow(2, power);
			power += 1;
			n >>= 1;
			i++;
		}
		while(i <= r) {
			if((n & 1) == 0)
				val += Math.pow(2, power);
			power += 1;
			n >>= 1;
			i++;
		}
		while(n > 0) {
			if((n & 1) == 1)
				val += Math.pow(2, power);
			power += 1;
			n >>= 1;
			i++;
		}
		return val;
		
	}
	
	public static void main(String[] args) {
		int n = 50 , l = 2, r = 5;
		System.out.println(toggleBits(n, l-1, r-1));
	}
}
