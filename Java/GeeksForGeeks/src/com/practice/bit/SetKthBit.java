package com.practice.bit;

public class SetKthBit {

	public static int setKthBit(int n, int k) {
		int i= 0;
		int val = 0, power = 0;
		while(i <= 31) {
			if((n & 1) == 1 || ((n & 1) == 0 && i == k))
				val += Math.pow(2, power);			
			power += 1;
			n >>= 1;
		    i++;
		}
		return val;
	}
	
	public static void main(String[] args) {
		int n = 10, k = 2;
		System.out.println(setKthBit(n,k));

	}

}
