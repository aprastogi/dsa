package com.practice.bit;

public class FirstSetBitFromRight {

	public static int find(int n) {
		int count = 1;
		while(n > 0) {
			if((n & 1) == 1)
				return count;
			n >>= 1;
			count++;
		}
		return 0;
	}
	public static void main(String[] args) {
		int n = 48;
		System.out.println(find(n));
	}

}
