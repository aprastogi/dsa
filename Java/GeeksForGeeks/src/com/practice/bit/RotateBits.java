package com.practice.bit;

public class RotateBits {

	public static int leftRotate(int n, int d) {
	     return (n << d) | (n >> (16 - d)); 
	}
	
	public static int rightRotate(int n, int d) {
		return (n >> d) | (n << (16 - d));
	}
	
	public static void main(String[] args) {
		int n= 229, d = 3;
		System.out.println(leftRotate(n,d));
		System.out.println(rightRotate(n,d));
	}
}
