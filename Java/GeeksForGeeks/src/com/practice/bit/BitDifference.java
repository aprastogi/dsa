package com.practice.bit;

public class BitDifference {

	public static int countSetBits(int num) {
		int count = 0;
		while(num != 0) {
			if((num & 1) == 1)
			  count++;
			num >>= 1;
		}
		return count;
	}
	
	public static int bitDifference(int A, int B) {
		return countSetBits(A ^ B);
	}
	
	public static void main(String[] args) {
		int A = 10, B = 20;
		System.out.println(bitDifference(A,B));
	}

}
