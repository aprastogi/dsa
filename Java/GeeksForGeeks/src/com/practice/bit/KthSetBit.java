package com.practice.bit;

public class KthSetBit {

	public static String findKthBit(int n, int k) {
		n >>= k;
		if((n & 1) == 1)
			return "YES";
		return "NO";
	}
	
	public static void main(String[] args) {
		int n = 4 , k = 2;
		System.out.println(findKthBit(n, k));
	}

}
