package com.practice.divideconquer;

public class QuickSort {

	public static void swap(int [] arr, int i, int j) {
		int temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}

	public static int partition(int arr[], int p , int r) {
		int x = arr[r];
		int i = p - 1;
		for(int j= p ; j<= r-1;j++) {
			if(arr[j] <= x) {
				i = i + 1;
				swap(arr,i,j);
			}
		}
		swap(arr,i+1,r);
		return i+1;
	}
	
	public static void quickSort(int [] arr, int p, int r) {
		if(p < r) {
			int q = partition(arr, p, r);
			quickSort(arr, p, q-1);
			quickSort(arr, q+1, r);
		}
	}
	
	public static void main(String[] args) {
		int [] arr = {3,4,7,8,1,10,2};
		quickSort(arr, 0, arr.length-1);
		for(int item : arr)
			System.out.print(item + " ");
	}
}
