package com.practice.divideconquer;

public class SearchInRotatedSorted {

	public static int search(int [] arr, int ele) {
		int start = 0, end = arr.length - 1;
		while(start <= end) {
			int mid = start + (end - start)/2;
			if(arr[mid] == ele)
				return mid;
			if(arr[start] < arr[mid]) {
				if(arr[start] <= ele && arr[mid] > ele)
					end = mid -1;
				else
					start = mid + 1;
			}
			else {
				if(arr[mid] < ele && arr[end] >= ele)
					start = mid + 1;
				else
					end = mid - 1;
			}
		}
		return -1;
	}
	
	public static void main(String[] args) {
		int [] arr = {5,6 ,7 ,8 ,9 ,10, 1, 2, 3};
        System.out.println(search(arr,10));
	}

}
