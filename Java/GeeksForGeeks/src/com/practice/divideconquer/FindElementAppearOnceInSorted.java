package com.practice.divideconquer;

public class FindElementAppearOnceInSorted {

	public static int findSingleElement(int [] arr) {
		int start = 0, end = arr.length -1 ;
		while(start <= end) {
			int mid = start + (end - start)/2;
			if(start == end) 
				return arr[start];
			if(mid < arr.length -2 && mid %2 == 0) {
				 if(arr[mid] == arr[mid+1])
					 start = mid + 1 ; 
				 else
					 end = mid;
			}
			else if(mid > 0 && mid %2 == 1) {
				if(arr[mid] == arr[mid-1])
					start = mid + 1 ;
				else
					end = mid;
			}
		}
		return -1;
	}
	
	public static void main(String[] args) {
		int arr[] = {1,1,2,2,3,3,4,5,5,6,6};
		System.out.println(findSingleElement(arr));
	}

}
