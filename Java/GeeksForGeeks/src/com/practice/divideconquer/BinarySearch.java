package com.practice.divideconquer;

public class BinarySearch {

	public static int binarySearch(int [] arr, int start, int end, int ele) {
		if(start > end) 
		   return -1;
		int mid = start + (end - start)/2;
		if(arr[mid] == ele)
			return mid;
		if(arr[mid] < ele)
			return binarySearch(arr, mid+1, end, ele);
		return binarySearch(arr, start, mid -1, ele);
	}
	
	public static int binarySearchTemplate(int [] arr, int ele) {
		int left = 0;
		int right = arr.length -1 ;
		while(left < right) {
			int mid = left + (right - left)/2;
			if(arr[mid] >= ele)
				right = mid;
			else
				left = mid + 1;
		}	
		if(arr[left] == ele)
			return left;
		return -1;
	}
	
	public static void main(String[] args) {
		int arr[] = {11,22,33,44,55};
		System.out.println(binarySearch(arr,0,arr.length-1,44));
		System.out.println(binarySearchTemplate(arr, 33));
	}

}
