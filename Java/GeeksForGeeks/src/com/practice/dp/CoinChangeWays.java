package com.practice.dp;

public class CoinChangeWays {

	public static int coinChangeWays(int [] arr, int amount) {
		int dp[][] = new int[arr.length+1][amount+1];
		for(int i=0;i<dp[0].length;i++)
			dp[0][i] = 0;
		for(int i=0;i<dp.length;i++)
			dp[i][0] = 1;
		for(int i=1;i<dp.length;i++) {
			for(int j=1;j<dp[0].length;j++) {
				if(arr[i-1] <= j) 
					dp[i][j] = dp[i][j-arr[i-1]] + dp[i-1][j];
				else
				    dp[i][j] = dp[i-1][j];
			}
		}
		return dp[dp.length-1][dp[0].length-1];
	}
	
	public static void main(String[] args) {
		int arr[] = {2,5,3,6};
		int amount = 10;
		System.out.println(coinChangeWays(arr,amount));
	}

}
