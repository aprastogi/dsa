package com.practice.dp;

import java.util.Arrays;

public class MinJumps {

	public static int minJumps(int [] arr) {
		int dp[] = new int[arr.length];
		Arrays.fill(dp,Integer.MAX_VALUE-1);
        dp[0] = 0;
	    for(int i=1;i<dp.length;i++) {
	    	for(int j=0;j<i;j++) {
	    		if(j + arr[j] >= i) {
	    			dp[i] = Math.min(dp[i], dp[j] + 1);
	    			break;
	    		}
	    	}
	    }
	    return dp[dp.length-1];
	}
	
	public static void main(String[] args) {
		int arr[] = {1,3,5,8,9,2,6,7,6,8,9};
        System.out.println(minJumps(arr));
	}

}
