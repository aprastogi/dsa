package com.practice.dp;

import java.util.Arrays;

public class CountHops {

	public static int countHops(int n) {
		if(n == 0 || n == 1)
			return 1;
		if(n == 2)
			return 2;
		return countHops(n-1) + countHops(n-2) + countHops(n-3);
	}
	
	public static int countHopsTD(int n, int [] dp) {
		if(n == 0 || n == 1)
			return 1;
		if(n == 2)
			return 2;
		int first = 0, second = 0, third = 0;
		if(dp[n-1] != -1)
			return dp[n-1];
		else {
			first = countHops(n-1);
            dp[n-1] = first;
		}
		if(dp[n-2] != -1)
			return dp[n-1];
		else {
			second = countHops(n-2);
            dp[n-2] = first;
		}
		if(dp[n-3] != -1)
			return dp[n-3];
		else {
			third = countHops(n-3);
            dp[n-3] = first;
		}
		return first + second + third;
	}
	
	public static int countHopsBottom(int n) {
		int dp[] = new int[n+1];
		dp[0] = 1;dp[1] = 1;dp[2] = 2;
		for(int i= 3;i<dp.length;i++)
			dp[i] = dp[i-1] + dp[i-2] + dp[i-3];
		return dp[dp.length-1];
	}
	
	public static void main(String[] args) {
		int n = 5;
		int dp[] = new int[n+1];
		Arrays.fill(dp,-1);
        System.out.println(countHops(n));
        System.out.println(countHopsTD(n, dp));
        System.out.println(countHopsBottom(n));
	}

}
