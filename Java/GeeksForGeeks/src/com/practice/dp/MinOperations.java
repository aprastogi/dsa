package com.practice.dp;

import java.util.Arrays;

public class MinOperations {

	public static int minOperationsBottomUp(int n) {
		int [] dp = new int[n+1];
		dp[1] = 1;
		for(int i=2;i<=n;i++) {
			if(i % 2 == 1)
				dp[i] = 1 + dp[i-1];
			else
			    dp[i] = 1 + Math.min(dp[i/2], dp[i-1]);
		}
		return dp[n];
	}
	
	public static int minOperations(int n) {
		if(n == 0)
			return 0;
		if(n % 2 == 1)
			return 1 + minOperations(n-1);
		return 1 + Math.min(minOperations(n-1), minOperations(n/2));
	}
	
	public static int minOperationsTD(int n, int [] dp) {
		if(n == 0)
			return 0;
		if(dp[n] != -1)
			return dp[n];
		if(n % 2 == 1)
			return dp[n] = 1 +  minOperationsTD(n-1, dp);
		return dp[n] = 1 + Math.min(minOperationsTD(n-1, dp), minOperationsTD(n/2, dp));
	}
	
	public static void main(String[] args) {
		int n = 7;
		int dp[] = new int[n+1];
		Arrays.fill(dp, -1);
		System.out.println(minOperations(n));
		System.out.println(minOperationsBottomUp(n));
		System.out.println(minOperationsTD(n,dp));
	}
}
