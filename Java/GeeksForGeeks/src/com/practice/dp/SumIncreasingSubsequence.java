package com.practice.dp;

public class SumIncreasingSubsequence {

	public static int sumIncreasingSubsequence(int [] arr) {
		int max = Integer.MIN_VALUE;
		int dp[] = new int[arr.length];
		for(int i=0;i<arr.length;i++)
			dp[i] = arr[i];
		for(int i=1;i<dp.length;i++) {
			for(int j=0;j<i;j++) {
				if(arr[i] > arr[j]) {
					dp[i] = Math.max(dp[i], dp[j] + arr[i]);
				}
			}
		}
		for(int item : dp) {
			max = Math.max(max, item);
		}
		return max;
	}
	
	public static void main(String[] args) {
		int [] arr = {1,101,2,3,100,4,5};
		System.out.println(sumIncreasingSubsequence(arr));
	}

}
