package com.practice.dp;

import java.util.Arrays;
import java.util.Comparator;

public class MaxLengthChain {

	public static int maxLengthChain(int [][] arr) {
		int max = Integer.MIN_VALUE;
		int dp[] = new int[arr.length];
		Arrays.fill(dp, 1);
		for(int i=1;i<dp.length;i++) {
			for(int j=0;j<i;j++) {
				if(arr[i][0] > arr[j][1]) {
					dp[i] = Math.max(dp[i], dp[j] + 1);
				}
			}
		}
		for(int item : dp) {
			max = Math.max(max, item);
		}
		return max;
	}
	
	public static void main(String[] args) {
		int arr[][] = {{5,24},{39,60},{15,28},{27,40},{50,90}};
		Arrays.sort(arr, new Comparator<int[]>() {
			public int compare(int [] first, int[] second) {
				return first[0] - second[0];
			}
		});
        System.out.println(maxLengthChain(arr));
	}

}
