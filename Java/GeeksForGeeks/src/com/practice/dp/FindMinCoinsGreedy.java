package com.practice.dp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FindMinCoinsGreedy {

	public static List<Integer> findMinCoins(int [] arr, int amount) {
		List<Integer> result = new ArrayList<Integer>();
		for(int i=arr.length-1;i>=0;i--) {
			while(amount >= arr[i]) {
				amount -= arr[i];
				result.add(arr[i]);
			}
		}
		return result;
	}
	
	public static void main(String[] args) {
		int [] arr = { 1, 2, 5, 10, 20, 50, 100, 200, 500, 2000};
		Arrays.sort(arr);
		int amount = 43;
		List<Integer> result= findMinCoins(arr, amount);
		for(int i=0;i<result.size();i++)
			System.out.print(result.get(i) + " ");
	}
}
