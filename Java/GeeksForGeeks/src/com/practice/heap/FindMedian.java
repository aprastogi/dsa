package com.practice.heap;

import java.util.Collections;
import java.util.PriorityQueue;

public class FindMedian {

	public static void findMedian(double [] arr) {
		PriorityQueue<Double> max = new PriorityQueue<Double>(Collections.reverseOrder());
		PriorityQueue<Double> min = new PriorityQueue<Double>();
		System.out.println(arr[0]);
		max.add(arr[0]);
		double mid = arr[0];
		for(int i=1;i<arr.length;i++) {
			if(max.size() == min.size()) {
				if(arr[i] < mid) {
					max.add(arr[i]);
					System.out.println(max.peek());
				}
				else {
					min.add(arr[i]);
					System.out.println(min.peek());
				}
			}
			else if(max.size() > min.size()) {
				if (arr[i] < mid) 
	            { 
	                min.add(max.remove()); 
	                max.add(arr[i]);
	            } 
	            else
	                min.add(arr[i]);
	  
	            System.out.println((max.peek() + min.peek())/2.0);
			}
			else {
				if (arr[i] > mid) 
	            { 
	                max.add(min.remove()); 
	                min.add(arr[i]);
	            } 
	            else
	                max.add(arr[i]);
	  
	            System.out.println((max.peek() + min.peek())/2.0);
			}
		}
		
	}
	
	public static void main(String[] args) {
		double arr[] = {5, 15, 10, 20, 3}; 
		findMedian(arr);
	}

}
