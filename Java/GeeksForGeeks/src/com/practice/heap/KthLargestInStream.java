package com.practice.heap;

import java.util.PriorityQueue;

public class KthLargestInStream {

	public static void solve(int [] arr, int k) {
		PriorityQueue<Integer> min = new PriorityQueue<Integer>();
		for(int i=0;i<arr.length;i++) {
			min.add(arr[i]);
			if(min.size() < k) {
				System.out.println(-1);				
			}
			else {
				System.out.println(min.remove());				
			}
		}
	}
	
	public static void main(String[] args) {
		int [] arr = {1,2,3,4,5,6};
		solve(arr,4);
	}

}
