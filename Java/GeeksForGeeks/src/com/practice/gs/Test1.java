package com.practice.gs;

public class Test1 {

	public static int getMinNumberForPattern(String seq) 
    { 
        int n = seq.length();   
        if (n >= 9) 
            return -1;   
        int result[] = new int[n + 1];  
        int count = 1; 
        for (int i = 0; i <= n; i++)  { 
            if (i == n || seq.charAt(i) == 'I') { 
                for (int j = i - 1; j >= -1; j--) { 
                    result[j + 1] = count++; 
                    if (j >= 0 && seq.charAt(j) == 'I') 
                        break; 
                } 
            } 
        } 
        int num =0;
        for(int item : result) {
        	num = num * 10 + item;
        }
        return num;
    } 
	
	public static void main(String[] args) {
		String inputs[] = { "IDID", "I", "DD", "II", "DIDI", "IIDDD", "DDIDDIID" }; 		  
        for(String input : inputs)        
            System.out.println(getMinNumberForPattern(input)); 
	}
}
