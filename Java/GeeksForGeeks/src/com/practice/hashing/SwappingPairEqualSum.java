package com.practice.hashing;

import java.util.HashSet;

public class SwappingPairEqualSum {

	public static boolean solve(int [] arr1, int [] arr2) {
		HashSet<Integer> set = new HashSet<Integer>();
		int sum1 = 0, sum2= 0;
		for(int item: arr1) {
			sum1 += item;
			set.add(item);
		}
		for(int item: arr2)
			sum2 += item;
		int diff = (sum1 - sum2)/2;
		for(int item : arr2) {
			if(set.contains(item + diff)) {
				System.out.println(item + " " + (item + diff));
				return true;
			}
		}
		return false;
	}
	
	public static void main(String[] args) {
		int arr1[] = {5,7,4,6};
		int arr2[] = {1,2,3,8};
		System.out.println(solve(arr1,arr2));
	}

}
