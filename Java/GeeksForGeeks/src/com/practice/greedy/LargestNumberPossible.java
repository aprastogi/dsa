package com.practice.greedy;

public class LargestNumberPossible {

	public static int[] findNumber(int number, int sum) {
		int result [] = new int[number];
		if(sum > 9* number)
			return new int[]{-1};
		for(int i=0;i<number;i++) {
			if(sum > 9) {
				result[i] = 9;
				sum -= 9;
			}
			else {
				result[i] = sum;
				sum = 0;
			}
		}
		return result;
	}
	
	public static void main(String[] args) {
		int number = 2, sum = 9;
		int [] result = findNumber(number,sum);
		for(int item : result)
			System.out.print(item);
	}

}
