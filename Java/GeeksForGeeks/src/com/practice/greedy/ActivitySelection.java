package com.practice.greedy;

public class ActivitySelection {

	public static int maxActivities(int [] start, int [] end) {
		int count = 1;
		int i= 0;
	    for(int j=1;j<start.length;j++) {
	    	if(start[j] >= end[i]) {
	    		count++;
	    		i = j;
	    	}
	    }
		return count;
	}
	
	public static void main(String[] args) {
		int start[] = {1, 3, 2, 5, 8, 5};
		int end[]   = {2 ,4, 6, 7, 9, 9};
		// if end time is not already sorted then create 2-d array to sort whole according to end time 
        System.out.println(maxActivities(start,end));
	}

}
