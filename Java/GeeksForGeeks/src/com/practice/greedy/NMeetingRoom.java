package com.practice.greedy;

import java.util.ArrayList;
import java.util.List;

public class NMeetingRoom {

	public static List<Integer> maxActivities(int [] start, int [] end) {
		List<Integer> result = new ArrayList<Integer>();
		int i= 0;
		result.add(1);
	    for(int j=1;j<start.length;j++) {
	    	if(start[j] >= end[i]) {
	    	    result.add(j+1);
	    		i = j;
	    	}
	    }
		return result;
	}
		
	public static void main(String[] args) {
		int start[] = {1, 3, 0, 5, 8, 5};
		int end[]	= {2, 4, 6, 7, 9, 9};
		// if end time is not already sorted then create 2-d array to sort whole according to end time 
        List<Integer> result = maxActivities(start, end);
        for(int i=0;i<result.size();i++)
        	System.out.print(result.get(i) + " ");
	}
}
