package com.practice.greedy;

import java.util.Arrays;

public class MaximizeToys {

	public static int maxToys(int [] toys, int amount) {
		Arrays.sort(toys);
		int result = 0, sum = 0;
		for(int i=0;i<toys.length;i++) {
			if(sum + toys[i] <= amount) {
				sum += toys[i];
				result++;
			}
		}
		return result;
	}
	
	public static void main(String[] args) {
		int toys[] = {1 ,12 ,5 ,111, 200, 1000, 10};
		int amount = 50;
        System.out.println(maxToys(toys,amount));
	}

}
