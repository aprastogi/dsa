package com.practice.greedy;

import java.util.Arrays;

public class MinMaxCandies {

	public static int min(int [] arr, int k) {
		int ans = 0;
		int start = 0, end = arr.length -1;
		while(start <= end) {
			ans += arr[start++];
			end -= k;
		}
		return ans;
	}
	
	public static int max(int [] arr, int k) {
		int ans = 0;
		int start = 0, end = arr.length -1;
		while(start <= end) {
			ans += arr[end--];
			start += k;
		}
		return ans;
	}
	
	public static void main(String[] args) {
		int [] arr = {3,2,1,4};
		int k = 2;
        Arrays.sort(arr);
        System.out.println(min(arr,k));
        System.out.println(max(arr,k));
	}

}
