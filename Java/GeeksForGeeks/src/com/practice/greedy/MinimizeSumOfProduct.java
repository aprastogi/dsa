package com.practice.greedy;

import java.util.Arrays;

public class MinimizeSumOfProduct {

	public static int minimizeProduct(int [] first, int [] second) {
		Arrays.sort(first);
		Arrays.sort(second);
		int total = 0;
		for(int i=0;i<first.length;i++) {
			total += first[i] * second[second.length-i-1];
		}
		return total;
	}
	
	public static void main(String[] args) {
		int [] first = {6, 1, 9, 5, 4};
		int [] second = {3, 4, 8, 2, 4};
        System.out.println(minimizeProduct(first,second));
	}

}
