package com.practice.greedy;

public class CoinPiles {

	public static int minRemoved(int [] arr, int k) {
		int minCoin = 0, minValue = 1;
		for(int i=0;i<arr.length;i++) {
			int diff = arr[i] - minValue;
			if(diff >= k) {
				minCoin += diff - k;				
			}
		}
		return minCoin;
	}
	
	public static void main(String[] args) {		
        int [] arr = {1 ,5 ,1 ,2 ,5 ,1};
        int k = 3;
        System.out.println(minRemoved(arr,k));
	}
}
