package com.practice.linkedList;

class Node {
	int data;
	Node next;
	
	public Node(int data) {
		this.data = data;
		this.next = null;
	}
}

public class LinkedList {	
	Node head;	
	
	public static void print(Node head) {
		if(head == null)
			return;
		System.out.print(head.data);
		if(head.next != null)
			System.out.print("-->");
		print(head.next);
	}
	
	public static int middleElement(Node head) {
		Node slow = head, fast = head;
		while(fast != null && fast.next != null) {
			slow = slow.next;
			fast = fast.next.next;
		}
		return slow.data;
	}
	
	public static void main(String[] args) {
	      LinkedList ls = new LinkedList();
	      ls.head = new Node(1);
	      ls.head.next = new Node(2);
	      ls.head.next.next = new Node(3);
	      ls.head.next.next.next = new Node(4);
	      ls.head.next.next.next.next = new Node(5);
	      ls.head.next.next.next.next.next = new Node(6);
	      print(ls.head);
	      System.out.println("\nMiddle Element is " + middleElement(ls.head));
	}

}
