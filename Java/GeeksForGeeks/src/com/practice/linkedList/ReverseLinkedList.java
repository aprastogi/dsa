package com.practice.linkedList;

public class ReverseLinkedList {	
    Node head;
	
    public static Node reverseList(Node head) {
    	if(head == null || head.next == null)
    		return head;
    	Node reverse = reverseList(head.next);
    	head.next.next = head;
    	head.next = null;
    	return reverse;
    }
    
	public static void main(String[] args) {
	   	  ReverseLinkedList ls = new ReverseLinkedList();
	      ls.head = new Node(1);
	      ls.head.next = new Node(2);
	      ls.head.next.next = new Node(3);
	      ls.head.next.next.next = new Node(4);
	      ls.head.next.next.next.next = new Node(5);
	      ls.head.next.next.next.next.next = new Node(6);
          System.out.println("Before Reverse: ");
          LinkedList.print(ls.head);
          ls.head = reverseList(ls.head);
          System.out.println("\nAfter Reverse: ");
          LinkedList.print(ls.head);
	}
}
