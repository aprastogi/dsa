package com.practice.array;

public class KadaneAlgorithm {

	public static int maxSumSubarray(int [] arr) {
		int curr_max = arr[0];
		int max_so_far = arr[0];
		for(int i=1;i<arr.length;i++) {
			curr_max = Math.max(arr[i], curr_max + arr[i]);
			max_so_far = Math.max(curr_max, max_so_far);
		}
		return max_so_far;
	}
	
	public static void main(String[] args) {
		int arr[] = {1, 2, 3, -2, 5};
        System.out.println(maxSumSubarray(arr));
	}

}
