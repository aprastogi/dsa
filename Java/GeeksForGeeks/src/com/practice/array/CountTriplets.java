package com.practice.array;

import java.util.HashSet;

public class CountTriplets {

	public static int countTriplets(int arr[]) {
		int count = 0;
		HashSet<Integer> set = new HashSet<Integer> ();
		for(int item : arr)
			set.add(item);
		for(int i=0;i<arr.length - 1; i++) {
			for(int j=i+1;j<arr.length;j++) {
				if(set.contains(arr[i] + arr[j]))
					count++;
			}
		}
		return count;
	}
	
	public static void main(String[] args) {
		int arr[] = {1,2,3,5};
		System.out.println(countTriplets(arr));
	}

}
