package com.practice.array;

import java.util.HashMap;

public class SubarrayWithGivenSum {

	public static int[] getSubarrayWithMap(int [] arr, int sum) {
		int [] result = new int[2];
		HashMap<Integer,Integer> map = new HashMap<Integer,Integer>();
		int curr_sum = 0;
		for(int i=0;i<arr.length;i++) {
			curr_sum += arr[i];
			if(sum == curr_sum) {
				result[0] = 0;
				result[1] = i;
				break;
			}
			if(map.containsKey(curr_sum - sum)) {
				result[0] = map.get(curr_sum - sum) + 2;
				result[1] = i + 1;
				break;
			}
			map.put(curr_sum,i);
		}
		return result;
	}
	public static int[] getSubarray(int [] arr, int sum) {
		int [] result = new int[2];
		int start = 0;
		int s = 0;
		for(int end =0; end <= arr.length;end++) {
			while(start < end && s > sum) {
				s -= arr[start];
				start++;
			}
			if(s == sum) {
				result[0] = start+1;
				result[1] = end;
				break;
			}
			if(end < arr.length)
			    s += arr[end];
		}
		return result;
	}
	
	public static void main(String[] args) {
		int arr[] = {1,2,3,7,5};
		int sum = 12;
        int [] result = getSubarray(arr,sum);
        System.out.println(result[0] + " " + result[1]);
        result = getSubarrayWithMap(arr,sum);
        System.out.println(result[0] + " " + result[1]);      
    }
}
