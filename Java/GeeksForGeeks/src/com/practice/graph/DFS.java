package com.practice.graph;

import java.util.ArrayList;
import java.util.Iterator;

public class DFS {
    
	int v;
	ArrayList<Integer> adj[];
	
	@SuppressWarnings("unchecked")
	public DFS(int v) {
		this.v = v;
		adj = new ArrayList[v];
		for(int i=0;i<v;i++)
			adj[i] = new ArrayList<Integer>();
	}
	
	public void addEdge(int v, int w) {
		adj[v].add(w);
	}
	
	public void dfs(int source, boolean [] visited) {
		visited[source] = true;
		Iterator<Integer> it = adj[source].listIterator(); 
		System.out.println(source + " ");
        while (it.hasNext()) 
        {         	
            int element = it.next(); 
            if (!visited[element]) 
            {
                visited[element] = true;
                dfs(element, visited);
            }                   
        }      
	}
	
	public static void main(String[] args) {
		DFS dfs = new DFS(4);
		dfs.addEdge(0, 1); 
		dfs.addEdge(0, 2); 
		dfs.addEdge(1, 2); 
		dfs.addEdge(2, 0); 
		dfs.addEdge(2, 3); 
		dfs.addEdge(3, 3);   
        System.out.println("Following is Depth First Traversal "+ 
                           "(starting from vertex 2)"); 
        boolean visited[] = new boolean[4];  
        dfs.dfs(2,visited); 
	}
}
