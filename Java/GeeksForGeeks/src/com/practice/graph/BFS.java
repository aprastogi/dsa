package com.practice.graph;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

public class BFS {
    int v;
    ArrayList<Integer> adj[];
    
    @SuppressWarnings("unchecked")
	public BFS(int v) {
    	this.v = v;
    	adj = new ArrayList[v];
    	for(int i=0;i<v;i++)
    		adj[i] = new ArrayList<Integer>();
    }
    
    public void addEdge(int v, int w) {
    	adj[v].add(w);
    }
    
    public void bfs(int source, boolean [] visited) {
    	Queue<Integer> que = new LinkedList<Integer>();
    	visited[source] = true;
    	que.add(source);
    	while(!que.isEmpty()) {
    		source = que.poll();
    		System.out.println(source + " ");
    		Iterator<Integer> it = adj[source].listIterator(); 
    		while(it.hasNext()) {
    			int element = it.next();
    			if(visited[element] == false) {
    			    visited[element] = true;
    			    que.add(element);
    			}
    		}
    	}
    }
    
	public static void main(String[] args) {
		BFS bfs = new BFS(4);		  
		bfs.addEdge(0, 1); 
		bfs.addEdge(0, 2); 
		bfs.addEdge(1, 2); 
		bfs.addEdge(2, 0); 
		bfs.addEdge(2, 3); 
		bfs.addEdge(3, 3);   
        System.out.println("Following is Depth First Traversal "+ 
                           "(starting from vertex 2)");   
        boolean visited[] = new boolean[4]; 
        bfs.bfs(2,visited);       
	}
}
