package com.practice.graph;

import java.util.ArrayList;
import java.util.Iterator;

public class DetectCycleUndirectedGraph {

	int v;
	ArrayList<Integer> adj[];
	
	@SuppressWarnings("unchecked")
	public DetectCycleUndirectedGraph(int v) {
		this.v = v;
		adj = new ArrayList[v];
		for(int i=0;i<v;i++)
			adj[i] = new ArrayList<Integer>();
	}
	
	public void addEdge(int v, int w) {
		adj[v].add(w);
		adj[w].add(v);
	}
	
	public boolean isCycleUtil(int source, boolean [] visited , int parent) {
		visited[source] = true;
		Iterator<Integer> it = adj[source].listIterator(); 
        while (it.hasNext()) 
        {         	
            int element = it.next(); 
            if (!visited[element]) 
            {
                if(isCycleUtil(element, visited, source))
                	return true;
            }      
            else if(element != parent) {
            		return true;
            }
        } 
        return false;
	}
	
	public boolean isCycle() {
		boolean[] visited = new boolean[v];
		for(int i=0;i<v;i++) {
			if(!visited[i] && isCycleUtil(i,visited,-1))
				return true;
		}
		return false;
	}
	
	public static void main(String[] args) {
		DetectCycleUndirectedGraph g1 = new DetectCycleUndirectedGraph(5);
		g1.addEdge(1, 0); 
        g1.addEdge(0, 2); 
        g1.addEdge(2, 1); 
        g1.addEdge(0, 3); 
        g1.addEdge(3, 4); 
		if(g1.isCycle())
			System.out.println("Graph Contains Cycle");
		else
			System.out.println("Graph doesn't Contains cycle");
		DetectCycleUndirectedGraph g2 = new DetectCycleUndirectedGraph(3); 
        g2.addEdge(0, 1); 
        g2.addEdge(1, 2); 
        if (g2.isCycle()) 
            System.out.println("Graph Contains cycle"); 
        else
            System.out.println("Graph doesn't Contains cycle"); 
	}

}
