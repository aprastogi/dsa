package com.practice.graph;

public class IslandProblem {
    
    public static boolean isSafe(char grid[][],int row,int col,boolean [][] visited,int ROW,int COL){
        if(row >=0 && row < ROW && col >=0 && col < COL && grid[row][col] == '1'&& visited[row][col] == false)
            return true;
        return false;
    }

    public static void DFS(char grid[][],boolean [][] visited,int row,int col,int m,int n){
        int rowNbr[] = {-1,0,0,1};
        int colNbr[] = { 0,-1,1,0};
        visited[row][col] = true;
        for (int k = 0; k < rowNbr.length; k++){
            if (isSafe(grid, row + rowNbr[k], col + colNbr[k], visited,m,n) == true){
                DFS(grid, visited,row + rowNbr[k], col + colNbr[k],m,n);
            }
        }
    }
    
    public static int numIslands(char[][] grid) {
         int count=0;
         if(grid.length == 0)
             return 0;
         int m = grid.length;
         int n = grid[0].length;
         boolean visited[][] = new boolean [grid.length][grid[0].length];
         for(int i=0;i<m;i++){
            for(int j=0;j<n;j++){
                if(grid[i][j] == '1' && visited[i][j]== false){
                    DFS(grid,visited,i,j,m,n);
                    count++;
                }
            }
          }
         return count;
    }
    
    public static void main(String [] args) {
    	char grid[][] = {{'1','1','0','0','0'},
    			         {'1','1','0','0','0'},
    			         {'0','0','1','0','0'},
    			         {'0','0','0','1','1'}};
    	System.out.println(numIslands(grid));
    }
}
