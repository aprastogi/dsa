package com.practice.graph;

import java.util.ArrayList;
import java.util.Iterator;

public class DetectCycleDirectedGraph {

	int v;
	ArrayList<Integer> adj[];
	
	@SuppressWarnings("unchecked")
	public DetectCycleDirectedGraph(int v) {
		this.v = v;
		adj = new ArrayList[v];
		for(int i=0;i<v;i++)
			adj[i] = new ArrayList<Integer>();
	}
	
	public void addEdge(int v, int w) {
		adj[v].add(w);
	}
	
	public boolean isCycleUtil(int source, boolean [] visited) {
		visited[source] = true;
		Iterator<Integer> it = adj[source].listIterator(); 
        while (it.hasNext()) 
        {         	
            int element = it.next(); 
            if (!visited[element]) 
            {
                if(isCycleUtil(element, visited))
                	return true;
            }    
            else {
            	return true;
            }
        } 
        return false;
	}
	
	public boolean isCycle() {
		boolean[] visited = new boolean[v];
		for(int i=0;i<v;i++) {
			if(!visited[i] && isCycleUtil(i,visited))
				return true;
		}
		return false;
	}
	
	public static void main(String[] args) {
		DetectCycleUndirectedGraph g = new DetectCycleUndirectedGraph(5);
		g.addEdge(0, 1); 
	    g.addEdge(0, 2); 
	    g.addEdge(1, 2); 
	    g.addEdge(2, 0); 
	    g.addEdge(2, 3); 
	    g.addEdge(3, 3); 
		if(g.isCycle())
			System.out.println("Graph Contains Cycle");
		else
			System.out.println("Graph doesn't Contains cycle");
		DetectCycleUndirectedGraph g2 = new DetectCycleUndirectedGraph(5);
		g2.addEdge(0, 1); 
        g2.addEdge(1, 2); 
		if(g2.isCycle())
			System.out.println("Graph Contains Cycle");
		else
			System.out.println("Graph doesn't Contains cycle");
	}

}
