package com.practice;

import java.util.ArrayList;
import java.util.List;

class Tree{
    Tree left;
    Tree right;
    int value;

    Tree(int value){
        this.left = null;
        this.right = null;
        this.value = value;
    }
}

public class PrintPathOfTree {

    Tree root;

    private void traverse(Tree root, int sum , List<Integer> list){
        if(root == null)
            return;
        sum += root.value;
        if(root.left == null && root.right == null){
            list.add(sum);
            return;
        }
        traverse(root.left, sum, list);
        traverse(root.right, sum, list);
    }

    public static void main(String[] args) {
        PrintPathOfTree obj = new PrintPathOfTree();
        obj.root = new Tree(1);
        obj.root.left = new Tree(2);
        obj.root.right = new Tree(3);
        obj.root.left.left = new Tree(4);
        obj.root.left.right = new Tree(5);
        obj.root.right.left = new Tree(6);
        obj.root.right.right = new Tree(7);
        List<Integer> list = new ArrayList<>();
        obj.traverse(obj.root, 0, list);
        System.out.println(list);
    }
}
