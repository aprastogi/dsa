package com.practice;

import java.util.*;

class Program {

    private static final int [][] direction= {{0,1},{1,0},{-1,0},{0,-1}};

    private static int dfs(int [][] matrix, int row, int col, boolean [][] visited){
        visited[row][col] = true;
        for(int [] dir : direction){
            int nextR = dir[0] + row;
            int nextC = dir[1] + col;
            if(nextR >=0 && nextR < matrix.length && nextC >=0 && nextC < matrix[0].length && matrix[nextR][nextC] == 1 && !visited[nextR][nextC]){
                return 1 + dfs(matrix, nextR, nextC, visited);
            }
        }
        return 0;
    }

    public static List<Integer> riverSizes(int[][] matrix) {
        List<Integer> list = new ArrayList<>();
        boolean [][] visited = new boolean[matrix.length][matrix[0].length];
        for(int i=0;i<matrix.length;i++){
            for(int j=0;j<matrix[0].length;j++){
                if(!visited[i][j] && matrix[i][j] == 1){
                    int count = 1;
                    count += dfs(matrix, i, j, visited);
                    list.add(count);
                }
            }
        }
        return list;
    }

    public static void main(String[] args) {
        int [][] matrix = {
                {1, 0, 0, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 1, 0},
                {0, 0, 1, 0, 1, 0, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 0, 1, 0, 1, 0, 0},
                {0, 1, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 0, 0, 1}
        };
        System.out.println(riverSizes(matrix));
    }
}

