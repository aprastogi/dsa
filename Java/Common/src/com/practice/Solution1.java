package com.practice;

import java.util.ArrayList;
import java.util.List;

class Solution1 {

    private final List<List<Integer>> list = new ArrayList<>();

    public List<List<Integer>> permute(int[] nums) {
        List<Integer> input = new ArrayList<>();
        List<Integer> output = new ArrayList<>();
        for(int item : nums){
            input.add(item);
        }
        permute(input, output);
        return list;
    }

    private void permute(List<Integer> input, List<Integer> output){
        if(input.size() == 0){
            list.add(output);
            return;
        }

        System.out.println(input);
        System.out.println("size is : " + input.size());

        List<Integer> newInput;
        for(int i=0;i<input.size();i++){
            int current = input.get(i);
            output.add(current);
            newInput = new ArrayList<>(input.subList(0,i));
            for(int j = i+1;j< input.size();j++)
                newInput.add(input.get(j));

            permute(newInput, output);
        }
    }

    public static void main(String[] args) {
        Solution1 obj = new Solution1();
        System.out.println(obj.permute(new int[] {1,2,3}));
    }
}
