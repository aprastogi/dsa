package com.practice;

import java.util.ArrayList;
import java.util.List;

class Solution {

    private final List<List<Integer>> list = new ArrayList<>();

    public List<List<Integer>> subsets(int[] nums) {
        List<Integer> input = new ArrayList<>();
        List<Integer> output = new ArrayList<>();
        for(int item : nums)
            input.add(item);
        solve(input, output);
        return list;
    }

    private void solve(List<Integer> input, List<Integer> output){
        if(input.size() == 0){
            list.add(output);
            return;
        }
        List<Integer> out1 = new ArrayList<>(output);
        List<Integer> out2 = new ArrayList<>(output);
        out2.add(input.get(0));
        input.remove(0);
        solve(new ArrayList<>(input), out1);
        solve(new ArrayList<>(input), out2);

    }

    public static void main(String[] args) {
        Solution obj = new Solution();
        System.out.println(obj.subsets(new int[]{1, 2, 3}));
    }
}
