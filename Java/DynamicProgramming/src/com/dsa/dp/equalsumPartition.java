package com.dsa.dp;

import java.util.Arrays;

public class equalsumPartition {
    
    public static boolean equalSumPartitionTD(int arr[], boolean dp[][], int n) {

        Arrays.fill(dp[0], false);
        for(int i=0;i<dp.length;i++)
            dp[i][0] = true;
        for(int i=1;i<dp.length;i++) {
            for(int j=1;j<dp[0].length;j++) {
                if(arr[i-1] <= j)
                    dp[i][j] = dp[i-1][j-arr[i-1]] || dp[i-1][j];
                else
                    dp[i][j] = dp[i-1][j];
            }
        }
        for(int i=0;i<dp.length;i++){
            for(int j=0;j<dp[0].length;j++){
                System.out.print(dp[i][j] + " ");
            }
            System.out.println();
        }
        return dp[dp.length-1][dp[0].length-1];
    }
    
    public static int equalSumPartitionMemo(int arr[], int dp[][],int total, int n) {
        if(n == 0 && total == 0)
            return 1;
        if(n == 0)
            return 0;
        if(total == 0)
            return 1;
        if(dp[n][total] != -1)
            return dp[n][total];
        if(arr[n-1] <= total) 
            return dp[n][total] = equalSumPartitionMemo(arr, dp,total - arr[n-1], n-1) | equalSumPartitionMemo(arr, dp,total, n-1);
        return dp[n][total] = equalSumPartitionMemo(arr,dp, total, n-1);
    }    
    
    public static boolean equalSumPartition(int arr[], int total, int n) {
        if(n == 0 && total == 0)
            return true;
        if(n == 0)
            return false;
        if(total == 0)
            return true;
        if(arr[n-1] <= total) 
            return equalSumPartition(arr, total - arr[n-1], n-1) || equalSumPartition(arr, total, n-1);
        return equalSumPartition(arr, total, n-1);
    }
    
    public static void main(String[] args) {
       int arr[] = {5,1,5,11};
       int sum = 0;
       for(int i=0;i<arr.length;i++)
           sum += arr[i];
       if(sum %2 == 1)
          System.out.println(false);
       else {
           int dp [] [] = new int [arr.length+1][sum/2+1];
           for(int i=0;i<dp.length;i++) {
               for(int j=0;j<dp[0].length;j++) {
                   dp[i][j] = -1;
               }
           }        
          boolean[][] dpTD = new boolean [arr.length+1][sum/2+1];           
          System.out.println(equalSumPartition(arr, sum /2, arr.length));
          System.out.println(equalSumPartitionMemo(arr,dp,sum/2,arr.length));
          System.out.println(equalSumPartitionTD(arr,dpTD,arr.length));
       }
    }

}
