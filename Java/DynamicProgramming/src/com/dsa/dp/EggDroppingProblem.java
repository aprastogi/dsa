package com.dsa.dp;

public class EggDroppingProblem {
    
    public static int minAttemptMemo(int floor,int egg, int [][] dp) {
        if(floor == 0 || floor == 1 || egg == 1)
            return floor;
        int min = Integer.MAX_VALUE;
        if(dp[floor][egg] != -1)
            return dp[floor][egg];
        for(int k=1;k<=floor;k++) {
            int temp = 1 + Math.max(minAttemptMemo(k-1, egg-1, dp), minAttemptMemo(floor - k, egg, dp));
            min = Math.min(min, temp);
        }
        return dp[floor][egg] = min;
    }
    
    public static int minAttemptMemoOptimized(int floor,int egg, int [][] dp) {
        if(floor == 0 || floor == 1 || egg == 1)
            return floor;
        int min = Integer.MAX_VALUE;
        if(dp[floor][egg] != -1)
            return dp[floor][egg];
        for(int k=1;k<=floor;k++) {
            int left = 0, right = 0;
            if(dp[k-1][egg-1] != -1) {
                left = dp[k-1][egg-1];
            }
            else {
                left = minAttemptMemoOptimized(k-1, egg-1, dp);
                dp[k-1][egg-1] = left;
            }
            if(dp[floor-k][egg] != -1) {
                right = dp[floor-k][egg];
            }
            else {
                right = minAttemptMemoOptimized(floor - k, egg, dp);
                dp[floor-k][egg] = right;
            }
            int temp = 1 + Math.max(left,right);
            min = Math.min(min, temp);
        }
        return dp[floor][egg] = min;
    }
    
    public static int minAttempt(int floor,int egg) {
        if(floor == 0 || floor == 1|| egg == 1)
            return floor;
        int min = Integer.MAX_VALUE;
        for(int k=1;k<=floor;k++) {
            int temp = 1 + Math.max(minAttempt(k-1, egg-1), minAttempt(floor - k, egg));
            min = Math.min(min, temp);
        }        
        return min;
    }
    
    public static void main(String[] args) {
       int floor = 5,egg = 3;
       int dp [][] = new int[1000][1000];
       for(int i=0;i<dp.length;i++) {
           for(int j=0;j<dp[0].length;j++)
               dp[i][j] = -1;
       }
       System.out.println(minAttempt(floor,egg));
       System.out.println(minAttemptMemo(floor,egg,dp));
       System.out.println(minAttemptMemoOptimized(floor,egg,dp));
    }

}
