package com.dsa.dp;

import java.util.Arrays;

public class CoinChangeWays {
    
    public static int coinChangeTD(int [] coins,int total) {
        int[][] dp = new int[coins.length+1][total+1];
        Arrays.fill(dp[0], 0);
        for(int i=0;i<dp.length;i++)
            dp[i][0] = 1;
        for(int i=1;i<dp.length;i++) {
            for(int j=1;j<dp[0].length;j++) {
                if(coins[i-1] <= j)
                    dp[i][j] = dp[i][j-coins[i-1]] + dp[i-1][j];
                else
                    dp[i][j] = dp[i-1][j];
            }
        }
        return dp[dp.length-1][dp[0].length-1];
    }
    
    public static int coinChangeMemo(int [] coins, int total, int n, int[][] dp) {
        if( n == 0)
            return 0;
        if(total == 0)
            return 1;
        if(dp[n][total] != -1)
            return dp[n][total];
        if(coins[n-1] <= total)
            return dp[n][total] = coinChangeMemo(coins, total - coins[n-1], n, dp) + coinChangeMemo(coins, total, n-1, dp);
        return dp[n][total] = coinChangeMemo(coins, total, n-1, dp);
    }
    
    public static int coinChange(int [] coins,int total, int n, String result) {
        if(n == 0)
            return 0;
        if(total == 0) {
            System.out.println(result);
            return 1;
        }
        if(coins[n-1] <= total) 
            return coinChange(coins, total - coins[n-1], n, result + " " + coins[n-1]) + coinChange(coins,total,n-1, result);
        return coinChange(coins,total,n-1, result);
    }    
    
    public static void main(String[] args) {
        int[] coins = {1,2,3};
        int total = 4;
        int[][] dp = new int [coins.length+1][total+1];
        for(int i=0;i<dp.length;i++) {
            for(int j=0;j<dp[0].length;j++) {
                dp[i][j] = -1;
            }
        }
       System.out.println(coinChange(coins, total, coins.length, ""));
       // System.out.println(coinChangeTD(coins,total));

    }

}
