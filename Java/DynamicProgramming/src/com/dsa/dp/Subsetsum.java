package com.dsa.dp;

public class Subsetsum {
    
    public static boolean subSetSumTD(int [] arr, boolean dp[][], int total) {
        for(int i=0;i<dp.length;i++) {
            for(int j=0;j<dp[0].length;j++) {
                if(i == 0)
                    dp[i][j] = false;
                if(j == 0)
                    dp[i][j] = true;
            }
        }
        for(int i=1;i<dp.length;i++) {
            for(int j=1;j<dp[0].length;j++) {
                if(arr[i-1] <= j) {
                    dp[i][j] = dp[i-1][j-arr[i-1]] || dp[i-1][j]; 
                }
                else
                    dp[i][j] = dp[i-1][j];
            }
        }
       return dp[dp.length-1][dp[0].length-1];
    }
    
    public static int subSetSumMemo(int [] arr, int [][] dp , int sum, int n) {
        if(n == 0 && sum == 0)
            return 1;        
        if(sum == 0)
            return 1;
        if(n== 0)
            return 0;
        if(dp[n][sum] != -1)
            return dp[n][sum];
        if(arr[n-1] <= sum)
            return dp[n][sum] = subSetSumMemo(arr, dp, sum-arr[n-1], n-1) | subSetSumMemo(arr, dp, sum, n-1);
        return dp[n][sum] = subSetSumMemo(arr, dp, sum, n-1);
    }
    
    public static boolean subSetSum(int [] arr, int sum , int n) {
        if(sum == 0)
            return true;     
        if(n== 0)
            return false;        
        if(arr[n-1] <= sum)
            return subSetSum(arr,sum-arr[n-1],n-1) || subSetSum(arr,sum,n-1);
        return subSetSum(arr, sum, n-1);
    }
    
    public static void main(String[] args) {
        int arr[] = {2,3,5,8,10};
        int total = 18;
        int dp [] [] = new int [arr.length+1][total+1];
        for(int i=0;i<dp.length;i++) {
            for(int j=0;j<dp[0].length;j++) {
                dp[i][j] = -1;
            }
        }        
        boolean dpTD [] [] = new boolean [arr.length+1][total+1];        
        System.out.println(subSetSum(arr,total,arr.length));
        System.out.println(subSetSumMemo(arr,dp,total,arr.length));
        System.out.println(subSetSumTD(arr,dpTD,total));
    }
}
