package com.dsa.dp;

public class DeleteDistance {
   
    public static int longestCommonSubsequence(String text1,String text2,int m, int n) {
        if(m == 0 || n == 0)
            return 0;
        if(text1.charAt(m-1) == text2.charAt(n-1))
            return 1 + longestCommonSubsequence(text1, text2, m-1, n-1);
        return Math.max(longestCommonSubsequence(text1, text2, m-1, n), longestCommonSubsequence(text1, text2, m, n-1));
    } 
    
    public static int minDelete(String text1,String text2,int m, int n) {
        if(m == 0)
            return n;
        if(n == 0)
            return m;
        if(text1.charAt(m-1) == text2.charAt(n-1))
            return minDelete(text1, text2, m-1, n-1);
        return 1 + Math.min(minDelete(text1, text2, m-1, n),minDelete(text1, text2, m, n-1));
    }
    
    public static int minDeleteMemo(String text1,String text2,int m, int n, int [][]dp) {
        if(m == 0)
            return n;
        if(n == 0)
            return m;
        if(dp[m][n] != -1)
            return dp[m][n];
        if(text1.charAt(m-1) == text2.charAt(n-1))
             return dp[m][n] = minDeleteMemo(text1, text2, m-1, n-1, dp);
        return dp[m][n] = 1 + Math.min(minDeleteMemo(text1, text2, m-1, n, dp),minDeleteMemo(text1, text2, m, n-1, dp));
        
    }
    public static void main(String[] args) {
        String text1 = "dog";
        String text2 = "frog";
        int dp [][] = new int[text1.length()+1][text2.length()+1];
        for(int i=0;i<dp.length;i++) {
            for(int j=0;j<dp[0].length;j++) {
                dp[i][j] = -1;
            }
        }
        System.out.println(minDelete(text1,text2,text1.length(),text2.length()));
        System.out.println(minDeleteMemo(text1, text2,text1.length(),text2.length(),dp));
        System.out.println(text1.length() + text2.length() - 2 * longestCommonSubsequence(text1, text2, text1.length(), text2.length()));
    }

}
