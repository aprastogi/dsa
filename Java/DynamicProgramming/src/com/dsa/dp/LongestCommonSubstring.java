package com.dsa.dp;

public class LongestCommonSubstring {
    
    public static int longestCommonSubString(String text1, String text2, int m , int n,int count) {
        if (m == 0 || n == 0) 
            return count;               
        if (text1.charAt(m-1) == text2.charAt(n-1)) { 
            return longestCommonSubString(text1,text2,m-1,n-1,count + 1);
        } 
        return Math.max(count, Math.max(longestCommonSubString(text1, text2, m-1, n, 0) ,longestCommonSubString(text1, text2, m, n-1, 0)));
    }
   
    public static int longestCommonSubStringTD(String text1,String text2) {
        int dp [][] = new int[text1.length()+1][text2.length()+1];
        int ans = 0;
        for(int i=0;i<dp.length;i++) {
            for(int j=0;j<dp[0].length;j++) {
                if(i == 0 || j == 0)
                    dp[i][j] = 0;
                else if(text1.charAt(i-1) == text2.charAt(j-1))
                    dp[i][j] = 1 + dp[i-1][j-1];
                else 
                    dp[i][j] = 0;
            }
        }
        for(int i=0;i<dp.length;i++) {
            for(int j=0;j<dp[0].length;j++) {
                ans = Math.max(ans, dp[i][j]);
            }
        }
        return ans;
        
    }
    
    public static void main(String[] args) {
        String text1 = "abcdefhhhhhll";
        String text2 = "abgdefghhhhll";
        System.out.println(longestCommonSubStringTD(text1, text2));
        System.out.println(longestCommonSubString(text1, text2,text1.length(),text2.length(),0));
    }

}
