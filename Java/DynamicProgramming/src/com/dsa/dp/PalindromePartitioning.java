package com.dsa.dp;

public class PalindromePartitioning {

    public static boolean isPalindrome(String str, int i, int j) {
        while(i<=j) {
            if(str.charAt(i) != str.charAt(j))
                return false;
            i++;
            j--;
        }
        return true;
    }
    
    public static int minPartionMemo(String str,int i, int j, int [][] dp) {
        if(i >= j)
            return 0;
        if(isPalindrome(str, i, j))
            return 0;
        if(dp[i][j] != -1)
            return dp[i][j];
        int min = Integer.MAX_VALUE;
        for(int k=i;k<=j-1;k++) {
            int temp = 1 + minPartionMemo(str, i, k,dp) + minPartionMemo(str, k+1,j,dp);
            min = Math.min(min, temp);
        }
        return dp[i][j] = min;
    }
    
    public static int minPartionMemoOptimized(String str,int i, int j, int [][] dp) {
        if(i >= j)
            return 0;
        if(isPalindrome(str, i, j))
            return 0;
        if(dp[i][j] != -1)
            return dp[i][j];
        int min = Integer.MAX_VALUE;
        for(int k=i;k<=j-1;k++) {
            int left = 0, right = 0;
            if(dp[i][k] != -1) {
                 left = dp[i][k];
            }
            else {
                left = minPartionMemo(str, i, k,dp);
                dp[i][k] = left;
            }
            if(dp[k+1][j] != -1) {
                right = dp[k+1][j];               
            }
            else {
                right = minPartionMemo(str, k+1,j,dp);
                dp[k+1][j] = right;
            }
            int temp = 1 + left + right ;
            min = Math.min(min, temp);
        }
        return dp[i][j] = min;
    }
    
    public static int minPartition(String str,int i, int j) {
        if(i >= j)
            return 0;
        if(isPalindrome(str,i,j))
            return 0;
        int min = Integer.MAX_VALUE;
        for(int k=i;k<=j-1;k++) {
            int temp = 1 + minPartition(str, i, k) + minPartition(str, k+1, j);
            min = Math.min(min, temp);
        }
        return min;
    }
    
    public static void main(String[] args) {
        String str = "ababbbabbababa";
        int dp[][] = new int [1000][1000];
        for(int i=0;i<dp.length;i++) {
            for(int j=0;j<dp[0].length;j++) {
                dp[i][j] = -1;
            }
        }
        System.out.println(minPartition(str,0,str.length()-1));
        System.out.println(minPartionMemo(str, 0, str.length()-1,dp));
        System.out.println(minPartionMemoOptimized(str, 0, str.length()-1,dp));
    }

}
