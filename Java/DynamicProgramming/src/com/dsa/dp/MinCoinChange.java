package com.dsa.dp;

import java.util.Arrays;

public class MinCoinChange {

    public static int minCoinChange(int [] coins,int total) {
        int[][] dp = new int[coins.length+1][total+1];
        Arrays.fill(dp[0], Integer.MAX_VALUE - 1);
        for(int i=1;i<dp.length;i++)
            dp[0][i] = 1;
        for(int i=1;i<dp[0].length;i++) {
            if(i % coins[0] == 0)
                dp[1][i] = i/coins[0];
            else
                dp[1][i] = Integer.MAX_VALUE-1;
        }
        for(int i=2;i<dp.length;i++) {
            for(int j=1;j<dp[0].length;j++) {
                if(coins[i-1] <= j)
                    dp[i][j] = Math.min(1 + dp[i][j- coins[i-1]], dp[i-1][j]);
                else
                    dp[i][j] = dp[i-1][j];
            }
        }
        return dp[dp.length-1][dp[0].length-1];
    }
    public static void main(String[] args) {
        int [] coins = {9,6,5,1};
        int total = 11;
        System.out.println(minCoinChange(coins,total));

    }

}
