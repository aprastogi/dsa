package com.dsa.dp;

public class MinDeletionForPalindrome {
    
    public static int longestCommonSubsequenceTD(String text1, String text2) {
        int dp[][] = new int[text1.length()+1][text2.length()+1];
        for(int i=0;i<dp.length;i++) {
            for(int j=0;j<dp[0].length;j++) {
                if(i == 0 || j == 0)
                    dp[i][j] = 0;
                else if(text1.charAt(i-1) == text2.charAt(j-1))
                    dp[i][j] = 1 + dp[i-1][j-1];
                else
                    dp[i][j] = Math.max(dp[i-1][j], dp[i][j-1]);
            }
        }
        return dp[dp.length-1][dp[0].length-1];
    }
    
    public static int longestCommonSubsequenceMemo(String text1, String text2, int dp[][], int m, int n) {
        if(m == 0 || n == 0)
            return 0;
        if(dp[m][n] != -1)
            return dp[m][n];
        if(text1.charAt(m-1) == text2.charAt(n-1))
            return dp[m][n] = 1 + longestCommonSubsequenceMemo(text1, text2,dp, m-1, n-1);
        return dp[m][n] = Math.max(longestCommonSubsequenceMemo(text1, text2,dp, m-1, n), longestCommonSubsequenceMemo(text1, text2,dp, m, n-1));
    }
    
    public static int longestCommonSubsequence(String text1,String text2,int m, int n) {
        if(m == 0 || n == 0)
            return 0;
        if(text1.charAt(m-1) == text2.charAt(n-1))
            return 1 + longestCommonSubsequence(text1, text2, m-1, n-1);
        return Math.max(longestCommonSubsequence(text1, text2, m-1, n), longestCommonSubsequence(text1, text2, m, n-1));
    } 
    
    public static void main(String[] args) {
        String str = "agbcba";
        String text1 = str;
        StringBuilder sb = new StringBuilder(str);
        String text2 = sb.reverse().toString();
        int dp[][] = new int[text1.length()+1][text2.length()+1];
        for(int i=0;i<dp.length;i++) {
            for(int j=0;j<dp[0].length;j++) {
                dp[i][j] = -1;
            }
        }
        System.out.println(text1.length() - longestCommonSubsequence(text1, text2, text1.length(), text2.length()));
        System.out.println(text1.length() - longestCommonSubsequenceMemo(text1, text2, dp,text1.length(), text2.length()));
        System.out.println(text1.length() - longestCommonSubsequenceTD(text1, text2));
    }

}
