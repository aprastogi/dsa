package com.dsa.dp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SubsetsumPrint {
    
	public static void subsetsumRec(int[] nums, int sum, int n, List<List<Integer>> result, List<Integer> current)
	{
		if(sum == 0) {
			result.add(current);
			return;
		}		
		if(n == 0)
			return;		
		if(nums[n-1] <= sum) {
			List<Integer> newList = new ArrayList<>(current);
			newList.add(nums[n-1]);
			subsetsumRec(nums, sum - nums[n-1], n-1, result, newList);
			subsetsumRec(nums, sum, n-1, result, current);
		}else {
			subsetsumRec(nums, sum, n-1, result, current);
		}
	}
    
    public static void main(String[] args) {
    	int[] nums = new int[] {3, 34, 4, 12, 5, 2};
		int sum = 9;		
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		subsetsumRec(nums, sum, nums.length, result, new ArrayList<Integer>());
		for(List<Integer> list : result)
			System.out.println(Arrays.toString(list.toArray()));
    }
}
