package com.dsa.dp;

public class KnapSack {
    
    public static int knapsackTD(int [] val,int [] wt,int weight,int [][] dp) {
        for(int i=0;i<dp.length;i++) {
            for(int j=0;j<dp[0].length;j++) {
                if(i == 0 || j == 0)
                    dp[i][j] = 0;
            }
        }
        for(int i=1;i<dp.length;i++) {
            for(int j=1;j<dp[0].length;j++) {
                if(wt[i-1] <= j) {
                    dp[i][j] = Math.max(val[i-1] + dp[i-1][j-wt[i-1]], dp[i-1][j]);
                }
                else {
                    dp[i][j] = dp[i-1][j];
                }
            }
        }
        return dp[dp.length-1][dp[0].length-1];
    }
    
    public static int knapsackMemo(int [][] dp, int wt[], int val[], int w, int n) {
        if(n == 0 || w == 0)
            return 0;
        if(dp[n][w] != -1)
            return dp[n][w];
        if(wt[n-1] <= w)
            return dp[n][w] = Math.max(val[n-1] + knapsackMemo(dp, wt, val, w - wt[n-1], n-1), knapsackMemo(dp, wt, val, w, n-1));
        return dp[n][w] =knapsackMemo(dp, wt, val, w, n-1);
    }
    
    public static int knapsack(int [] wt,int [] val, int w,int n) {
        if(n == 0 || w == 0)
            return 0;
        if(wt[n-1] <= w)
            return Math.max(val[n-1] + knapsack(wt, val, w - wt[n-1], n-1), knapsack(wt, val, w, n-1));
        return knapsack(wt, val, w, n-1);
    }
    
    public static void main(String[] args) {
        int wt [] = {4,4,6,8};
        int val [] = {2,4,4,5};
        int weight = 15;
        int dp [][] = new int [wt.length+1][weight+1];
        for(int i=0;i<dp.length;i++) {
            for(int j=0;j<dp[0].length;j++) {
                dp[i][j] = -1;
            }
        }        
        System.out.println(knapsack(wt,val,weight,wt.length));
        System.out.println(knapsackMemo(dp,wt,val,weight,wt.length));
        System.out.println(knapsackTD(val,wt,weight,dp));
    }

}
