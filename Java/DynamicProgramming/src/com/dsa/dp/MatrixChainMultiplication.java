package com.dsa.dp;

public class MatrixChainMultiplication {
    
    static int minCostMemoOptimized(int [] arr, int i, int j,int [][] dp) {
        if(i>=j)
            return 0;
        int min = Integer.MAX_VALUE;
        if(dp[i][j] != -1)
            return dp[i][j];
        for(int k=i;k<=j-1;k++) {
            int left = 0, right = 0;
            if(dp[i][k] != -1) {
                left = dp[i][k];
            }
            else {
                left = minCostMemoOptimized(arr, i, k,dp);
            }
            if(dp[k+1][j] != -1) {
                right = dp[k+1][j];
            }
            else {
                right = minCostMemoOptimized(arr, k+1, j,dp);
            }
            int currCost = left + right + arr[i-1]* arr[k] * arr[j];
            min = Math.min(min, currCost);
         }
         return dp[i][j] = min;
    }
    
    static int minCostMemo(int [] arr, int i, int j,int [][] dp) {
        if(i>=j)
            return 0;
        int min = Integer.MAX_VALUE;
        if(dp[i][j] != -1)
            return dp[i][j];
        for(int k=i;k<=j-1;k++) {
            int currCost = minCost(arr, i, k) + minCost(arr, k+1, j) + arr[i-1]* arr[k] * arr[j];
            min = Math.min(min, currCost);
         }
         return dp[i][j] = min;
    }
    
    static int minCost(int [] arr, int i, int j) {
        if(i>=j)
            return 0;
        int min = Integer.MAX_VALUE;
        for(int k=i;k<=j-1;k++) {
           int currCost = minCost(arr, i, k) + minCost(arr, k+1, j) + arr[i-1]* arr[k] * arr[j];
           min = Math.min(min, currCost);
        }
        return min;
    }
    
    public static void main(String[] args) {
        int arr [] = {1, 2, 3, 4, 3};
        int dp[][] = new int [1000][1000];
        for(int i=0;i<dp.length;i++) {
            for(int j=0;j<dp[0].length;j++) {
                dp[i][j] = -1;
            }
        }
        System.out.println(minCost(arr,1,arr.length-1));
        System.out.println(minCostMemo(arr, 1, arr.length-1, dp));
        System.out.println(minCostMemoOptimized(arr, 1, arr.length-1, dp));
    }
}
