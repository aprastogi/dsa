package com.dsa.dp;

public class RodCutting {
    
    public static int maxProfitRodTD(int [] len, int [] price, int total) {
        int [][] dp = new int [len.length+1][total+1];
        for(int i=0;i<dp.length;i++) {
            for(int j=0;j<dp[0].length;j++) {
                if(i == 0 || j == 0)
                    dp[i][j] = 0;
                else if(len[i-1] <= j)
                    dp[i][j] = Math.max(price[i-1] + dp[i][j- len[i-1]], dp[i-1][j]);
                else
                    dp[i][j] = dp[i-1][j];
            }
        }
        return dp[dp.length-1][dp[0].length-1];
    }
    
    public static int maxProfitRodMemo(int [] len, int [] price,int n , int total , int[][] dp) {
        if(n == 0 || total == 0)
            return 0;
        if(dp[n][total] != -1)
            return dp[n][total];
        if(len[n-1] <= total)
            return dp[n][total] = Math.max(price[n-1] + maxProfitRodMemo(len, price, n, total- len[n-1],dp), maxProfitRodMemo(len, price, n-1, total, dp));
        return dp[n][total] = maxProfitRodMemo(len, price, n-1, total, dp);
    }
    
    public static int maxProfitRod(int [] len,int [] price, int n, int total) {
         if(n == 0 || total == 0)
             return 0;
         if(len[n-1] <= total)
             return Math.max(price[n-1] + maxProfitRod(len, price, n, total - len[n-1]), maxProfitRod(len, price, n-1, total));
         return maxProfitRod(len, price, n-1, total);
    }
    
    public static void main(String[] args) {
       int [] len = {1,2,3,4,5,6,7,8};
       int [] price = {1,5,8,9,10,17,17,20};
       int N = 8;
       int [][] dp = new int[len.length+1][N+1];
       for(int i=0;i<dp.length;i++) {
           for(int j=0;j<dp[0].length;j++) {
               dp[i][j] = -1;
           }
       }
       System.out.println(maxProfitRod(len,price,len.length,N));
       System.out.println(maxProfitRodMemo(len, price, len.length, N, dp));
       System.out.println(maxProfitRodTD(len, price, N));
    }

}


