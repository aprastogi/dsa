package com.basics.java;

public class Generics<T,U> {

	public void add(T o1, T o2, U o3) {
		System.out.println((Integer)o1 + (Integer)o2);
		System.out.println(o3);
	}
	
	public static void main(String[] args) {
		Generics<Integer,String> obj = new Generics<Integer,String>();
		obj.add(12,13,"apoorv");
	}
}
