package com.practice.lambda;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class PersonUtil {

	public static void printAll(List<Person> people) {
		for(Person p : people)
			System.out.println(p);
	}
	
	public static void printCondional(List<Person> people, Condition condition) {
		for(Person p : people) {
			if(condition.test(p))
				System.out.println(p);
		}
	}
	
	public static void main(String[] args) {
		List<Person> people = Arrays.asList(
				new Person("Charles", "Dickens", 34),
				new Person("Lewis", "Caroll", 34),
				new Person("Thomas", "Carlyle", 34),
				new Person("Charlatte", "Brante", 34),
				new Person("Mahhew", "Arnold", 34)
		);
		
		//sort the people by last name
		Collections.sort(people, new Comparator<Person>() {

			@Override
			public int compare(Person o1, Person o2) {
				return o1.getLastName().compareTo(o2.getLastName());
			}
		});
		
		printAll(people);
		//print all element of person
		
		Condition condition = new Condition() {
			
			@Override
			public boolean test(Person p) {
				return p.getLastName().startsWith("C");
			}
		};
		
		printCondional(people,condition);
		//print all people that have last name beginning with C

	}

}
