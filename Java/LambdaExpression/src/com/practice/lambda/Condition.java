package com.practice.lambda;

public interface Condition {
     public boolean test(Person p);
}
