package com.practice.lambda;

public class MethodReference {

	public static void main(String[] args) {
		Thread t1 = new Thread(MethodReference::printMessage);
		// MethodReference::printMessage  == () -> printMessage()
		t1.start();
	}
	
	public static void printMessage() {
		System.out.println("Hello world");
	}

}
