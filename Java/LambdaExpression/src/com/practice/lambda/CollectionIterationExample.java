package com.practice.lambda;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

public class CollectionIterationExample {

	public static void main(String[] args) {
		List<Person> people = Arrays.asList(
				new Person("Charles", "Dickens", 34),
				new Person("Lewis", "Caroll", 34),
				new Person("Thomas", "Carlyle", 34),
				new Person("Charlatte", "Brante", 34),
				new Person("Mahhew", "Arnold", 34)
		);
		
		Stream<Person> s = people.stream();
		List<String> list = Arrays.asList(new String[] {"hello","world","hello"});
		list.stream().anyMatch(str -> str.contains("ll"));

		list.stream().forEach(p->System.out.println(p));

		Map<String,Integer> map = new HashMap();

		list.stream().forEach(str -> map.put(str, map.getOrDefault(str,0)+1));

		System.out.println(map.entrySet());


	}

}
