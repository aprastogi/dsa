package com.practice.lambda;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class PersonUtilLambda {

	public static void printCondional(List<Person> people, Predicate<Person> condition, Consumer<Person> consumer) {
		for(Person p : people) {
			if(condition.test(p))
				consumer.accept(p);
		}
	}
	
	public static void main(String[] args) {
		List<Person> people = Arrays.asList(
				new Person("Charles", "Dickens", 34),
				new Person("Lewis", "Caroll", 34),
				new Person("Thomas", "Carlyle", 34),
				new Person("Charlatte", "Brante", 34),
				new Person("Mahhew", "Arnold", 34)

		//sort the people by last name		
		Collections.sort(people, (p1,p2) -> p1.getLastName().compareTo(p2.getLastName()));
		
		//print all element of person	
		printCondional(people, (p) -> true, (p) -> System.out.println(p));	 // system.out::println (Method Reference)	
		
		//print all people that have last name beginning with C
		printCondional(people, (p) -> p.getLastName().startsWith("C"), (p) -> System.out.println(p));

	}

}
