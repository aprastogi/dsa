package com.practice.lambda;

public class ThisLambdaReference {

	public void doProcess(int i, Processs  p) {
		p.process(i);
	}
	
	public void execute() {
        doProcess(10, (i) -> {
			System.out.println("Value of i is : " + i);
			System.out.println(this);
		});
	}
	
	public String toString() {
		return "This is main annonymous inner class";
	}
	
	public static void main(String[] args) {
		ThisLambdaReference obj = new ThisLambdaReference();
		obj.doProcess(10, new Processs() {			
			@Override
			public void process(int i) {
				System.out.println("value of i is : " + i);
				System.out.println(this);				
			}
			
			public String toString() {
				return "This is annonymous inner class";
			}
		});
		obj.execute();		
	}
}

interface Processs {
	void process(int i);
}
