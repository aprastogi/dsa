package com.practice.lambda;

public class LambdaBasic {

	public static void main(String[] args) {
		Greeting g = () -> System.out.println("Hello world");		
		g.perform();
	}
}
