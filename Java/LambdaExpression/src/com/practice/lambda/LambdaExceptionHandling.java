package com.practice.lambda;

import java.util.function.BiConsumer;

public class LambdaExceptionHandling {

	public static void main(String[] args) {
		int [] num = {1,2,3,4};
		int key = 2;
		process(num,key, wrapperLambda((v, k) -> System.out.println(v/k)));

	}
	
	public static void process(int [] num, int key, BiConsumer<Integer,Integer> consumer) {
		 for(int item : num)
			 consumer.accept(item, key);
	}
	
	public static BiConsumer<Integer, Integer> wrapperLambda(BiConsumer<Integer, Integer> consumer){
		return (v,k) -> {
			try {
			consumer.accept(v, k);
			}
			catch(ArithmeticException e) {
				System.out.println("Arithmetic Exception");
			}
		};
	}
}