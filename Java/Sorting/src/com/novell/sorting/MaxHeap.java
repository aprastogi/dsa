package com.novell.sorting;

public class MaxHeap {
    
	public static void swap(int arr[],int i, int j) {
		int temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}
	
	public static void maxHeapify(int arr[] , int i, int n) {
		int left = 2 * i + 1;
		int right = 2 * i + 2;
		int largest = i;
		if(left < n && arr[left] > arr[largest])
			largest = left;
		if(right < n && arr[right] > arr[largest])
			largest = right;
		if(largest != i) {
			swap(arr,i ,largest);
		    maxHeapify(arr, largest, n);
		}
	}
	
	public static void buildMaxHeap(int [] arr,int n) {
		for(int i = n/2 -1 ; i>=0; i--) {
			maxHeapify(arr,i,n);
		}
	}
	
	public static int DeleteMax(int arr[],int n,int i) {
		if(i > n)
			return -1;
		int max = arr[0];
		arr[0] = arr[n-1];
		n = n-1;
		maxHeapify(arr, 0, n);
		return max;
	}
	
	public static void increaseKey(int arr[],int i,int key) {
		if(arr[i] > key)
			return;
		arr[i] = key;
		while(i > 0 &&  arr[(i -1)/2] < arr[i]) {
			swap(arr,i, (i-1)/2);
			i = (i -1 )/2;
		}
	}
	public static void main(String[] args) {
		int [] arr = {1,3,6,8,10,15,18};
		int n = arr.length;
		buildMaxHeap(arr, n);
		for(int i=0;i<n;i++)
			System.out.print(arr[i] + " ");
		System.out.println();
		System.out.println("Max Element in Heap is : " + arr[0]);
		/*DeleteMax(arr,n,0);
		System.out.println("After Deleting Max from Heap .. ");
		for(int i=0;i<n-1;i++)
			System.out.print(arr[i] + " ");
		System.out.println();
		increaseKey(arr,4,30);
		for(int i=0;i<n-1;i++)
			System.out.print(arr[i] + " ");*/
		System.out.println();
		System.out.println("After applying heap sort");
		for(int i= n-1;i>=0;i--) {
			swap(arr,0,i); 
			maxHeapify(arr, 0, i);
		}
		for(int i=0;i<n;i++)
			System.out.print(arr[i] + " ");
	}

}
