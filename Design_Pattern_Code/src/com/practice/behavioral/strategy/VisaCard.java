package com.practice.behavioral.strategy;

public class VisaCard extends Strategy {

    @Override
    public boolean isValid(CreditCard creditCard) {
        return creditCard.getCardNumber().startsWith("24") && !creditCard.getCvv().isEmpty();
    }
}
