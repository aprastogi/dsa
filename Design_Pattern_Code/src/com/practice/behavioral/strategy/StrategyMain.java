package com.practice.behavioral.strategy;

public class StrategyMain {

    public static void main(String [] args) {
        CreditCard amexCard = new CreditCard(new AmexCard());
        amexCard.setCardNumber("3452");
        amexCard.setCvv("321");
        System.out.println(amexCard.isValid());

        CreditCard visaCard = new CreditCard(new VisaCard());
        visaCard.setCardNumber("1438");
        visaCard.setCvv("123");
        System.out.println(visaCard.isValid());
    }
}
