package com.practice.behavioral.strategy;

public class AmexCard extends Strategy{

    @Override
    public boolean isValid(CreditCard creditCard) {
        return creditCard.getCardNumber().startsWith("34") && !creditCard.getCvv().isEmpty();
    }
}
