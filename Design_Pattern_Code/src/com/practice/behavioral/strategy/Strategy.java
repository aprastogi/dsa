package com.practice.behavioral.strategy;

public abstract class Strategy {

    public abstract boolean isValid(CreditCard creditCard);
}
