package com.practice.behavioral.strategy;

public class CreditCard {

    private String cardNumber;
    private String cvv;

    private Strategy validationStrategy;

    public CreditCard(Strategy validationStrategy){
        this.validationStrategy = validationStrategy;
    }

    public boolean isValid(){
        return validationStrategy.isValid(this);
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getCvv() {
        return cvv;
    }
}
