package com.practice.behavioral.chainofresponsibility;

public enum RequestType {
    CONFERENCE,PURCHASE;
}
