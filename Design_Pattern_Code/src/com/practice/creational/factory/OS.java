package com.practice.creational.factory;

public abstract class OS {
    public abstract void spec();
}
