package com.practice.creational.factory;

public class FactoryMain {

    public static void main(String [] args){
        OS currentOS = new OSFactory().getOS(OSType.ANDROID);
        currentOS.spec();
        currentOS = new OSFactory().getOS(OSType.IOS);
        currentOS.spec();
    }
}
