package com.practice.creational.factory;

public class OSFactory {

    public OS getOS(OSType type){
        switch (type){
            case ANDROID:
                return new Android();
            case IOS:
                return new IOS();
            default:
                return null;
        }
    }
}
