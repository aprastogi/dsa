package com.practice.creational.abstractfactory;

public class OSFactory {
      public OS getinstance(AbstractFactory af) {
    	  return af.getinstance();
      }
}
