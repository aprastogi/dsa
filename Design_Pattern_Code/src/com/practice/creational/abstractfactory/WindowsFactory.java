package com.practice.creational.abstractfactory;

public class WindowsFactory implements AbstractFactory{

	@Override
	public OS getinstance() {
		return new Windows();
	}

}
