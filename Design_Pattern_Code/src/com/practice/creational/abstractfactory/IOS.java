package com.practice.creational.abstractfactory;

public class IOS implements OS {

	@Override
	public void spec() {
		System.out.println("Welcome to IOS");
	}

}
