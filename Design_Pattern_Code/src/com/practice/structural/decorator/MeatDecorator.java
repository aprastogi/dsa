package com.practice.structural.decorator;

public class MeatDecorator extends SandwichDecorator{

    private Sandwich customSandwich;

    public MeatDecorator(Sandwich customSandwich) {
        super(customSandwich);
        this.customSandwich = customSandwich;
    }

    public String make(){
        return customSandwich.make() + " Turkey";

    }
}
