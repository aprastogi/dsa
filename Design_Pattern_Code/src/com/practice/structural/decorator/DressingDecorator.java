package com.practice.structural.decorator;

public class DressingDecorator extends SandwichDecorator{

    private Sandwich customSandwich;

    public DressingDecorator(Sandwich customSandwich) {
        super(customSandwich);
        this.customSandwich = customSandwich;
    }

    public String make(){
        return customSandwich.make() + " musturd";
    }
}
