package com.practice.structural.decorator;

public interface Sandwich {
    String make();
}
