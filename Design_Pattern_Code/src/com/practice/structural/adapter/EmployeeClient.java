package com.practice.structural.adapter;

import java.util.ArrayList;
import java.util.List;

public class EmployeeClient {

    public List<Employee> getEmployees(){
        List<Employee> employees = new ArrayList<>();
        Employee employeeFromDb = new EmployeeDB("1234","john","wick","john.wick@abc.com");
        employees.add(employeeFromDb);
        EmployeeLdap employeeLdap = new EmployeeLdap("chewie","solo","han","han.solo@abc.com");
        employees.add(new EmployeeAdapterLdap(employeeLdap));

        return employees;
    }
}
