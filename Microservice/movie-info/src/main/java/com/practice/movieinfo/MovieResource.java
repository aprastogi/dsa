package com.practice.movieinfo;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.practice.model.Movie;

@RestController
@RequestMapping("/movies")
public class MovieResource {
      
	@RequestMapping("/{movieId}")
	public Movie getMovieInfo(@PathVariable("movieId") String movieid){
		 return new Movie(movieid, "TestName");
	}
	
}
